
var log4js = require('log4js');

var currentDate = Math.floor(Date.now());

log4js.configure({
    appenders: { infoLogs: { type: 'file', filename: "./report/logs/umpInfoLogs_"+currentDate+".log" } },
    categories: { default: { appenders: ['infoLogs'], level: 'info' } }
});

export const logger = log4js.getLogger('default');
