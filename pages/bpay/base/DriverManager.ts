
import { NightwatchBrowser } from 'nightwatch';
const config =require("../../../configs/bpay/EnvConfig.js");


export class DriverManager{

   public browser:NightwatchBrowser;
    constructor(browser : NightwatchBrowser){
        this.browser = browser;
    }

    navigateToURL(url=config.URLS.baseurl) {
        this.browser.pause(2000);
        this.browser
            .url(url);
        console.log("******"+url);
    }

}


