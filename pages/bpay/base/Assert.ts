import {NightwatchBrowser} from "nightwatch";
import {ElementAction} from "./ElementAction";

export class Assert{

    static isValueEqual(browser: NightwatchBrowser,actual,expected,message) {
         browser.verify.equal(actual, expected, message);
    }


    static async isXpathElePresent(browser: NightwatchBrowser, selector: string,expected:boolean, message: string,fieldName:string) {
        let isElePresent = await ElementAction.isElementVisibleByXpath(browser, selector,fieldName);
        await this.isValueEqual(browser,isElePresent,expected,message)
    }

    static async isCssElePresent(browser: NightwatchBrowser, selector: string,expected:boolean, message: string,fieldName:string) {
        let isElePresent  = await ElementAction.isElementVisibleByCss(browser, selector, fieldName);
        await this.isValueEqual(browser,isElePresent,expected,message)
    }

    static async isXpathEleNotPresent(browser: NightwatchBrowser, selector:string, message: string){
        browser.useXpath().verify.elementNotPresent(selector,message);
    }

    static async isCSSEleNotPresent(browser: NightwatchBrowser, selector:string, message: string){
        browser.useCss().verify.elementNotPresent(selector,message);
    }

    static async isCurrentUrlContainsString(browser: NightwatchBrowser, url:string, message: string){
        browser.assert.urlContains(url,message);
    }

    static async isStringContainsValue(browser: NightwatchBrowser,selector,expected,message:string){
        browser.assert.valueContains(selector, expected, message)
    }

    static async checkNavigationToWindow(browser: NightwatchBrowser,url:string,windowSwitch:number, actionMessage:string){
        await ElementAction.switchToWindow(browser,windowSwitch);
        Assert.isCurrentUrlContainsString(browser,url,"Verified the Navigation to new tab successful after click on "+actionMessage);
        await browser.pause(2000);
    }
}

