"use strict";
exports.__esModule = true;
var config = require("../../../configs/bpay/EnvConfig.js");
var DriverManager = /** @class */ (function () {
    function DriverManager(browser) {
        this.browser = browser;
    }
    DriverManager.prototype.navigateToURL = function (url) {
        if (url === void 0) { url = config.URLS.baseurl; }
        this.browser.pause(2000);
        this.browser
            .url(url);
        console.log("******" + url);
    };
    return DriverManager;
}());
exports.DriverManager = DriverManager;
