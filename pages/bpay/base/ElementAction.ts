import {NightwatchBrowser, NightwatchCallbackResult} from 'nightwatch';
import * as Logger from './Logger';
var logger = Logger.logger;
var waitTime = 20000;
var longWaitTime = 30000;
var abortOnFailure = false;
const Selector = require('./GlobalSelector');


// Issue with print log :: https://github.com/nightwatchjs/nightwatch/issues/555
function setValueByXpath(browser: NightwatchBrowser, locator: string, value: string, fieldName: string) {
    return new Promise((resolve, reject) => {
        browser.useXpath().setValue(locator, value);
        resolve("Successfully able to enter the value '" + value + "' in to field '" + fieldName + "'.");
    });
}

function setValueByIndexUsingXpath(browser, eles, index,value, fieldName: string) {
    return new Promise((resolve, reject) => {
        browser.useXpath().elementIdValue(eles.value[index].ELEMENT,value, function (text) {
            resolve("Successfully able to enter the value '" + text.value + "' in to field '" + fieldName + "'.");
        });
    });
}


function setValueByCss(browser: NightwatchBrowser, locator: string, value: string, fieldName: string) {
    return new Promise((resolve, reject) => {
        browser.useCss().setValue(locator, value);
        resolve("Successfully able to enter the value '" + value + "' in to field '" + fieldName + "'.");
    });
}

function setValueByIndexUsingCSS(browser, eles, index,value, fieldName: string) {
    return new Promise((resolve, reject) => {
        browser.useCss().elementIdValue(eles.value[index].ELEMENT,value, function (text) {
            resolve("Successfully able to enter the value '" + text.value + "' in to field '" + fieldName + "'.");
        });
    });
}


function getPageSource(browser :NightwatchBrowser){
    return new Promise((resolve, reject) => {
        let pageCode="null";
        browser
            .source((result) => {
                pageCode = result.value;
                resolve(pageCode);
            });
    });
}



function performClickXpathFun(browser: NightwatchBrowser, locator: string, fieldName: string) {
    return new Promise((resolve, reject) => {
        browser.useXpath().click(locator);
        resolve("Successfully able to perform clickByXpath on '" + fieldName + "'.");
    });
}

function performClickCSSFun(browser: NightwatchBrowser, locator: string, fieldName: string) {
    return new Promise((resolve, reject) => {
        browser.useCss().click(locator);
        resolve("Successfully able to perform clickByXpath on '" + fieldName + "'.");
    });
}

function moveAndPerformClickByCSS(browser: NightwatchBrowser, locator: string, fieldName: string) {
    return new Promise((resolve, reject) => {
        browser.useCss().getLocationInView(locator).moveToElement(locator, 100, 100);
        browser.pause(1100).useCss().click(locator);
        resolve("Successfulyy able to perform clickByXpath on '" + fieldName + "'.");
    });
}


function moveAndPerformClickByXpath(browser: NightwatchBrowser, locator: string, fieldName: string) {
    return new Promise((resolve, reject) => {
        browser.useXpath().getLocationInView(locator).moveToElement(locator, 100, 100);
        browser.pause(1100).useXpath().click(locator);
        resolve("Successfulyy able to perform clickByXpath on '" + fieldName + "'.");
    });
}

function scrollToElementByClassName(browser: NightwatchBrowser,className:string,index:number){
    return new Promise((resolve, reject) => {
        browser.execute('var footerElements = document.getElementsByClassName("'+className+'");' +
            'footerElements['+index+'].scrollIntoView(true);')
        resolve(true);
    });
}

function scrollDown(browser: NightwatchBrowser){
    return new Promise((resolve, reject) => {
        browser.execute('window.scrollTo(0 ,document.body.scrollHeight);');
        resolve(true);
    });
}

function clickAndEnterTextByCss(browser: NightwatchBrowser, locator: string, fieldValue:string,fieldName: string) {
    return new Promise((resolve, reject) => {
        browser.useCss().click(locator).setValue(locator, fieldValue);
        resolve("Able to set Value");
    });
}

function clickAndEnterTextByXpath(browser: NightwatchBrowser, locator: string, fieldValue:string,fieldName: string) {
    return new Promise((resolve, reject) => {
        browser.useXpath().click(locator).useXpath().setValue(locator, fieldValue);
        resolve(true);
    });
}


function clearByCss(browser: NightwatchBrowser, locator: string, fieldName: string) {
    return new Promise((resolve) => {
        browser.useCss().clearValue(locator);
        resolve("Successfully able to clear Text");
    })

}

function clearByXpath(browser: NightwatchBrowser, locator: string, fieldName: string) {
    return new Promise((resolve) => {
        browser.useXpath().clearValue(locator);
        resolve("Successfully able to clear Text");
    })

}

function isElementPresentXpath(browser: NightwatchBrowser, locator: string,fieldName:string) {
    return new Promise((resolve, reject) => {
        browser.element('xpath', locator, function(result){
            if(result.status != -1){
                resolve(true);
            }
            else
                resolve(false);
        });
    });
}

function isElementPresentByCss(browser: NightwatchBrowser, locator: string,fieldName:string) {
    return new Promise((resolve, reject) => {
        browser.element('css selector', locator, function(result){
            if(result.status != -1){
                resolve(true);
            }
            else
                resolve(false);
        });
    });
}

function isElementVisbleByXpath(browser: NightwatchBrowser, locator: string,fieldName:string) {
    return new Promise((resolve, reject) => {
        browser.useXpath().isVisible(locator , function(result) {
            let res = typeof result.value === "boolean" ? result.value : false;
           resolve(res);
        });
    });
}

function isElementVisbleByCss(browser: NightwatchBrowser, locator: string,fieldName:string) {
    return new Promise((resolve, reject) => {
        browser.useCss().isVisible(locator , function(result) {
            var res = typeof result.value === "boolean" ? result.value : false;
            resolve(res);
        });
    });
}



function switchToWindow(browser: NightwatchBrowser, handler: number) {
    return new Promise((resolve, reject) => {
        browser.windowHandles(function (result) {
            let newWindow = result.value[handler];
            browser.switchWindow(newWindow);
            resolve(true);
        });
    });

}

function totalWindowCounr(browser: NightwatchBrowser) {
    return new Promise((resolve, reject) => {
        browser.windowHandles(function (result) {
            let windowCount = result.value.length;
            resolve(windowCount);
        });
    });

}

function getAllElementsStatusByCss(browser, eles) {
    return new Promise((resolve, reject) => {
        let temp = [];
        for (let i = 0; i < eles.value.length; i++) {
            browser.useCss().elementIdAttribute(eles.value[i].ELEMENT, 'checked', function (status) {
                temp.push(status.value == null ? 'false' : status.value);
                if (temp.length == eles.value.length)
                    resolve(temp);
            });
        }
    });
}

function getAttributeValueByIndexUsingXpath(browser, eles,index,attribute) {
    return new Promise((resolve, reject) => {
        browser.useXpath().elementIdAttribute(eles.value[index].ELEMENT, attribute, function (value) {
            resolve(value);
        });
    });
}

function getAttributeValueByIndexUsingCSS(browser, eles,index,attribute) {
    return new Promise((resolve, reject) => {
            browser.useCss().elementIdAttribute(eles.value[index].ELEMENT, attribute, function (value) {
                resolve(value);
            });
    });
}

function getAllAttributeListByCSS(browser, eles,attribute) {
    return new Promise((resolve, reject) => {
        let temp = [];
        for (let i = 0; i < eles.value.length; i++) {
            browser.useCss().elementIdAttribute(eles.value[i].ELEMENT, attribute, function (status) {
                temp.push(status.value);
                if (temp.length == eles.value.length)
                    resolve(temp);
            });
        }
    });
}

function getAllAttributeListByXpath(browser, eles,attribute) {
    return new Promise((resolve, reject) => {
        let temp = [];
        for (let i = 0; i < eles.value.length; i++) {
            browser.useXpath().elementIdAttribute(eles.value[i].ELEMENT, attribute, function (status) {
                temp.push(status.value);
                if (temp.length == eles.value.length)
                    resolve(temp);
            });
        }
    });
}


function getAllElementsByCSS(browser, selector) {
    return new Promise((resolve, reject) => {
        browser.useCss().elements('css selector', selector, function (result) {
            resolve(result);
        });
    });
}



function getAllElementsByXpath(browser, selector) {
    return new Promise((resolve, reject) => {
        browser.useXpath().elements('xpath', selector, function (result) {
            resolve(result);
        });
    });
}

function getTextByIndex(browser, eles, index) {
    return new Promise((resolve, reject) => {
        browser.elementIdText(eles.value[index].ELEMENT, function (text) {
            resolve(text.value);
        })
    })
}

function getAllText(browser, eles) {
    return new Promise((resolve, reject) => {
        let temp = [];
        for (let i = 0; i < eles.value.length; i++) {
            browser.elementIdText(eles.value[i].ELEMENT, function (text) {
                temp.push(text.value);
                if (temp.length == eles.value.length)
                    resolve(temp);
            });
        }
    })
}

function getAllTextInRange(browser, eles,startIndex:number,endIndex:number) {
    return new Promise((resolve, reject) => {
        let temp = [];
        for (let i = startIndex; i <= endIndex ; i++) {
            browser.elementIdText(eles.value[i].ELEMENT, function (text) {
                temp.push(text.value);
                if (temp.length == (endIndex+1-startIndex))
                    resolve(temp);
            });
        }
    })
}



function performClickByIndex(browser, eles, index) {
    return new Promise((resolve, reject) => {
        browser.elementIdClick(eles.value[index].ELEMENT);
        resolve(true);
    })
}


function getText(browser, selector) {
    return new Promise((resolve, reject) => {
        browser.useCss().getText(selector, function (text) {
            resolve(text.value);
        })
    })
}

function waitForElementDisappearByCSS(browser, selector,fieldName:string= '') {
    return new Promise((resolve, reject) => {
        browser.useCss().waitForElementNotPresent(selector, waitTime ,500,false,function() {},"Verify Element '"+fieldName+"' is not present.");
        resolve("Disappeared")
    })
}

function waitForElementPresentByCSS(browser:NightwatchBrowser, selector:string,fieldName:string) {
    return new Promise((resolve, reject) => {
        browser.useCss().waitForElementPresent(selector, waitTime ,500, false,function() {},"Verify Element '"+fieldName+"' is present.").useCss().getElementSize(selector, function (this: NightwatchBrowser, size) {
            if (size.status >= 0)
                resolve(true);
            else
                resolve(false);
        });
    })
}

function waitForElementPresentByXpath(browser:NightwatchBrowser, selector,fieldName:string) {
    return new Promise((resolve, reject) => {
        browser.useXpath().waitForElementPresent(selector, waitTime ,abortOnFailure,function() {},"Verify Element '"+fieldName+"' is present.").useXpath().getElementSize(selector, function (this: NightwatchBrowser, size) {
            if (size.status >= 0) {
                resolve(true);
            }
            else
                resolve(false);
        });
    })
}


function getCSSPropertyValueByCSS(browser, selector,cssPropertyName) {
    return new Promise((resolve, reject) => {
        browser.useCss().getCssProperty(selector, cssPropertyName, function(result) {
            resolve(result);
        });
    });
}


function getCSSPropertyValueByXpath(browser, selector,cssPropertyName) {
    return new Promise((resolve, reject) => {
        browser.useXpath().getCssProperty(selector, cssPropertyName, function(result) {
            resolve(result);
        });
    });
}

function getAttributeValueByCSS(browser, selector,cssPropertyName) {
    return new Promise((resolve, reject) => {
        browser.useCss().getAttribute(selector, cssPropertyName, function(result) {
            resolve(result);
        });
    });
}

function getAttributeValueByXpath(browser, selector,cssPropertyName) {
    return new Promise((resolve, reject) => {
        browser.useXpath().getAttribute(selector, cssPropertyName, function(result) {
            resolve(result);
        });
    });
}


function getTextByXpath(browser, eles) {
    return new Promise((resolve, reject) => {
        browser.useXpath().getText(eles, function (text) {
            resolve(text.value);
        })
    })
}

function getCurrentURL(browser){
    return new Promise((resolve, reject) => {
        browser.url(function (result) {
            resolve(result.value);
        });
    });
}


export class ElementAction {

    static async enterValueByXpath(browser: NightwatchBrowser, selector, value, fieldName : string) {
        let logs;
        if (await waitForElementPresentByXpath(browser, selector,fieldName))
            logs = await setValueByXpath(browser, selector, value, fieldName);
        else
            logs = "Unable to enter the value '" + value + "' in to field '" + fieldName;
        logger.info(logs)
    }


    static async enterValueByCss(browser: NightwatchBrowser, selector, value, fieldName : string) {
        let logs;
        if (await waitForElementPresentByCSS(browser, selector,fieldName))
            logs = await setValueByCss(browser, selector, value, fieldName);
        else
            logs = "Unable to enter the value '" + value + "' in to field '" + fieldName;
        logger.info(logs)

    }

    static async enterValueByIndexUsingXpath(browser: NightwatchBrowser, selector, text,index, fieldName : string) {
        let value;
        if (await waitForElementPresentByXpath(browser, selector,fieldName)) {
            let eles = await getAllElementsByXpath(browser, selector).catch(function (error) {
                console.log('Unable to find elements for : ' + fieldName);
                console.log(error);
            });
            value  =  await setValueByIndexUsingXpath(browser, eles,index,text, fieldName);
        }
        return value;

    }


    static async enterValueByIndexUsingCss(browser: NightwatchBrowser, selector, text,index, fieldName : string) {
        let value;
        if (await this.waitForElementPresentByCSS(browser, selector,fieldName)) {
            let eles = await getAllElementsByCSS(browser, selector).catch(function (error) {
                console.log('Unable to find elements for : ' + fieldName);
                console.log(error);
            });
            value  =  await setValueByIndexUsingCSS(browser, eles,index,text, fieldName);
        }
        return value;

    }


    static async  clickAndEnterTextByCss(browser: NightwatchBrowser, selector, value, fieldName:string) {
        if (await waitForElementPresentByCSS(browser, selector,fieldName))
             clickAndEnterTextByCss(browser, selector, value, fieldName).catch(error => {
            console.log('Some Error in it', error);
             });
    }

    static async clickAndEnterTextByXpath(browser: NightwatchBrowser, selector, value, fieldName:string) {
        if (await waitForElementPresentByXpath(browser, selector, fieldName))
             clickAndEnterTextByXpath(browser, selector, value, fieldName);
    }



    static async getTextByCSS(browser, selector, fieldName:string) {
        if(await  waitForElementPresentByCSS(browser,selector,fieldName))
            return await getText(browser, selector);
    }


    static async getTextByXpath(browser, selector, fieldName:string) {
        if(await  waitForElementPresentByXpath(browser,selector,fieldName))
         return await  getTextByXpath(browser, selector);
    }

    static async clickByXpath(browser: NightwatchBrowser, selector: string, fieldName: string) {
        let logs;
        if (await waitForElementPresentByXpath(browser, selector,fieldName))
            logs = await performClickXpathFun(browser, selector, fieldName);
        else
            throw new Error( "Unable to perform clickByXpath on the field '" + fieldName);
        return logs;
    }


    static async clickByXpathWithoutWait(browser: NightwatchBrowser, selector: string, fieldName: string) {
        let logs = await performClickXpathFun(browser, selector, fieldName);
        return logs;
    }

    static async clickByCSSWithoutWait(browser: NightwatchBrowser, selector: string, fieldName: string) {
        let logs = await performClickCSSFun(browser, selector, fieldName);
        return logs;
    }

    static async clickByCss(browser: NightwatchBrowser, selector: string, fieldName: string) {
        let logs;
        if (await waitForElementPresentByCSS(browser, selector,fieldName))
             logs = await performClickCSSFun(browser, selector, fieldName);
        return logs;
    }


    static async moveAndClickByCSS(browser: NightwatchBrowser, selector: string, fieldName: string) {
        let logs;
        if(await waitForElementPresentByCSS(browser, selector,fieldName))
            logs = await moveAndPerformClickByCSS(browser, selector, fieldName);
        return logs;
    }

    static async moveandClickByXpath(browser: NightwatchBrowser, selector: string, fieldName: string) {
        let logs;
        if(await waitForElementPresentByXpath(browser, selector,fieldName)) {
            logs = await moveAndPerformClickByXpath(browser, selector, fieldName);
        }
        return logs;
    }

    static async scrollToElement(browser:NightwatchBrowser,className:string,index:number){
        let value = await scrollToElementByClassName(browser,className,index);
        return value;
    }

    static async scrollDown(browser:NightwatchBrowser){
        return await scrollDown(browser);
    }


    static async clearTextByCSS(browser: NightwatchBrowser, selector: string, fieldName: string) {
        if(await waitForElementPresentByCSS(browser, selector,fieldName))
             return await clearByCss(browser, selector, fieldName);
    }

    static async clearTextByXpath(browser: NightwatchBrowser, selector: string, fieldName: string) {
        if(await waitForElementPresentByXpath(browser, selector,fieldName))
            return await clearByXpath(browser, selector, fieldName);
    }


    static async clearTextAndEnterValueByCSS(browser: NightwatchBrowser, selector: string, value , fieldName: string) {
        if(await waitForElementPresentByCSS(browser, selector,fieldName)) {
            await clearByCss(browser, selector, fieldName);
            await setValueByCss(browser, selector, value, fieldName);
        }
        else
            throw new Error( "Unable to enter the value '" + value + "' in to field '" + fieldName);
    }



    static async switchToWindow(browser: NightwatchBrowser, handler: number) {
        let logs = await switchToWindow(browser, handler);
        return logs;
    }


    static async getWindowCount(browser: NightwatchBrowser){
        let count = await totalWindowCounr(browser);
        return count;
    }


    static async getAllElementTextByCSS(browser, selector, fieldName : string) {
        let eleValue = null;
        if(await this.waitForElementPresentByCSS(browser, selector,fieldName)) {
            let eles: any = await getAllElementsByCSS(browser, selector);
            if (eles.value.length > 0) {
                eleValue = await getAllText(browser, eles);
                console.log("*************  All Element Text  ********************");
                console.log(eleValue);
            }
        }
        else
            throw new Error( "No Element present for selector " + selector);
        return eleValue;
    }

    static async getAllElementTextByCSSWithoutWait(browser, selector, fieldName:string) {
        let eleValue = null;
            let eles: any = await getAllElementsByCSS(browser, selector);
            if (eles.value.length > 0) {
                eleValue = await getAllText(browser, eles);
                console.log("*************  All Element Text  ********************");
                console.log(eleValue);
            }
            else
                throw new Error( "No Element present for selector " + fieldName);
        return eleValue;
    }

    static async getAllElementTextByXpathWithoutWait(browser, selector,fieldName:string) {
        let eleValue = null;
            let eles: any = await getAllElementsByXpath(browser, selector);
            if (eles.value.length > 0) {
                eleValue = await getAllText(browser, eles);
                console.log("*************  All Element Text  ********************");
                console.log(eleValue);
            }
            else
            throw new Error( "No Element present for selector " + selector);
        return eleValue;
    }


    static async getAllElementTextInRange(browser, selector, fieldName:string, startIndex:number, endIndex:number=-1) {
        let eleValue = null;
        if(await waitForElementPresentByCSS(browser, selector,fieldName)) {
            let eles: any = await getAllElementsByCSS(browser, selector);
            console.log("*******************"+eles.value.length);
            browser.pause(1000);
            endIndex = endIndex == -1 ? (eles.value.length-1) : endIndex;
            if (eles.value.length > startIndex) {
                eleValue = await getAllTextInRange(browser, eles, startIndex,endIndex);
                console.log("*************  All Element Text  ********************");
                console.log(eleValue);
            }
        }
        else
            throw new Error( "No Element present for selector " + selector+" on index "+startIndex);

        return eleValue;
    }


    static async getAllElementTextByXpath(browser, selector,fieldName:string) {
        let eleValue = null;
        if(await this.waitForElementPresentByXpath(browser, selector,fieldName)) {
            let eles: any = await getAllElementsByXpath(browser, selector);
            if (eles.value.length > 0) {
                eleValue = await getAllText(browser, eles);
                console.log("*************  All Element Text  ********************");
                console.log(eleValue);
            }
        }
        else
            throw new Error( "No Element present for selector " + selector);
        return eleValue;
    }

    static async getAllElementsStatusByCSS(browser, selector: string, fieldName:string) {
        let eles = await getAllElementsByCSS(browser,selector);
        let eleValue = await getAllElementsStatusByCss(browser, eles);
        return eleValue;
    }

    static async isElementPresentByXpath(browser, selector, fieldName:string) {
        let isPresent = await isElementPresentXpath(browser, selector,fieldName);
        return isPresent;
    }

    static async waitForElementDisappear(browser, selector,fieldName:string){
        let isDisappered = await waitForElementDisappearByCSS(browser, selector,fieldName);
        return isDisappered;
    }

    static async waitForElementPresentByCSS(browser, selector,fieldName:string){
        let res = await waitForElementPresentByCSS(browser, selector,fieldName);
        return res;
    }

    static async waitForElementPresentByXpath(browser, selector,fieldName:string){
        let res = await waitForElementPresentByXpath(browser, selector,fieldName);
        return res;
    }

    static async isElementPresentByCSS(browser, selector,fieldName:string){
        let isPresent = await isElementPresentByCss(browser, selector,fieldName);
        return isPresent;
    }

    static async isElementVisibleByXpath(browser, selector,fieldName:string){
        let isPresent = await isElementVisbleByXpath(browser, selector,fieldName);
        return isPresent;
    }

    static async isElementVisibleByCss(browser, selector,fieldName:string){
        let isPresent = await isElementVisbleByCss(browser, selector,fieldName);
        return isPresent;
    }

    static async getElementTextByIndex(browser, selector, index,fieldName:string){
        let eles = await getAllElementsByCSS(browser, selector).catch(function (error) {
            console.log('Unable to find elements for : '+fieldName);
            console.log(error);
        });
        let value = await getTextByIndex(browser, eles, index).catch(function (error) {
            console.log('Unable to find elements for : '+fieldName);
            console.log(error);
        });
        return value;
    }

    static assertElementPresent(browser: NightwatchBrowser, selector: string, message: string) {
        browser.useXpath().waitForElementVisible(selector, 30000).expect.element(selector).present;
    }

    static async clickByIndex(browser: NightwatchBrowser, selector, index,fieldName:string){
        if(await waitForElementPresentByCSS(browser,selector,fieldName)) {
            let eles = await getAllElementsByCSS(browser, selector);
            let eleValue = await performClickByIndex(browser, eles, index);
        } else
            throw new Error( "No Element present for selector " + fieldName);
    }

    static async clickByIndexUsingXpath(browser: NightwatchBrowser, selector, index,fieldName:string){
        if(await waitForElementPresentByXpath(browser,selector,fieldName)) {
            let eles = await getAllElementsByXpath(browser, selector);
            let eleValue = await performClickByIndex(browser, eles, index);
        }else
            throw new Error( "No Element present for selector " + fieldName);
    }

    static async getAllElementPresentByCSS(browser: NightwatchBrowser,  selector:string ,fieldName:string){
        let eles :any= await getAllElementsByCSS(browser, selector);
        return (eles.value).length > 0;

    }

    static async getCSSPropertyValueByCSS(browser:NightwatchBrowser, selector:string,cssPropertyName:string,fieldName:string){
        let cssValue: any = await getCSSPropertyValueByCSS(browser, selector, cssPropertyName);
        return cssValue.value;
    }

    static async getCSSPropertyValueByXpath(browser:NightwatchBrowser, selector:string,cssPropertyName:string,fieldName:string){
        let cssValue: any = await getCSSPropertyValueByXpath(browser, selector, cssPropertyName);
        return cssValue.value;
    }
    

    static async getAttributeValueByCSS(browser:NightwatchBrowser, selector:string,attributeName:string,fieldName:string){
        let cssValue: any = await getAttributeValueByCSS(browser, selector, attributeName);
        return cssValue.value;
    }

    static async getAttributeValueByXpath(browser:NightwatchBrowser, selector:string,attributeName:string,fieldName:string){
        let cssValue: any = await getAttributeValueByXpath(browser, selector, attributeName);
        return cssValue.value;
    }

    static async getAttributeValueOnIndexByXpath(browser:NightwatchBrowser, selector:string,index,attributeName:string,fieldName:string){
        let attributeValue: any;
        if(await waitForElementPresentByXpath(browser,selector,fieldName)) {
            let eles = await getAllElementsByXpath(browser, selector);
            attributeValue = await getAttributeValueByIndexUsingXpath(browser, eles,index ,attributeName);
        }
        return attributeValue.value;
    }

    static async getAttributeValueOnIndexByCSS(browser:NightwatchBrowser, selector:string,index,attributeName:string,fieldName:string){
        let attributeValue: any;
        if(await this.waitForElementPresentByCSS(browser,selector,fieldName)) {
            let eles = await getAllElementsByCSS(browser, selector);
            attributeValue = await getAttributeValueByIndexUsingCSS(browser, eles,index ,attributeName);
        }
        return attributeValue.value;
    }

    static async getAllAttributeValueByCSS(browser:NightwatchBrowser, selector:string,attributeName:string,fieldName:string){
        let attributeValue: any;
        if(await this.waitForElementPresentByCSS(browser,selector,fieldName)) {
            let eles = await getAllElementsByCSS(browser, selector);
            attributeValue = await getAllAttributeListByCSS(browser, eles,attributeName);
        }
        return attributeValue;
    }


    static async getAllAttributeValueByXpath(browser:NightwatchBrowser, selector:string,attributeName:string,fieldName:string){
        let attributeValue: any;
        if(await this.waitForElementPresentByXpath(browser,selector,fieldName)) {
            let eles = await getAllElementsByXpath(browser, selector);
            attributeValue = await getAllAttributeListByXpath(browser, eles,attributeName);
        }
        return attributeValue;
    }

    static async waitForPageToLoad(browser: NightwatchBrowser) {
        await ElementAction.waitForElementDisappear(browser,Selector.GlobalSelectors.Page_Loader ,'Page Loadder');
        browser.pause(4000);
    }

    static async getAlertText(browser: NightwatchBrowser) {
       return await ElementAction.getTextByCSS(browser,Selector.GlobalSelectors.Alert_Text,"Alert Text")
    }

    static async moveToElement(browser, locator:string, fieldName:string) {
        return new Promise((resolve, reject) => {
            browser.pause(3000);
            browser.useXpath().getLocationInView(locator).moveToElement(locator, 10, 10);
            resolve("Successfully able to move on element '" + fieldName + "'.");
        });
    }

    static async getCurrentURL(browser){
        let url =  await getCurrentURL(browser) ;
        return url;
    }


    static async moveToElementByCSS(browser, locator:string, fieldName:string) {
        return new Promise((resolve, reject) => {
            browser.pause(3000);
            browser.useCss().getLocationInView(locator).moveToElement(locator, 10, 10);
            resolve("Successfully able to move on element '" + fieldName + "'.");
        });
    }

    static async switchToHomeScreen(browser) {
        browser.closeWindow();
        await ElementAction.switchToWindow(browser, 0);
    }

    static async getPageSource(browser) {
        let pageCode =  await getPageSource(browser);
        return pageCode;
    }

}