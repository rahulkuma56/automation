"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Logger = require("./Logger");
var logger = Logger.logger;
var waitTime = 20000;
var longWaitTime = 30000;
var abortOnFailure = false;
var Selector = require('./GlobalSelector');
// Issue with print log :: https://github.com/nightwatchjs/nightwatch/issues/555
function setValueByXpath(browser, locator, value, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().setValue(locator, value);
        resolve("Successfully able to enter the value '" + value + "' in to field '" + fieldName + "'.");
    });
}
function setValueByIndexUsingXpath(browser, eles, index, value, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().elementIdValue(eles.value[index].ELEMENT, value, function (text) {
            resolve("Successfully able to enter the value '" + text.value + "' in to field '" + fieldName + "'.");
        });
    });
}
function setValueByCss(browser, locator, value, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useCss().setValue(locator, value);
        resolve("Successfully able to enter the value '" + value + "' in to field '" + fieldName + "'.");
    });
}
function setValueByIndexUsingCSS(browser, eles, index, value, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useCss().elementIdValue(eles.value[index].ELEMENT, value, function (text) {
            resolve("Successfully able to enter the value '" + text.value + "' in to field '" + fieldName + "'.");
        });
    });
}
function getPageSource(browser) {
    return new Promise(function (resolve, reject) {
        var pageCode = "null";
        browser
            .source(function (result) {
            pageCode = result.value;
            resolve(pageCode);
        });
    });
}
function performClickXpathFun(browser, locator, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().click(locator);
        resolve("Successfully able to perform clickByXpath on '" + fieldName + "'.");
    });
}
function performClickCSSFun(browser, locator, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useCss().click(locator);
        resolve("Successfully able to perform clickByXpath on '" + fieldName + "'.");
    });
}
function moveAndPerformClickByCSS(browser, locator, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useCss().getLocationInView(locator).moveToElement(locator, 100, 100);
        browser.pause(1100).useCss().click(locator);
        resolve("Successfulyy able to perform clickByXpath on '" + fieldName + "'.");
    });
}
function moveAndPerformClickByXpath(browser, locator, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().getLocationInView(locator).moveToElement(locator, 100, 100);
        browser.pause(1100).useXpath().click(locator);
        resolve("Successfulyy able to perform clickByXpath on '" + fieldName + "'.");
    });
}
function scrollToElementByClassName(browser, className, index) {
    return new Promise(function (resolve, reject) {
        browser.execute('var footerElements = document.getElementsByClassName("' + className + '");' +
            'footerElements[' + index + '].scrollIntoView(true);');
        resolve(true);
    });
}
function scrollDown(browser) {
    return new Promise(function (resolve, reject) {
        browser.execute('window.scrollTo(0 ,document.body.scrollHeight);');
        resolve(true);
    });
}
function clickAndEnterTextByCss(browser, locator, fieldValue, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useCss().click(locator).setValue(locator, fieldValue);
        resolve("Able to set Value");
    });
}
function clickAndEnterTextByXpath(browser, locator, fieldValue, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().click(locator).useXpath().setValue(locator, fieldValue);
        resolve(true);
    });
}
function clearByCss(browser, locator, fieldName) {
    return new Promise(function (resolve) {
        browser.useCss().clearValue(locator);
        resolve("Successfully able to clear Text");
    });
}
function clearByXpath(browser, locator, fieldName) {
    return new Promise(function (resolve) {
        browser.useXpath().clearValue(locator);
        resolve("Successfully able to clear Text");
    });
}
function isElementPresentXpath(browser, locator, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.element('xpath', locator, function (result) {
            if (result.status != -1) {
                resolve(true);
            }
            else
                resolve(false);
        });
    });
}
function isElementPresentByCss(browser, locator, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.element('css selector', locator, function (result) {
            if (result.status != -1) {
                resolve(true);
            }
            else
                resolve(false);
        });
    });
}
function isElementVisbleByXpath(browser, locator, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().isVisible(locator, function (result) {
            var res = typeof result.value === "boolean" ? result.value : false;
            resolve(res);
        });
    });
}
function isElementVisbleByCss(browser, locator, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useCss().isVisible(locator, function (result) {
            var res = typeof result.value === "boolean" ? result.value : false;
            resolve(res);
        });
    });
}
function switchToWindow(browser, handler) {
    return new Promise(function (resolve, reject) {
        browser.windowHandles(function (result) {
            var newWindow = result.value[handler];
            browser.switchWindow(newWindow);
            resolve(true);
        });
    });
}
function totalWindowCounr(browser) {
    return new Promise(function (resolve, reject) {
        browser.windowHandles(function (result) {
            var windowCount = result.value.length;
            resolve(windowCount);
        });
    });
}
function getAllElementsStatusByCss(browser, eles) {
    return new Promise(function (resolve, reject) {
        var temp = [];
        for (var i = 0; i < eles.value.length; i++) {
            browser.useCss().elementIdAttribute(eles.value[i].ELEMENT, 'checked', function (status) {
                temp.push(status.value == null ? 'false' : status.value);
                if (temp.length == eles.value.length)
                    resolve(temp);
            });
        }
    });
}
function getAttributeValueByIndexUsingXpath(browser, eles, index, attribute) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().elementIdAttribute(eles.value[index].ELEMENT, attribute, function (value) {
            resolve(value);
        });
    });
}
function getAttributeValueByIndexUsingCSS(browser, eles, index, attribute) {
    return new Promise(function (resolve, reject) {
        browser.useCss().elementIdAttribute(eles.value[index].ELEMENT, attribute, function (value) {
            resolve(value);
        });
    });
}
function getAllAttributeListByCSS(browser, eles, attribute) {
    return new Promise(function (resolve, reject) {
        var temp = [];
        for (var i = 0; i < eles.value.length; i++) {
            browser.useCss().elementIdAttribute(eles.value[i].ELEMENT, attribute, function (status) {
                temp.push(status.value);
                if (temp.length == eles.value.length)
                    resolve(temp);
            });
        }
    });
}
function getAllAttributeListByXpath(browser, eles, attribute) {
    return new Promise(function (resolve, reject) {
        var temp = [];
        for (var i = 0; i < eles.value.length; i++) {
            browser.useXpath().elementIdAttribute(eles.value[i].ELEMENT, attribute, function (status) {
                temp.push(status.value);
                if (temp.length == eles.value.length)
                    resolve(temp);
            });
        }
    });
}
function getAllElementsByCSS(browser, selector) {
    return new Promise(function (resolve, reject) {
        browser.useCss().elements('css selector', selector, function (result) {
            resolve(result);
        });
    });
}
function getAllElementsByXpath(browser, selector) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().elements('xpath', selector, function (result) {
            resolve(result);
        });
    });
}
function getTextByIndex(browser, eles, index) {
    return new Promise(function (resolve, reject) {
        browser.elementIdText(eles.value[index].ELEMENT, function (text) {
            resolve(text.value);
        });
    });
}
function getAllText(browser, eles) {
    return new Promise(function (resolve, reject) {
        var temp = [];
        for (var i = 0; i < eles.value.length; i++) {
            browser.elementIdText(eles.value[i].ELEMENT, function (text) {
                temp.push(text.value);
                if (temp.length == eles.value.length)
                    resolve(temp);
            });
        }
    });
}
function getAllTextInRange(browser, eles, startIndex, endIndex) {
    return new Promise(function (resolve, reject) {
        var temp = [];
        for (var i = startIndex; i <= endIndex; i++) {
            browser.elementIdText(eles.value[i].ELEMENT, function (text) {
                temp.push(text.value);
                if (temp.length == (endIndex + 1 - startIndex))
                    resolve(temp);
            });
        }
    });
}
function performClickByIndex(browser, eles, index) {
    return new Promise(function (resolve, reject) {
        browser.elementIdClick(eles.value[index].ELEMENT);
        resolve(true);
    });
}
function getText(browser, selector) {
    return new Promise(function (resolve, reject) {
        browser.useCss().getText(selector, function (text) {
            resolve(text.value);
        });
    });
}
function waitForElementDisappearByCSS(browser, selector, fieldName) {
    if (fieldName === void 0) { fieldName = ''; }
    return new Promise(function (resolve, reject) {
        browser.useCss().waitForElementNotPresent(selector, waitTime, 500, false, function () { }, "Verify Element '" + fieldName + "' is not present.");
        resolve("Disappeared");
    });
}
function waitForElementPresentByCSS(browser, selector, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useCss().waitForElementPresent(selector, waitTime, 500, false, function () { }, "Verify Element '" + fieldName + "' is present.").useCss().getElementSize(selector, function (size) {
            if (size.status >= 0)
                resolve(true);
            else
                resolve(false);
        });
    });
}
function waitForElementPresentByXpath(browser, selector, fieldName) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().waitForElementPresent(selector, waitTime, abortOnFailure, function () { }, "Verify Element '" + fieldName + "' is present.").useXpath().getElementSize(selector, function (size) {
            if (size.status >= 0) {
                resolve(true);
            }
            else
                resolve(false);
        });
    });
}
function getCSSPropertyValueByCSS(browser, selector, cssPropertyName) {
    return new Promise(function (resolve, reject) {
        browser.useCss().getCssProperty(selector, cssPropertyName, function (result) {
            resolve(result);
        });
    });
}
function getCSSPropertyValueByXpath(browser, selector, cssPropertyName) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().getCssProperty(selector, cssPropertyName, function (result) {
            resolve(result);
        });
    });
}
function getAttributeValueByCSS(browser, selector, cssPropertyName) {
    return new Promise(function (resolve, reject) {
        browser.useCss().getAttribute(selector, cssPropertyName, function (result) {
            resolve(result);
        });
    });
}
function getAttributeValueByXpath(browser, selector, cssPropertyName) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().getAttribute(selector, cssPropertyName, function (result) {
            resolve(result);
        });
    });
}
function getTextByXpath(browser, eles) {
    return new Promise(function (resolve, reject) {
        browser.useXpath().getText(eles, function (text) {
            resolve(text.value);
        });
    });
}
function getCurrentURL(browser) {
    return new Promise(function (resolve, reject) {
        browser.url(function (result) {
            resolve(result.value);
        });
    });
}
var ElementAction = /** @class */ (function () {
    function ElementAction() {
    }
    ElementAction.enterValueByXpath = function (browser, selector, value, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 3];
                        return [4 /*yield*/, setValueByXpath(browser, selector, value, fieldName)];
                    case 2:
                        logs = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        logs = "Unable to enter the value '" + value + "' in to field '" + fieldName;
                        _a.label = 4;
                    case 4:
                        logger.info(logs);
                        return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.enterValueByCss = function (browser, selector, value, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 3];
                        return [4 /*yield*/, setValueByCss(browser, selector, value, fieldName)];
                    case 2:
                        logs = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        logs = "Unable to enter the value '" + value + "' in to field '" + fieldName;
                        _a.label = 4;
                    case 4:
                        logger.info(logs);
                        return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.enterValueByIndexUsingXpath = function (browser, selector, text, index, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var value, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllElementsByXpath(browser, selector)["catch"](function (error) {
                                console.log('Unable to find elements for : ' + fieldName);
                                console.log(error);
                            })];
                    case 2:
                        eles = _a.sent();
                        return [4 /*yield*/, setValueByIndexUsingXpath(browser, eles, index, text, fieldName)];
                    case 3:
                        value = _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/, value];
                }
            });
        });
    };
    ElementAction.enterValueByIndexUsingCss = function (browser, selector, text, index, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var value, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllElementsByCSS(browser, selector)["catch"](function (error) {
                                console.log('Unable to find elements for : ' + fieldName);
                                console.log(error);
                            })];
                    case 2:
                        eles = _a.sent();
                        return [4 /*yield*/, setValueByIndexUsingCSS(browser, eles, index, text, fieldName)];
                    case 3:
                        value = _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/, value];
                }
            });
        });
    };
    ElementAction.clickAndEnterTextByCss = function (browser, selector, value, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (_a.sent())
                            clickAndEnterTextByCss(browser, selector, value, fieldName)["catch"](function (error) {
                                console.log('Some Error in it', error);
                            });
                        return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.clickAndEnterTextByXpath = function (browser, selector, value, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (_a.sent())
                            clickAndEnterTextByXpath(browser, selector, value, fieldName);
                        return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.getTextByCSS = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 3];
                        return [4 /*yield*/, getText(browser, selector)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.getTextByXpath = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 3];
                        return [4 /*yield*/, getTextByXpath(browser, selector)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.clickByXpath = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 3];
                        return [4 /*yield*/, performClickXpathFun(browser, selector, fieldName)];
                    case 2:
                        logs = _a.sent();
                        return [3 /*break*/, 4];
                    case 3: throw new Error("Unable to perform clickByXpath on the field '" + fieldName);
                    case 4: return [2 /*return*/, logs];
                }
            });
        });
    };
    ElementAction.clickByXpathWithoutWait = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, performClickXpathFun(browser, selector, fieldName)];
                    case 1:
                        logs = _a.sent();
                        return [2 /*return*/, logs];
                }
            });
        });
    };
    ElementAction.clickByCSSWithoutWait = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, performClickCSSFun(browser, selector, fieldName)];
                    case 1:
                        logs = _a.sent();
                        return [2 /*return*/, logs];
                }
            });
        });
    };
    ElementAction.clickByCss = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 3];
                        return [4 /*yield*/, performClickCSSFun(browser, selector, fieldName)];
                    case 2:
                        logs = _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/, logs];
                }
            });
        });
    };
    ElementAction.moveAndClickByCSS = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 3];
                        return [4 /*yield*/, moveAndPerformClickByCSS(browser, selector, fieldName)];
                    case 2:
                        logs = _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/, logs];
                }
            });
        });
    };
    ElementAction.moveandClickByXpath = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 3];
                        return [4 /*yield*/, moveAndPerformClickByXpath(browser, selector, fieldName)];
                    case 2:
                        logs = _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/, logs];
                }
            });
        });
    };
    ElementAction.scrollToElement = function (browser, className, index) {
        return __awaiter(this, void 0, void 0, function () {
            var value;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, scrollToElementByClassName(browser, className, index)];
                    case 1:
                        value = _a.sent();
                        return [2 /*return*/, value];
                }
            });
        });
    };
    ElementAction.scrollDown = function (browser) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, scrollDown(browser)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ElementAction.clearTextByCSS = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 3];
                        return [4 /*yield*/, clearByCss(browser, selector, fieldName)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.clearTextByXpath = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 3];
                        return [4 /*yield*/, clearByXpath(browser, selector, fieldName)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.clearTextAndEnterValueByCSS = function (browser, selector, value, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 4];
                        return [4 /*yield*/, clearByCss(browser, selector, fieldName)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, setValueByCss(browser, selector, value, fieldName)];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4: throw new Error("Unable to enter the value '" + value + "' in to field '" + fieldName);
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.switchToWindow = function (browser, handler) {
        return __awaiter(this, void 0, void 0, function () {
            var logs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, switchToWindow(browser, handler)];
                    case 1:
                        logs = _a.sent();
                        return [2 /*return*/, logs];
                }
            });
        });
    };
    ElementAction.getWindowCount = function (browser) {
        return __awaiter(this, void 0, void 0, function () {
            var count;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, totalWindowCounr(browser)];
                    case 1:
                        count = _a.sent();
                        return [2 /*return*/, count];
                }
            });
        });
    };
    ElementAction.getAllElementTextByCSS = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var eleValue, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        eleValue = null;
                        return [4 /*yield*/, this.waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 5];
                        return [4 /*yield*/, getAllElementsByCSS(browser, selector)];
                    case 2:
                        eles = _a.sent();
                        if (!(eles.value.length > 0)) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllText(browser, eles)];
                    case 3:
                        eleValue = _a.sent();
                        console.log("*************  All Element Text  ********************");
                        console.log(eleValue);
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5: throw new Error("No Element present for selector " + selector);
                    case 6: return [2 /*return*/, eleValue];
                }
            });
        });
    };
    ElementAction.getAllElementTextByCSSWithoutWait = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var eleValue, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        eleValue = null;
                        return [4 /*yield*/, getAllElementsByCSS(browser, selector)];
                    case 1:
                        eles = _a.sent();
                        if (!(eles.value.length > 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, getAllText(browser, eles)];
                    case 2:
                        eleValue = _a.sent();
                        console.log("*************  All Element Text  ********************");
                        console.log(eleValue);
                        return [3 /*break*/, 4];
                    case 3: throw new Error("No Element present for selector " + fieldName);
                    case 4: return [2 /*return*/, eleValue];
                }
            });
        });
    };
    ElementAction.getAllElementTextByXpathWithoutWait = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var eleValue, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        eleValue = null;
                        return [4 /*yield*/, getAllElementsByXpath(browser, selector)];
                    case 1:
                        eles = _a.sent();
                        if (!(eles.value.length > 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, getAllText(browser, eles)];
                    case 2:
                        eleValue = _a.sent();
                        console.log("*************  All Element Text  ********************");
                        console.log(eleValue);
                        return [3 /*break*/, 4];
                    case 3: throw new Error("No Element present for selector " + selector);
                    case 4: return [2 /*return*/, eleValue];
                }
            });
        });
    };
    ElementAction.getAllElementTextInRange = function (browser, selector, fieldName, startIndex, endIndex) {
        if (endIndex === void 0) { endIndex = -1; }
        return __awaiter(this, void 0, void 0, function () {
            var eleValue, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        eleValue = null;
                        return [4 /*yield*/, waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 5];
                        return [4 /*yield*/, getAllElementsByCSS(browser, selector)];
                    case 2:
                        eles = _a.sent();
                        console.log("*******************" + eles.value.length);
                        browser.pause(1000);
                        endIndex = endIndex == -1 ? (eles.value.length - 1) : endIndex;
                        if (!(eles.value.length > startIndex)) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllTextInRange(browser, eles, startIndex, endIndex)];
                    case 3:
                        eleValue = _a.sent();
                        console.log("*************  All Element Text  ********************");
                        console.log(eleValue);
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5: throw new Error("No Element present for selector " + selector + " on index " + startIndex);
                    case 6: return [2 /*return*/, eleValue];
                }
            });
        });
    };
    ElementAction.getAllElementTextByXpath = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var eleValue, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        eleValue = null;
                        return [4 /*yield*/, this.waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 5];
                        return [4 /*yield*/, getAllElementsByXpath(browser, selector)];
                    case 2:
                        eles = _a.sent();
                        if (!(eles.value.length > 0)) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllText(browser, eles)];
                    case 3:
                        eleValue = _a.sent();
                        console.log("*************  All Element Text  ********************");
                        console.log(eleValue);
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5: throw new Error("No Element present for selector " + selector);
                    case 6: return [2 /*return*/, eleValue];
                }
            });
        });
    };
    ElementAction.getAllElementsStatusByCSS = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var eles, eleValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getAllElementsByCSS(browser, selector)];
                    case 1:
                        eles = _a.sent();
                        return [4 /*yield*/, getAllElementsStatusByCss(browser, eles)];
                    case 2:
                        eleValue = _a.sent();
                        return [2 /*return*/, eleValue];
                }
            });
        });
    };
    ElementAction.isElementPresentByXpath = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var isPresent;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, isElementPresentXpath(browser, selector, fieldName)];
                    case 1:
                        isPresent = _a.sent();
                        return [2 /*return*/, isPresent];
                }
            });
        });
    };
    ElementAction.waitForElementDisappear = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var isDisappered;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementDisappearByCSS(browser, selector, fieldName)];
                    case 1:
                        isDisappered = _a.sent();
                        return [2 /*return*/, isDisappered];
                }
            });
        });
    };
    ElementAction.waitForElementPresentByCSS = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res];
                }
            });
        });
    };
    ElementAction.waitForElementPresentByXpath = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res];
                }
            });
        });
    };
    ElementAction.isElementPresentByCSS = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var isPresent;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, isElementPresentByCss(browser, selector, fieldName)];
                    case 1:
                        isPresent = _a.sent();
                        return [2 /*return*/, isPresent];
                }
            });
        });
    };
    ElementAction.isElementVisibleByXpath = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var isPresent;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, isElementVisbleByXpath(browser, selector, fieldName)];
                    case 1:
                        isPresent = _a.sent();
                        return [2 /*return*/, isPresent];
                }
            });
        });
    };
    ElementAction.isElementVisibleByCss = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var isPresent;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, isElementVisbleByCss(browser, selector, fieldName)];
                    case 1:
                        isPresent = _a.sent();
                        return [2 /*return*/, isPresent];
                }
            });
        });
    };
    ElementAction.getElementTextByIndex = function (browser, selector, index, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var eles, value;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getAllElementsByCSS(browser, selector)["catch"](function (error) {
                            console.log('Unable to find elements for : ' + fieldName);
                            console.log(error);
                        })];
                    case 1:
                        eles = _a.sent();
                        return [4 /*yield*/, getTextByIndex(browser, eles, index)["catch"](function (error) {
                                console.log('Unable to find elements for : ' + fieldName);
                                console.log(error);
                            })];
                    case 2:
                        value = _a.sent();
                        return [2 /*return*/, value];
                }
            });
        });
    };
    ElementAction.assertElementPresent = function (browser, selector, message) {
        browser.useXpath().waitForElementVisible(selector, 30000).expect.element(selector).present;
    };
    ElementAction.clickByIndex = function (browser, selector, index, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var eles, eleValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllElementsByCSS(browser, selector)];
                    case 2:
                        eles = _a.sent();
                        return [4 /*yield*/, performClickByIndex(browser, eles, index)];
                    case 3:
                        eleValue = _a.sent();
                        return [3 /*break*/, 5];
                    case 4: throw new Error("No Element present for selector " + fieldName);
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.clickByIndexUsingXpath = function (browser, selector, index, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var eles, eleValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllElementsByXpath(browser, selector)];
                    case 2:
                        eles = _a.sent();
                        return [4 /*yield*/, performClickByIndex(browser, eles, index)];
                    case 3:
                        eleValue = _a.sent();
                        return [3 /*break*/, 5];
                    case 4: throw new Error("No Element present for selector " + fieldName);
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.getAllElementPresentByCSS = function (browser, selector, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getAllElementsByCSS(browser, selector)];
                    case 1:
                        eles = _a.sent();
                        return [2 /*return*/, (eles.value).length > 0];
                }
            });
        });
    };
    ElementAction.getCSSPropertyValueByCSS = function (browser, selector, cssPropertyName, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var cssValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getCSSPropertyValueByCSS(browser, selector, cssPropertyName)];
                    case 1:
                        cssValue = _a.sent();
                        return [2 /*return*/, cssValue.value];
                }
            });
        });
    };
    ElementAction.getCSSPropertyValueByXpath = function (browser, selector, cssPropertyName, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var cssValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getCSSPropertyValueByXpath(browser, selector, cssPropertyName)];
                    case 1:
                        cssValue = _a.sent();
                        return [2 /*return*/, cssValue.value];
                }
            });
        });
    };
    ElementAction.getAttributeValueByCSS = function (browser, selector, attributeName, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var cssValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getAttributeValueByCSS(browser, selector, attributeName)];
                    case 1:
                        cssValue = _a.sent();
                        return [2 /*return*/, cssValue.value];
                }
            });
        });
    };
    ElementAction.getAttributeValueByXpath = function (browser, selector, attributeName, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var cssValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getAttributeValueByXpath(browser, selector, attributeName)];
                    case 1:
                        cssValue = _a.sent();
                        return [2 /*return*/, cssValue.value];
                }
            });
        });
    };
    ElementAction.getAttributeValueOnIndexByXpath = function (browser, selector, index, attributeName, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var attributeValue, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllElementsByXpath(browser, selector)];
                    case 2:
                        eles = _a.sent();
                        return [4 /*yield*/, getAttributeValueByIndexUsingXpath(browser, eles, index, attributeName)];
                    case 3:
                        attributeValue = _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/, attributeValue.value];
                }
            });
        });
    };
    ElementAction.getAttributeValueOnIndexByCSS = function (browser, selector, index, attributeName, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var attributeValue, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllElementsByCSS(browser, selector)];
                    case 2:
                        eles = _a.sent();
                        return [4 /*yield*/, getAttributeValueByIndexUsingCSS(browser, eles, index, attributeName)];
                    case 3:
                        attributeValue = _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/, attributeValue.value];
                }
            });
        });
    };
    ElementAction.getAllAttributeValueByCSS = function (browser, selector, attributeName, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var attributeValue, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.waitForElementPresentByCSS(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllElementsByCSS(browser, selector)];
                    case 2:
                        eles = _a.sent();
                        return [4 /*yield*/, getAllAttributeListByCSS(browser, eles, attributeName)];
                    case 3:
                        attributeValue = _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/, attributeValue];
                }
            });
        });
    };
    ElementAction.getAllAttributeValueByXpath = function (browser, selector, attributeName, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            var attributeValue, eles;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.waitForElementPresentByXpath(browser, selector, fieldName)];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 4];
                        return [4 /*yield*/, getAllElementsByXpath(browser, selector)];
                    case 2:
                        eles = _a.sent();
                        return [4 /*yield*/, getAllAttributeListByXpath(browser, eles, attributeName)];
                    case 3:
                        attributeValue = _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/, attributeValue];
                }
            });
        });
    };
    ElementAction.waitForPageToLoad = function (browser) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction.waitForElementDisappear(browser, Selector.GlobalSelectors.Page_Loader, 'Page Loadder')];
                    case 1:
                        _a.sent();
                        browser.pause(4000);
                        return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.getAlertText = function (browser) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction.getTextByCSS(browser, Selector.GlobalSelectors.Alert_Text, "Alert Text")];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ElementAction.moveToElement = function (browser, locator, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        browser.pause(3000);
                        browser.useXpath().getLocationInView(locator).moveToElement(locator, 10, 10);
                        resolve("Successfully able to move on element '" + fieldName + "'.");
                    })];
            });
        });
    };
    ElementAction.getCurrentURL = function (browser) {
        return __awaiter(this, void 0, void 0, function () {
            var url;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getCurrentURL(browser)];
                    case 1:
                        url = _a.sent();
                        return [2 /*return*/, url];
                }
            });
        });
    };
    ElementAction.moveToElementByCSS = function (browser, locator, fieldName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        browser.pause(3000);
                        browser.useCss().getLocationInView(locator).moveToElement(locator, 10, 10);
                        resolve("Successfully able to move on element '" + fieldName + "'.");
                    })];
            });
        });
    };
    ElementAction.switchToHomeScreen = function (browser) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        browser.closeWindow();
                        return [4 /*yield*/, ElementAction.switchToWindow(browser, 0)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ElementAction.getPageSource = function (browser) {
        return __awaiter(this, void 0, void 0, function () {
            var pageCode;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getPageSource(browser)];
                    case 1:
                        pageCode = _a.sent();
                        return [2 /*return*/, pageCode];
                }
            });
        });
    };
    return ElementAction;
}());
exports.ElementAction = ElementAction;
