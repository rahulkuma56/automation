"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var ElementAction_1 = require("../base/ElementAction");
var DriverManager_1 = require("../base/DriverManager");
var Assert_1 = require("../base/Assert");
var EmailReader_1 = require("../../util/emailUtil/EmailReader");
var HomeKeySelector = require('./HomeKeySelector');
var LoginPage = /** @class */ (function () {
    function LoginPage(client) {
        this.browser = client;
    }
    LoginPage.prototype.setUserName = function (userName) {
        ElementAction_1.ElementAction.enterValueByXpath(this.browser, HomeKeySelector.Login.UserName, userName, 'User Name');
        this.browser.pause(10);
    };
    LoginPage.prototype.setPassword = function (password) {
        ElementAction_1.ElementAction.enterValueByXpath(this.browser, HomeKeySelector.Login.Password, password, 'Password');
        this.browser.pause(10);
    };
    LoginPage.prototype.clickSignInSecurely = function () {
        ElementAction_1.ElementAction.clickByXpath(this.browser, HomeKeySelector.Login.SignInBtn, 'Sign In Button');
        this.browser.pause(100);
    };
    LoginPage.prototype.clickOnUserMenu = function () {
        ElementAction_1.ElementAction.clickByCss(this.browser, HomeKeySelector.Login.UserMenu, 'User Menu');
    };
    LoginPage.prototype.clickLogoutLink = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByCSS(this.browser, HomeKeySelector.Login.LogoutLink, 'Logout Link')];
                    case 1:
                        _a.sent();
                        this.browser.pause(1000);
                        return [4 /*yield*/, ElementAction_1.ElementAction.clickByCss(this.browser, HomeKeySelector.Login.LogoutLink, 'Logout Link')];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.clickOnTestToggleBTN = function () {
        if (process.env.TOGGLE === 'on')
            ElementAction_1.ElementAction.clickByCss(this.browser, HomeKeySelector.Login.TestToggleBtn, 'Toggle Button');
    };
    LoginPage.prototype.chooseJoinTeamOption = function (optionID) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.clickByCss(this.browser, optionID, "Join Team Option")];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.joinTheTeamAndAssertAlertText = function (optionID, adminName) {
        return __awaiter(this, void 0, void 0, function () {
            var expectedAlertText, actualAlertText;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.chooseJoinTeamOption(optionID)];
                    case 1:
                        _a.sent();
                        expectedAlertText = "You have successfully joined " + adminName + "’s team";
                        return [4 /*yield*/, ElementAction_1.ElementAction.getAlertText(this.browser)];
                    case 2:
                        actualAlertText = _a.sent();
                        Assert_1.Assert.isValueEqual(this.browser, actualAlertText, expectedAlertText, "Verify the Join Team Alert Text is as expected.");
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.login = function (userDetail) {
        new DriverManager_1.DriverManager(this.browser).navigateToURL();
        // this.browser.pause(5000);
        this.browser.frame(0);
        this.setUserName(userDetail.name);
        this.setPassword(userDetail.password);
        this.clickSignInSecurely();
        // this.browser.frame(null);
        // this.clickOnTestToggleBTN();
    };
    LoginPage.prototype.waitForPageToLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.waitForPageToLoad(this.browser)];
                    case 1:
                        _a.sent();
                        this.browser.pause(4000);
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.moveToMid = function (mid) {
        return __awaiter(this, void 0, void 0, function () {
            var log;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.clickOnUserMenu();
                        return [4 /*yield*/, ElementAction_1.ElementAction.clickAndEnterTextByCss(this.browser, HomeKeySelector.Login.MidSearch, mid, 'Search Mid')];
                    case 1:
                        _a.sent();
                        this.browser.pause(2000);
                        return [4 /*yield*/, ElementAction_1.ElementAction.clickByCss(this.browser, HomeKeySelector.Login.MID, 'MID')["catch"](function (error) {
                                console.log('Some Error in it', error);
                            })];
                    case 2:
                        log = _a.sent();
                        return [2 /*return*/, log];
                }
            });
        });
    };
    LoginPage.prototype.logoutFromApp = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.clickOnUserMenu();
                this.browser.pause(2000);
                this.clickLogoutLink();
                this.browser.pause(2000);
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.enterOTP = function () {
        return __awaiter(this, void 0, void 0, function () {
            var otpNo, log;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByCSS(this.browser, ".resend-otp-main input", "Enter OTP")];
                    case 1:
                        _a.sent();
                        this.browser.pause(8000);
                        return [4 /*yield*/, new EmailReader_1.EmailReader().getOTP()];
                    case 2:
                        otpNo = _a.sent();
                        this.browser.pause(8000);
                        return [4 /*yield*/, ElementAction_1.ElementAction.clickAndEnterTextByCss(this.browser, ".resend-otp-main input", otpNo, "Enter OTP")];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, ElementAction_1.ElementAction.clickByCss(this.browser, "button[name='Login'] i", 'OTP Submit Button')["catch"](function (error) {
                                console.log('Some Error in it', error);
                            })];
                    case 4:
                        log = _a.sent();
                        this.browser.pause(5000);
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.assertLogoutFromTheApp = function () {
        return __awaiter(this, void 0, void 0, function () {
            var actualResult;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.logoutFromApp()
                        // await this.browser.pause(2000);
                    ];
                    case 1:
                        _a.sent();
                        // await this.browser.pause(2000);
                        this.browser.frame(0);
                        return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByXpath(this.browser, HomeKeySelector.Login.Password, 'Password')];
                    case 2:
                        actualResult = _a.sent();
                        Assert_1.Assert.isValueEqual(this.browser, actualResult, true, "Verify password field is visible as user logout from the app.");
                        this.browser.pause(5000);
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.switchToMID = function (userDetail) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByCSS(this.browser, HomeKeySelector.Login.UserMenu, 'Merchant User Menu')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, ElementAction_1.ElementAction.clickByCss(this.browser, HomeKeySelector.Login.UserMenu, 'Merchant User Menu')];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.searchMid(userDetail.mid)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, ElementAction_1.ElementAction.clickByCss(this.browser, HomeKeySelector.Login.MID, 'Select MID')];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.searchMid = function (mid) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByCSS(this.browser, HomeKeySelector.Login.MidSearch, 'MID Search Text Box')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, ElementAction_1.ElementAction.clickByCss(this.browser, HomeKeySelector.Login.MidSearch, 'MID Search Text Box')];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, ElementAction_1.ElementAction.enterValueByCss(this.browser, HomeKeySelector.Login.MidSearch, mid, 'Enter value to MID Search text Box')];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return LoginPage;
}());
exports.LoginPage = LoginPage;
var JoinTeam;
(function (JoinTeam) {
    JoinTeam["ACCEPTED"] = "[id*='accepted']";
    JoinTeam["REJECTED"] = "[id*='rejected]";
})(JoinTeam = exports.JoinTeam || (exports.JoinTeam = {}));
