
import { NightwatchBrowser } from 'nightwatch';
import {ElementAction} from "../base/ElementAction";
import {DriverManager} from "../base/DriverManager";
import {Assert} from "../base/Assert";
import {EmailReader} from "../../util/emailUtil/EmailReader";
const HomeKeySelector = require('./HomeKeySelector');

export class LoginPage{

    public browser:NightwatchBrowser;
    constructor(client : NightwatchBrowser){
        this.browser = client;
    }

    setUserName(userName:string) {
        ElementAction.enterValueByXpath(this.browser,HomeKeySelector.Login.UserName,userName,'User Name');
        this.browser.pause(10);

    }

    setPassword(password:string) {
        ElementAction.enterValueByXpath(this.browser,HomeKeySelector.Login.Password,password,'Password');
        this.browser.pause(10);

    }

    clickSignInSecurely(){
        ElementAction.clickByXpath(this.browser,HomeKeySelector.Login.SignInBtn,'Sign In Button');
        this.browser.pause(100);
    }

    clickOnUserMenu(){
        ElementAction.clickByCss(this.browser,HomeKeySelector.Login.UserMenu,'User Menu');
    }

    async clickLogoutLink(){
       await ElementAction.waitForElementPresentByCSS(this.browser,HomeKeySelector.Login.LogoutLink,'Logout Link');
       this.browser.pause(1000);
        await ElementAction.clickByCss(this.browser,HomeKeySelector.Login.LogoutLink,'Logout Link');
    }

    clickOnTestToggleBTN(){
        if(process.env.TOGGLE === 'on')
             ElementAction.clickByCss(this.browser,HomeKeySelector.Login.TestToggleBtn,'Toggle Button');
    }

   async chooseJoinTeamOption(optionID:JoinTeam){
        await ElementAction.clickByCss(this.browser,optionID,"Join Team Option");
    }

    async joinTheTeamAndAssertAlertText(optionID:JoinTeam,adminName:string){
        await this.chooseJoinTeamOption(optionID);
        let expectedAlertText = "You have successfully joined "+adminName+"’s team"
        let actualAlertText = await ElementAction.getAlertText(this.browser);
        Assert.isValueEqual(this.browser,actualAlertText,expectedAlertText,"Verify the Join Team Alert Text is as expected.")
    }

    login(userDetail){
        new DriverManager(this.browser).navigateToURL();
        // this.browser.pause(5000);
        this.browser.frame(0);
        this.setUserName(userDetail.name);
        this.setPassword(userDetail.password);
        this.clickSignInSecurely();
        // this.browser.frame(null);
        // this.clickOnTestToggleBTN();
    }

    async waitForPageToLoad() {
        await ElementAction.waitForPageToLoad(this.browser);
        this.browser.pause(4000);
    }

   async moveToMid(mid:string){
        this.clickOnUserMenu();
        await ElementAction.clickAndEnterTextByCss(this.browser,HomeKeySelector.Login.MidSearch,mid,'Search Mid')
        this.browser.pause(2000);
        let log =  await ElementAction.clickByCss(this.browser,HomeKeySelector.Login.MID,'MID').catch(error => {
            console.log('Some Error in it', error);
        });
        return log;
    }


   async logoutFromApp(){
        this.clickOnUserMenu();
        this.browser.pause(2000);
        this.clickLogoutLink();
       this.browser.pause(2000);
    }

    async enterOTP(){
        await ElementAction.waitForElementPresentByCSS(this.browser, ".resend-otp-main input","Enter OTP");
        this.browser.pause(8000);
        let otpNo = await new EmailReader().getOTP();
        this.browser.pause(8000);
        await ElementAction.clickAndEnterTextByCss(this.browser, ".resend-otp-main input",otpNo,"Enter OTP");
        let log =  await ElementAction.clickByCss(this.browser,"button[name='Login'] i",'OTP Submit Button').catch(error => {
            console.log('Some Error in it', error);
        });
        this.browser.pause(5000);
    }

    async assertLogoutFromTheApp(){
        await this.logoutFromApp()
        // await this.browser.pause(2000);
        this.browser.frame(0);
        let actualResult = await ElementAction.waitForElementPresentByXpath(this.browser,HomeKeySelector.Login.Password,'Password');
        Assert.isValueEqual(this.browser,actualResult,true,"Verify password field is visible as user logout from the app.")
        this.browser.pause(5000);
    }

    async switchToMID(userDetail){
        await ElementAction.waitForElementPresentByCSS(this.browser,HomeKeySelector.Login.UserMenu,'Merchant User Menu');
        await ElementAction.clickByCss(this.browser,HomeKeySelector.Login.UserMenu,'Merchant User Menu');
        await this.searchMid(userDetail.mid);
        await ElementAction.clickByCss(this.browser,HomeKeySelector.Login.MID,'Select MID');

    }

    async searchMid(mid:string){
        await ElementAction.waitForElementPresentByCSS(this.browser,HomeKeySelector.Login.MidSearch,'MID Search Text Box');
        await ElementAction.clickByCss(this.browser,HomeKeySelector.Login.MidSearch,'MID Search Text Box');
        await ElementAction.enterValueByCss(this.browser,HomeKeySelector.Login.MidSearch,mid,'Enter value to MID Search text Box');
    }
}

export enum JoinTeam {
    ACCEPTED = "[id*='accepted']",
    REJECTED ="[id*='rejected]"
}
