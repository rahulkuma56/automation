/*
import { NightwatchBrowser } from 'nightwatch';
import {ElementAction} from "../base/ElementAction";
//import {BasePage, DurationOptionList, TabID} from "../transaction/BasePage";
//import {DashboardKeys, TransactionPaymode} from "../../../apis/ump/home/TransactionPaymode";
import {DataObject} from "../../../apis/bpay/DataObject";
import {Assert} from "../base/Assert";
import {OrderType} from "../../../apis//global/APIFilters";
//import {TransactionSummary} from "../../../apis/ump/transaction/TransactionSummary";
const HomeKeySelector = require('./HomeKeySelector');


export class HomePage {

    //
    // private expectedTotalCountPaymodeHeaderList: string[] = ['PAYMENT MODE', 'NO OF TRANSACTIONS', 'OVERALL %'];
    // private expectedTotalAmountPaymodeHeaderList: string[] = ['PAYMENT MODE', 'AMOUNT', 'OVERALL %'];
    // private expectedSuccessRatePaymodeHeaderList: string[] = ['PAYMENT MODE', 'SUCCESS RATE'];

    ExpectedHeader = {
        TOTAL_COUNT: ['PAYMENT MODE', 'NO OF TRANSACTIONS', 'OVERALL %'],
        TOTAL_AMOUNT: ['PAYMENT MODE', 'AMOUNT', 'OVERALL %'],
        SUCCESS_RATE: ['PAYMENT MODE', 'SUCCESS RATE']
    };

    private expectedData;
    public browser: NightwatchBrowser;

    constructor(browser: NightwatchBrowser) {
       // super(TabID.HOME,browser);
        this.browser = browser;
    }

    async waitForPageToLoad() {
        await ElementAction.waitForPageToLoad(this.browser);
        await this.browser.pause(4000);
    }

    async assertLineChartGraphDisplayed() {
        let isElePresent = await ElementAction.isElementPresentByCSS(this.browser,HomeKeySelector.Home.LineChart ,'Line Chart Graph');
        Assert.isValueEqual(this.browser,isElePresent, true, "Verified Line chart is displayed on Home screen.");
    }

    async assertPieChartGraphDisplayed() {
        let isElePresent = await ElementAction.isElementPresentByCSS(this.browser, HomeKeySelector.Home.PieChart,'Pie Chart Graph');
        Assert.isValueEqual(this.browser,isElePresent, true, "Verified pie chart graph is displayed on Home screen.");
    }


    async getExpectedData(dataObject: DataObject) {
        let object = new TransactionPaymode();
        if (this.expectedData == undefined)
            this.expectedData = await object.getRefactoredData(dataObject,this.browser);
        return this.expectedData;
    }

    async clickOnSeeAllTransactions() {
        let res = await ElementAction.clickByCss(this.browser, HomeKeySelector.Home.SeeAllTransaction, 'See all transaction Button');
        console.log(res);
    }

    async gettNumberOfPayments() {
        let data = await ElementAction.getTextByXpath(this.browser, HomeKeySelector.Home.NumberOfPayment,"Number of Payments");
        return data;

    }


    async getTotalPayments(){
        this.browser.pause(2000);
        let value  = await ElementAction.getAllElementTextByCSSWithoutWait(this.browser, HomeKeySelector.Home.AmountDisplayValue,'Amount Display Value');
        return value;
    }
    async getTotalRefactoredPayments() {
        let value = await this.getTotalPayments();
        let label = await ElementAction.getAllElementTextByXpathWithoutWait(this.browser, "//div[@class='amtDisplay']/../label",'Amount Display Label');
        return label+"\n"+value;
    }

    async assertPaymentsData(dataObject: DataObject) {
        let totalRefactoredPayments = await this.getTotalRefactoredPayments();
        let noOfPayments = await this.gettNumberOfPayments();
        let totalPayment = await this.getTotalPayments();
        await this.getExpectedData(dataObject);
        let transactionExpectedData = await new TransactionSummary(OrderType.TRANSACTION).getRefactoredData(dataObject,this.browser);
        Assert.isValueEqual(this.browser,noOfPayments, "NO. OF PAYMENTS\n" + this.expectedData["NO_OF_PAYMENTS"], "Verified "+noOfPayments +" is displayed as expected '"+"NO. OF PAYMENTS\n" + this.expectedData["NO_OF_PAYMENTS"]+"'.");
        Assert.isValueEqual(this.browser,totalRefactoredPayments, "TOTAL PAYMENTS\n" + this.expectedData["TOTAL_PAYMENTS"], "Verified "+totalRefactoredPayments+" is displayed as expected '"+"TOTAL PAYMENTS\n" + this.expectedData["TOTAL_PAYMENTS"]+"'.");

        // Now verify the data is same as transaction screen data, as there mey be discrepancy as dashboard data is coming from DWS.
        Assert.isValueEqual(this.browser,noOfPayments,  "NO. OF PAYMENTS\n"+transactionExpectedData['totalCount'], "Verified Number of transaction displayed on Dashboard is similar to  Number of transaction on Transaction screen.");
        Assert.isValueEqual(this.browser,totalPayment[0], transactionExpectedData['totalAmount'].replace('\n',''), "Verified total Transaction Amount displayed on Dashboard is similar to  total Transaction Amount on Transaction screen.");
    }


    // async clickOnDurationFilter() {
    //     await ElementAction.moveAndClickByCSS(this.browser, HomeKeySelector.Home.DurationDropDown, 'View Drop Down');
    //     this.browser.pause(1000);
    // }
    //
    // async selectDurationOption(duration: DurationOptionList) {
    //     this.browser.pause(1000);
    //     await ElementAction.clickByCSSWithoutWait(this.browser, '.showList li:nth-child(' + duration + ')', 'Duration Option').catch((err) => {
    //         console.log(err);
    //     });
    // }

    async clickOnViewDropDown() {
        await ElementAction.moveAndClickByCSS(this.browser, HomeKeySelector.Home.ViewDropDown, 'View Drop Down');
        this.browser.pause(1000);
    }


    async selectViewOption(value: Filter) {
        await ElementAction.clickByCss(this.browser, '._3pOW.grid.vertical li:nth-child(' + value + ')', 'View Option').catch((err) => {
            console.log(err);
        });
    }


    async verifyHomeScrenPayModeData(dataObject: DataObject, selectedFilter) {

        let actualPaymodeHeaderList = await ElementAction.getAllElementTextByCSS(this.browser, HomeKeySelector.Home.PaymodeHeaderList,'Paymode Column Names').catch(function (error) {
            console.log('Unable to get Transaction Header.');
            console.log(error);
        });
        console.log("Actual Table  Headers is  :: " + actualPaymodeHeaderList);

        // Fetch the expected data and modify the 'OVERALL %' column data as per user selected filter.
        let expectedData = await this.getExpectedData(dataObject);
        for (let payMode of expectedData) {
            payMode["OVERALL %"] = payMode[DashboardKeys[selectedFilter]]
        }

        this.browser.pause(1000);

        // Fetch the expected data and modify the 'OVERALL %' column data as per user selected filter.
        for (let payMode of expectedData) {
            payMode["OVERALL %"] = payMode[DashboardKeys[selectedFilter]]
        }

        let paymodeDataList = await ElementAction.getAllElementTextByCSS(this.browser, HomeKeySelector.Home.PaymodeDataList,'Paymode Data').catch(function (error) {
            console.log('Unable to get Actual Transaction List.');
            console.log(error);
        });
        console.log("Actual table Data is :: " + paymodeDataList);

        Assert.isValueEqual(this.browser,JSON.stringify(actualPaymodeHeaderList), JSON.stringify(this.ExpectedHeader[selectedFilter]), "Verified actual Paymode headers is '"+JSON.stringify(actualPaymodeHeaderList)+"' as expected '"+JSON.stringify(this.ExpectedHeader[selectedFilter])+"'.");

        // Modify the actul data from index array to header bound array.
        let modifyActualList = [];
        for (let rowIndex = 0; rowIndex < paymodeDataList.length;) {
            let listVal = [];
            for (let header of this.ExpectedHeader[selectedFilter]) {
                listVal[header] = paymodeDataList[rowIndex];
                rowIndex++
            }
            modifyActualList.push(listVal)
        }

        // Modify the order of  expected data as per requirement.
        let modifyExpectedData = [];
        let modifyIndex=0;

            for (let key of Object.keys(OrderedBankMap)) {
                for (let index = 0; index < expectedData.length; index++) {
                if(expectedData[index]['PAYMENT MODE'] == OrderedBankMap[key]){
                    modifyExpectedData[modifyIndex] = expectedData[index];
                    modifyIndex++;
                    break;
                }
            }
        }

        // Compare the expected data with actual data.
        for (let index = 0; index < modifyActualList.length; index++) {
            let row = index + 1;
            for (let key of this.ExpectedHeader[selectedFilter])
                Assert.isValueEqual(this.browser,modifyActualList[index][key], modifyExpectedData[index][key], "Verified '" + key + "' in list '" + row + "' is '"+modifyActualList[index][key]+"' as expected '"+modifyExpectedData[index][key]+"'");
        }
    }
}

const OrderedBankMap = {
    PPI: 'Paytm Wallet',
    UPI: 'UPI',
    PPBL: 'Paytm Payments Bank',
    CC: 'Credit Card',
    DC: 'Debit Card',
    NB: 'Net Banking',
    COD: 'COD',
    EMI: 'EMI',
    BANK_EXPRESS: 'Bank Express',
    IMPS: 'IMPS',
    ATM: 'ATM Card',
    MP_COD: 'Cash on Delivery',
    CASH_COUPON: 'Cash Coupon',
    // PREPAID_CARD: 'Paytm Wallet',
    HYBRID_PAYMENT: 'Hybrid Payment',
    PAYTM_DIGITAL_CREDIT: 'Paytm Postpaid',
    LOYALTY_POINTS: 'Paytm Loyalty Points',
    MULTIPLE_PAYMODES: 'Credit Card, Debit Card, Net Banking'
};

export enum  Filter{
    TOTAL_AMOUNT = 1,
    SUCCESS_RATE = 2,
    TOTAL_COUNT = 3
}






*/
