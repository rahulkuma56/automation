import {NightwatchBrowser} from 'nightwatch';
import {ElementAction} from "../base/ElementAction";
import {Assert} from "../base/Assert";
//import {TabID, TabIDUser, TabLabel, TabLabelUser} from "../transaction/BasePage";
const Selector = require('./HomeKeySelector');


export class NavigationPage {

    public browser: NightwatchBrowser;

    constructor(client: NightwatchBrowser) {
        this.browser = client;
    }
    public static expectedTabsLabel = ['Home','Transactions','Settlements','Refunds','Reports','Payment Links',"Payment Invoices",'My QR Code','API Keys','API Documentation','Settings','Manage Users'];
    public static expectedTabsLabelForUsers = ["Get Started","Activate Account",'Home','Transactions','Settlements','Refunds','Accounts','Payment Links',"Payment Invoices",'My QR Code','API Keys','API Documentation','Settings','Manage Users'];
    private diableElementToolTip = "Activate your account to view this section.";

    clickOnTryItNow() {
        ElementAction.clickByXpath(this.browser,Selector.Navigation.TryItNow , 'Try It Now link');
    }

    navigateToTransaction() {
        ElementAction.clickByCss(this.browser,Selector.Navigation.TransactionTab , "Transaction Page");
        this.browser.pause(100);
    }

    navigateToPaymentLink() {
        return ElementAction.clickByXpath(this.browser,Selector.Navigation.PaymentLinkTab , 'Payment Link Page');
    }

    navigateToSettingsLink() {
        this.browser.pause(5000);
        return ElementAction.clickByXpath(this.browser, Selector.Navigation.SettingsTab, 'Settings Page');
    }

    navigateToSettlementLink() {
        return ElementAction.clickByXpath(this.browser,Selector.Navigation.SettlementTab , 'Settlement Page');
    }

    navigateToPaymentInvoice(){
         ElementAction.clickByXpath(this.browser,Selector.Navigation.PaymentInvoice , 'Payment Invoice');
         ElementAction.waitForPageToLoad(this.browser);
        this.browser.pause(3000);
    }

    navigateToRefund() {
        this.browser.pause(10);
        ElementAction.clickByXpath(this.browser,Selector.Navigation.RefundTab , "Refund tab");
        this.browser.pause(100);
    }

    async navigateToRefundHandleToolTip() {
        this.browser.pause(100);
        let isToolTipPresent = await ElementAction.isElementPresentByCSS(this.browser,"[class='_uiYD  ']" , "Toool TiP");
        this.browser.pause(1000);
        if(isToolTipPresent)
            ElementAction.clickByCss(this.browser,"[class='_uiYD  ']" , "Toool TiP");
        ElementAction.clickByXpath(this.browser,Selector.Navigation.RefundTab , "Refund tab");
        this.browser.pause(100);
    }




    navigateToReport() {
        this.browser.pause(10);
        ElementAction.clickByXpath(this.browser,Selector.Navigation.ReportTab , "Report tab");
        this.browser.pause(100);
    }

    navigateToAPIKey(){
        ElementAction.clickByXpath(this.browser,Selector.Navigation.APIKeyTab , 'API Key Page');
        this.browser.pause(1000);
    }

    navigateToqr() {
        return ElementAction.clickByXpath(this.browser,Selector.Navigation.QRCodeTab , 'QR Code Page');
    }

    navigateToManageUsers() {
        return ElementAction.clickByXpath(this.browser,Selector.Navigation.ManageUser,'Manage Users');
    }

    navigateToApiDocumentation(){
        return ElementAction.clickByXpath(this.browser,Selector.Navigation.ApiDocumentation,'Api Documentation');
    }
    assertComingSoonPresent(msg: string) {
        ElementAction.assertElementPresent(this.browser,Selector.Navigation.ComingSoonLink , "Verify 'Coming Soon' link is present " + msg);
    }

    assertTryItNowPresent(msg: string) {
        ElementAction.assertElementPresent(this.browser,Selector.Navigation.TryItNow, "Verify 'Try It Now' link is present " + msg);
    }

    assertPricingLinkDisplayed(msg: string) {
        return ElementAction.assertElementPresent(this.browser,Selector.Navigation.PricingLink, "Verify 'Pricing link' is displayed " + msg);
    }

    async allNavigationTabTextVerificationTest(expectedTabLabels){
        await ElementAction.waitForElementPresentByCSS(this.browser,Selector.Navigation.AllTabText,'Navigation Tab Label');
        this.browser.pause(1000);
        let tabText =  await ElementAction.getAllElementTextByCSS(this.browser,Selector.Navigation.AllTabText,'Navigation Tab Label');
        Assert.isValueEqual(this.browser,JSON.stringify(tabText),JSON.stringify(expectedTabLabels),'Verify all tabs label is displayed as expected :: '+JSON.stringify(tabText));
    }

   /* async assertTabNavigation(){
        for(let key in TabID){
            if(TabLabel[key] == 'API Keys'){
                let className = await ElementAction.getAttributeValueByXpath(this.browser,"//label[text()='"+TabLabel[key]+"']","class","Settings Tab");
                await ElementAction.scrollToElement(this.browser,className,10);
            }
            await ElementAction.moveandClickByXpath(this.browser,"//label[text()='"+TabLabel[key]+"']",TabLabel[key]);
            let tabText = await ElementAction.getTextByCSS(this.browser,".wrapper-content "+TabID[key] +" h1",TabLabel[key]);
            Assert.isValueEqual(this.browser,tabText,TabLabel[key],"Verify user successfully navigate to '"+TabLabel[key]+"' tab.");
        }
    }

*/
    async presenceOfActivateAccountButton(){
        let tabs = ['Home','Transactions','Settlements','Refunds','API Keys'];
        for(let i=0;i<tabs.length;i++) {
            await ElementAction.waitForElementPresentByXpath(this.browser,"//a//label[text()='"+tabs[i]+"']",tabs[i]);
            this.browser.pause(500);
            await ElementAction.clickByXpathWithoutWait(this.browser,"//a//label[text()='"+tabs[i]+"']",tabs[i]);
            this.browser.pause(500);
            await ElementAction.isElementPresentByXpath(this.browser,"//a//button[text()='Activate account']",tabs[i])
            // console.log("Button Activate account is Present for"+tabs[i]);

        }
    }

    async disabilityOfElement() {
        let tabs = ['Settings','Manage Users'];
        for(let i=0;i<tabs.length;i++) {
            await ElementAction.waitForElementPresentByXpath(this.browser, "//label[text()='" + tabs[i] + "']//parent::span//parent::a", tabs[i]);
            if(tabs[i] == 'Settings') {
                let className = await ElementAction.getAttributeValueByXpath(this.browser, "//label[text()='" + tabs[i]  + "']", "class", "Settings Tab");
                await ElementAction.scrollToElement(this.browser, className, 10);
            }
            let attValue = await ElementAction.getAttributeValueByXpath(this.browser,"//label[text()='" + tabs[i] + "']//parent::span//parent::a","href","Disable"+tabs[i]);
            Assert.isValueEqual(this.browser,attValue,"javascript:void(0)","Verified the tab '"+tabs[i]+"' is disabled.");
            await ElementAction.moveToElement(this.browser,"//label[text()='" + tabs[i] + "']//parent::span//parent::a",tabs[i]);
            this.browser.pause(1000);
            let eleToolTip = await ElementAction.getTextByXpath(this.browser,"//label[text()='" + tabs[i] + "']//parent::span//following-sibling::div//p",tabs[i]+" Tool Tip");
            Assert.isValueEqual(this.browser,eleToolTip,this.diableElementToolTip,"Verified for '"+tabs[i]+"' the tool tip is displayed." )
        }
    }

/*

    async assertTabNavigationForUser(){
        for(let key in TabIDUser){
            await ElementAction.waitForElementPresentByXpath(this.browser,"//label[text()='"+TabLabelUser[key]+"']",TabLabelUser[key]);
            this.browser.pause(1000);
            await ElementAction.clickByXpathWithoutWait(this.browser,"//label[text()='"+TabLabelUser[key]+"']",TabLabelUser[key]);
            await ElementAction.waitForElementPresentByCSS(this.browser,".wrapper-content "+TabIDUser[key] +" h1",TabIDUser[key]);
            this.browser.pause(1000);
            let tabText = await ElementAction.getAllElementTextByCSSWithoutWait(this.browser,".wrapper-content "+TabIDUser[key] +" h1",TabIDUser[key]);
            if(TabLabelUser[key]=='Get Started')
                Assert.isValueEqual(this.browser,tabText,"Select a product to get started","Verify user successfully navigate to '"+TabLabelUser[key]+"' tab.");
            else if(TabLabelUser[key]=='Activate Account')
                Assert.isValueEqual(this.browser, tabText, "Activate your\nPaytm for Business Account", "Verify user successfully navigate to '" + TabLabelUser[key] + "' tab.")
            else if(TabLabelUser[key]=='API Keys') {
                this.browser.pause(2000);
                Assert.isValueEqual(this.browser, tabText, TabLabelUser[key], "Verify user successfully navigate to '" + TabLabelUser[key] + "' tab.")
                await ElementAction.isElementPresentByXpath(this.browser,"//div[@id='appApi']//button[text()='Generate now']",TabLabelUser[key])
                this.browser.pause(1000);
                if(TabLabelUser[key] == 'API Keys') {
                    let className = await ElementAction.getAttributeValueByXpath(this.browser, "//label[text()='" + TabLabelUser[key] + "']", "class", "Settings Tab");
                    await ElementAction.scrollToElement(this.browser, className, 10);
                }
            }
            else
                Assert.isValueEqual(this.browser,tabText,TabLabelUser[key],"Verify user successfully navigate to '"+TabLabelUser[key]+"' tab.");
        }
    }
*/

    async assertNonMerchantData(){
        await this.allNavigationTabTextVerificationTest(NavigationPage.expectedTabsLabelForUsers);
       // await this.assertTabNavigationForUser();
        await this.presenceOfActivateAccountButton();
        await this.disabilityOfElement();
    }
}
