module.exports = {

    Home: {
        'LineChart':'.heading-wrapper + div canvas.chartjs-render-monitor',
        'PieChart':'.justify-center canvas.chartjs-render-monitor',
        'SeeAllTransaction':"#app a.btn-primary",
        'NumberOfPayment':"//div[@id='app']//div[@class='wrapper-content max-wrap']//p/..",
        'AmountDisplayValue':"#app .amtDisplay",
        'AmountDisplayLabel':"//div[@class='amtDisplay']/../label",
        'DurationDropDown':'ul.nowrap svg',
        'ViewDropDown':'div.ffilter div svg',
        'PaymodeHeaderList':'.c-wrap table tr th',
        'PaymodeDataList':'.c-wrap table tbody tr td',
    },
    Login:{
        'UserName':"//*[@name='username']",
        'Password':"//*[@name='password']",
        'SignInBtn':"//button[@class='btn btn-primary ng-binding']",
        'UserMenu':"div.justify-end div",
        'LogoutLink':"div.btn-primary",
        'TestToggleBtn':"ul.hiddendefult label div",
        'MidSearch':'#merchantSearch',
        'MID':'.hiddendefult [data-ump-desktop="true"] li',
    },
    Navigation:{
        //"TryItNow":"//*[@class='linkInfo']",
        "TryItNow":"//header//*[@class='linkInfo']",  //Locator updated for staging
        "TransactionTab":"#app .left-menu a[href='/next/transactions']",
        "PaymentLinkTab":"//a[@href='/next/payment-link']",
        "SettingsTab":"//a[@href='/next/account-settings']",
        "SettlementTab":"//a[@href='/next/settlements']",
        "PaymentInvoice":"//a[@href='/next/payment-invoice']",
        "RefundTab":"//label[contains(text(),'Refunds')]",
        "ReportTab":"//label[contains(text(),'Reports')]",
        "APIKeyTab":"//a[@href='/next/apikeys']",
        "ApiDocumentation" : "//label[contains(text(),'API Documentation')]",
        "QRCodeTab":"//a[@href='/next/qr-details']",
        "ManageUser":"//a[@href='/next/manage-users']",
        "ComingSoonLink":"//*[@class='coming-soon-icon']",
        "PricingLink":"//*[@href='https://business.paytm.com/pricing']",
        "AllTabText":'.smooth-scroll li label',
    }
}


