"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var ElementAction_1 = require("../base/ElementAction");
var Assert_1 = require("../base/Assert");
//import {TabID, TabIDUser, TabLabel, TabLabelUser} from "../transaction/BasePage";
var Selector = require('./HomeKeySelector');
var NavigationPage = /** @class */ (function () {
    function NavigationPage(client) {
        this.diableElementToolTip = "Activate your account to view this section.";
        this.browser = client;
    }
    NavigationPage.prototype.clickOnTryItNow = function () {
        ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.TryItNow, 'Try It Now link');
    };
    NavigationPage.prototype.navigateToTransaction = function () {
        ElementAction_1.ElementAction.clickByCss(this.browser, Selector.Navigation.TransactionTab, "Transaction Page");
        this.browser.pause(100);
    };
    NavigationPage.prototype.navigateToPaymentLink = function () {
        return ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.PaymentLinkTab, 'Payment Link Page');
    };
    NavigationPage.prototype.navigateToSettingsLink = function () {
        this.browser.pause(5000);
        return ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.SettingsTab, 'Settings Page');
    };
    NavigationPage.prototype.navigateToSettlementLink = function () {
        return ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.SettlementTab, 'Settlement Page');
    };
    NavigationPage.prototype.navigateToPaymentInvoice = function () {
        ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.PaymentInvoice, 'Payment Invoice');
        ElementAction_1.ElementAction.waitForPageToLoad(this.browser);
        this.browser.pause(3000);
    };
    NavigationPage.prototype.navigateToRefund = function () {
        this.browser.pause(10);
        ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.RefundTab, "Refund tab");
        this.browser.pause(100);
    };
    NavigationPage.prototype.navigateToRefundHandleToolTip = function () {
        return __awaiter(this, void 0, void 0, function () {
            var isToolTipPresent;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.browser.pause(100);
                        return [4 /*yield*/, ElementAction_1.ElementAction.isElementPresentByCSS(this.browser, "[class='_uiYD  ']", "Toool TiP")];
                    case 1:
                        isToolTipPresent = _a.sent();
                        this.browser.pause(1000);
                        if (isToolTipPresent)
                            ElementAction_1.ElementAction.clickByCss(this.browser, "[class='_uiYD  ']", "Toool TiP");
                        ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.RefundTab, "Refund tab");
                        this.browser.pause(100);
                        return [2 /*return*/];
                }
            });
        });
    };
    NavigationPage.prototype.navigateToReport = function () {
        this.browser.pause(10);
        ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.ReportTab, "Report tab");
        this.browser.pause(100);
    };
    NavigationPage.prototype.navigateToAPIKey = function () {
        ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.APIKeyTab, 'API Key Page');
        this.browser.pause(1000);
    };
    NavigationPage.prototype.navigateToqr = function () {
        return ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.QRCodeTab, 'QR Code Page');
    };
    NavigationPage.prototype.navigateToManageUsers = function () {
        return ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.ManageUser, 'Manage Users');
    };
    NavigationPage.prototype.navigateToApiDocumentation = function () {
        return ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Navigation.ApiDocumentation, 'Api Documentation');
    };
    NavigationPage.prototype.assertComingSoonPresent = function (msg) {
        ElementAction_1.ElementAction.assertElementPresent(this.browser, Selector.Navigation.ComingSoonLink, "Verify 'Coming Soon' link is present " + msg);
    };
    NavigationPage.prototype.assertTryItNowPresent = function (msg) {
        ElementAction_1.ElementAction.assertElementPresent(this.browser, Selector.Navigation.TryItNow, "Verify 'Try It Now' link is present " + msg);
    };
    NavigationPage.prototype.assertPricingLinkDisplayed = function (msg) {
        return ElementAction_1.ElementAction.assertElementPresent(this.browser, Selector.Navigation.PricingLink, "Verify 'Pricing link' is displayed " + msg);
    };
    NavigationPage.prototype.allNavigationTabTextVerificationTest = function (expectedTabLabels) {
        return __awaiter(this, void 0, void 0, function () {
            var tabText;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByCSS(this.browser, Selector.Navigation.AllTabText, 'Navigation Tab Label')];
                    case 1:
                        _a.sent();
                        this.browser.pause(1000);
                        return [4 /*yield*/, ElementAction_1.ElementAction.getAllElementTextByCSS(this.browser, Selector.Navigation.AllTabText, 'Navigation Tab Label')];
                    case 2:
                        tabText = _a.sent();
                        Assert_1.Assert.isValueEqual(this.browser, JSON.stringify(tabText), JSON.stringify(expectedTabLabels), 'Verify all tabs label is displayed as expected :: ' + JSON.stringify(tabText));
                        return [2 /*return*/];
                }
            });
        });
    };
    /* async assertTabNavigation(){
         for(let key in TabID){
             if(TabLabel[key] == 'API Keys'){
                 let className = await ElementAction.getAttributeValueByXpath(this.browser,"//label[text()='"+TabLabel[key]+"']","class","Settings Tab");
                 await ElementAction.scrollToElement(this.browser,className,10);
             }
             await ElementAction.moveandClickByXpath(this.browser,"//label[text()='"+TabLabel[key]+"']",TabLabel[key]);
             let tabText = await ElementAction.getTextByCSS(this.browser,".wrapper-content "+TabID[key] +" h1",TabLabel[key]);
             Assert.isValueEqual(this.browser,tabText,TabLabel[key],"Verify user successfully navigate to '"+TabLabel[key]+"' tab.");
         }
     }
 
 */
    NavigationPage.prototype.presenceOfActivateAccountButton = function () {
        return __awaiter(this, void 0, void 0, function () {
            var tabs, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        tabs = ['Home', 'Transactions', 'Settlements', 'Refunds', 'API Keys'];
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < tabs.length)) return [3 /*break*/, 6];
                        return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByXpath(this.browser, "//a//label[text()='" + tabs[i] + "']", tabs[i])];
                    case 2:
                        _a.sent();
                        this.browser.pause(500);
                        return [4 /*yield*/, ElementAction_1.ElementAction.clickByXpathWithoutWait(this.browser, "//a//label[text()='" + tabs[i] + "']", tabs[i])];
                    case 3:
                        _a.sent();
                        this.browser.pause(500);
                        return [4 /*yield*/, ElementAction_1.ElementAction.isElementPresentByXpath(this.browser, "//a//button[text()='Activate account']", tabs[i])
                            // console.log("Button Activate account is Present for"+tabs[i]);
                        ];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        i++;
                        return [3 /*break*/, 1];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    NavigationPage.prototype.disabilityOfElement = function () {
        return __awaiter(this, void 0, void 0, function () {
            var tabs, i, className, attValue, eleToolTip;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        tabs = ['Settings', 'Manage Users'];
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < tabs.length)) return [3 /*break*/, 10];
                        return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByXpath(this.browser, "//label[text()='" + tabs[i] + "']//parent::span//parent::a", tabs[i])];
                    case 2:
                        _a.sent();
                        if (!(tabs[i] == 'Settings')) return [3 /*break*/, 5];
                        return [4 /*yield*/, ElementAction_1.ElementAction.getAttributeValueByXpath(this.browser, "//label[text()='" + tabs[i] + "']", "class", "Settings Tab")];
                    case 3:
                        className = _a.sent();
                        return [4 /*yield*/, ElementAction_1.ElementAction.scrollToElement(this.browser, className, 10)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5: return [4 /*yield*/, ElementAction_1.ElementAction.getAttributeValueByXpath(this.browser, "//label[text()='" + tabs[i] + "']//parent::span//parent::a", "href", "Disable" + tabs[i])];
                    case 6:
                        attValue = _a.sent();
                        Assert_1.Assert.isValueEqual(this.browser, attValue, "javascript:void(0)", "Verified the tab '" + tabs[i] + "' is disabled.");
                        return [4 /*yield*/, ElementAction_1.ElementAction.moveToElement(this.browser, "//label[text()='" + tabs[i] + "']//parent::span//parent::a", tabs[i])];
                    case 7:
                        _a.sent();
                        this.browser.pause(1000);
                        return [4 /*yield*/, ElementAction_1.ElementAction.getTextByXpath(this.browser, "//label[text()='" + tabs[i] + "']//parent::span//following-sibling::div//p", tabs[i] + " Tool Tip")];
                    case 8:
                        eleToolTip = _a.sent();
                        Assert_1.Assert.isValueEqual(this.browser, eleToolTip, this.diableElementToolTip, "Verified for '" + tabs[i] + "' the tool tip is displayed.");
                        _a.label = 9;
                    case 9:
                        i++;
                        return [3 /*break*/, 1];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    /*
    
        async assertTabNavigationForUser(){
            for(let key in TabIDUser){
                await ElementAction.waitForElementPresentByXpath(this.browser,"//label[text()='"+TabLabelUser[key]+"']",TabLabelUser[key]);
                this.browser.pause(1000);
                await ElementAction.clickByXpathWithoutWait(this.browser,"//label[text()='"+TabLabelUser[key]+"']",TabLabelUser[key]);
                await ElementAction.waitForElementPresentByCSS(this.browser,".wrapper-content "+TabIDUser[key] +" h1",TabIDUser[key]);
                this.browser.pause(1000);
                let tabText = await ElementAction.getAllElementTextByCSSWithoutWait(this.browser,".wrapper-content "+TabIDUser[key] +" h1",TabIDUser[key]);
                if(TabLabelUser[key]=='Get Started')
                    Assert.isValueEqual(this.browser,tabText,"Select a product to get started","Verify user successfully navigate to '"+TabLabelUser[key]+"' tab.");
                else if(TabLabelUser[key]=='Activate Account')
                    Assert.isValueEqual(this.browser, tabText, "Activate your\nPaytm for Business Account", "Verify user successfully navigate to '" + TabLabelUser[key] + "' tab.")
                else if(TabLabelUser[key]=='API Keys') {
                    this.browser.pause(2000);
                    Assert.isValueEqual(this.browser, tabText, TabLabelUser[key], "Verify user successfully navigate to '" + TabLabelUser[key] + "' tab.")
                    await ElementAction.isElementPresentByXpath(this.browser,"//div[@id='appApi']//button[text()='Generate now']",TabLabelUser[key])
                    this.browser.pause(1000);
                    if(TabLabelUser[key] == 'API Keys') {
                        let className = await ElementAction.getAttributeValueByXpath(this.browser, "//label[text()='" + TabLabelUser[key] + "']", "class", "Settings Tab");
                        await ElementAction.scrollToElement(this.browser, className, 10);
                    }
                }
                else
                    Assert.isValueEqual(this.browser,tabText,TabLabelUser[key],"Verify user successfully navigate to '"+TabLabelUser[key]+"' tab.");
            }
        }
    */
    NavigationPage.prototype.assertNonMerchantData = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.allNavigationTabTextVerificationTest(NavigationPage.expectedTabsLabelForUsers)];
                    case 1:
                        _a.sent();
                        // await this.assertTabNavigationForUser();
                        return [4 /*yield*/, this.presenceOfActivateAccountButton()];
                    case 2:
                        // await this.assertTabNavigationForUser();
                        _a.sent();
                        return [4 /*yield*/, this.disabilityOfElement()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    NavigationPage.expectedTabsLabel = ['Home', 'Transactions', 'Settlements', 'Refunds', 'Reports', 'Payment Links', "Payment Invoices", 'My QR Code', 'API Keys', 'API Documentation', 'Settings', 'Manage Users'];
    NavigationPage.expectedTabsLabelForUsers = ["Get Started", "Activate Account", 'Home', 'Transactions', 'Settlements', 'Refunds', 'Accounts', 'Payment Links', "Payment Invoices", 'My QR Code', 'API Keys', 'API Documentation', 'Settings', 'Manage Users'];
    return NavigationPage;
}());
exports.NavigationPage = NavigationPage;
