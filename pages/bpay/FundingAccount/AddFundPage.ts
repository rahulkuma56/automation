import {NightwatchBrowser} from 'nightwatch';
const Selector = require('./homeKeySelector');
import {ElementAction} from "../../bpay/base/ElementAction";
import {Assert} from "../../bpay/base/Assert";
import {SubwalletDetailApi} from "../../../apis/bpay/FundingAccount/SubwalletDetailApi";

let beforeFundAdditionSubwalletBalance;
let beforeFundAdditionSubwalletBalanceApi;

export class AddFundPage {

    public browser: NightwatchBrowser;
    private businessWalletApi: SubwalletDetailApi;

    constructor(client: NightwatchBrowser) {
        this.browser = client;
        this.businessWalletApi = new SubwalletDetailApi();
    }

   /* // Search the sub wallet by account name.
    async searchSubWalletByAccountname(accountName){
        await ElementAction.waitForElementPresentByXpath(this.browser, Selector.Home.SeachBox, 'Wait for the search box');
        await ElementAction.enterValueByXpath(this.browser, Selector.Home.SeachBox, accountName, "Enter subwallet name "+accountName+" in the search box");
        //beforeFundAdditionSubwalletBalance = await ElementAction.getTextByXpath(this.browser, Selector.Home.SubWalletBalance, 'Get subwallet balance before fund addition');
        console.log(accountName+" is searched.");

    }
*/
  /*  //Assert subwallet record
    async assertSubwalletByAccountName(accountName){
        // let flag = await  ElementAction.isElementPresentByXpath(this.browser,Selector.Home.AccountName.replace("value",accountName), accountName+" row is displayed");
        await Assert.isXpathElePresent(this.browser, Selector.Home.AccountName.replace("value",accountName), true,accountName + " is present","accountName");
    }*/

   /* async searchSubWallet(userDetails){
        await ElementAction.waitForElementPresentByXpath(this.browser, Selector.Home.SeachBox, 'Wait for the search box');
        await ElementAction.enterValueByXpath(this.browser, Selector.Home.SeachBox, '1212', 'Enter subwallet name in the search box');
        beforeFundAdditionSubwalletBalance = await ElementAction.getTextByXpath(this.browser, Selector.Home.SubWalletBalance, 'Get subwallet balance before fund addition');
        console.log("UIBEFORE:"+beforeFundAdditionSubwalletBalance);
        beforeFundAdditionSubwalletBalanceApi = await this.businessWalletApi.getSubwalletBalance(userDetails);
        console.log("APIBEFORE:"+beforeFundAdditionSubwalletBalanceApi);
    }*/

   /* async clickAddFundButton(){
        await ElementAction.clickByXpath(this.browser, Selector.Home.AddFund, 'Click on the Add Fund button');
    }*/

    async addFund(amount){
        await ElementAction.enterValueByXpath(this.browser, Selector.Home.Amount,amount, 'Enter amount to be added in the subwallet');
        await ElementAction.clickByXpath(this.browser, Selector.Home.AddFundButton, 'Click on the Add fund button');
    }

   /* async assertSubwalletBalance(userDetails){
        await ElementAction.waitForElementPresentByXpath(this.browser, Selector.Home.SeachBox, 'Wait for the search box');
        await ElementAction.enterValueByXpath(this.browser, Selector.Home.SeachBox, '1212', 'Enter subwallet name in the search box');
        let afterFundAdditionSubwalletBalance = await ElementAction.getTextByXpath(this.browser, Selector.Home.SubWalletBalance, 'Get subwallet balance before fund addition');
        let actualBalance = parseInt("beforeFundAdditionSubwalletBalance", 0);

        console.log("UIAFTERACTUAL"+ actualBalance)

        let afterFundAdditionSubwalletBalanceApi = await this.businessWalletApi.getSubwalletBalance(userDetails);
        let expectedBalance = Number(beforeFundAdditionSubwalletBalanceApi) + Number(afterFundAdditionSubwalletBalanceApi);

        console.log("APIAFTERACTUAL"+ expectedBalance)

        Assert.isValueEqual(this.browser,actualBalance,expectedBalance, "Verified actual link page headers is '"+actualBalance+"' as expected '"+expectedBalance+"'.");

    }*/



}