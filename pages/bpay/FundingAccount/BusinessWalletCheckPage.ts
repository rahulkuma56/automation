import {NightwatchBrowser} from 'nightwatch';
const Selector = require('./homeKeySelector');
import {ElementAction} from "../../bpay/base/ElementAction";
import {Assert} from "../../bpay/base/Assert";
import {BusinessWalletBalanceApI} from "../../../apis/bpay/FundingAccount/BusinessWalletBalanceApI";
import {MerchantProfileDetailApi} from "../../../apis/bpay/FundingAccount/MerchantProfileDetailApi";
import {NumberUtil} from "../../../pages/util/NumberUtil";
import {FileUtil} from "../../util/FileUtil";

export class businessWalletCheckPage {

    public browser: NightwatchBrowser;
    private businessWalletApi: BusinessWalletBalanceApI;
    private numberUtil: NumberUtil;
    private merchantProfileDetailApi: MerchantProfileDetailApi;

    constructor(client: NightwatchBrowser) {
        this.browser = client;
        this.businessWalletApi = new BusinessWalletBalanceApI();
        this.merchantProfileDetailApi = new MerchantProfileDetailApi();
    }

    /*async assertBusinessWalletBalance(userDetails) {
       let actualBusinessWalletBalance = await ElementAction.getElementTextByIndex(this.browser, Selector.Home.businessWalletBalance, 2,  "Assert business wallet balance" );
        let expectedBusinessWalletBalance = await this.businessWalletApi.getBusinessWalletBalance(userDetails);
        Assert.isValueEqual(this.browser,actualBusinessWalletBalance,expectedBusinessWalletBalance, "Verified actual link page headers is '"+actualBusinessWalletBalance+"' as expected '"+expectedBusinessWalletBalance+"'.");
        this.browser.pause(10);
    }
    async assertMerchantAccountNoAndIFSC(userDetails) {
        let actualMerchantAccountNo = await ElementAction.getElementTextByIndex(this.browser, Selector.Home.MerchantAccountNo, 0, "Assert business wallet balance" );
        let actualMerchantIFSC = await ElementAction.getElementTextByIndex(this.browser, Selector.Home.MerchantAccountNo, 1, "Assert business wallet balance" );
        await console.log("Expected: Merchant Account no and IFSC are: "+actualMerchantAccountNo+"     "+actualMerchantIFSC);
        let trimAcc = new String(actualMerchantAccountNo).match(/\d+/g);
        let expectedMerchantAccountNo = await this.merchantProfileDetailApi.getVAN(userDetails);
        Assert.isValueEqual(this.browser,trimAcc[0],expectedMerchantAccountNo, "Verified actual bank account no is '"+trimAcc[0]+"' as expected '"+expectedMerchantAccountNo+"'.");
        this.browser.pause(10);
    }
*/
}