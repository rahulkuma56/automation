"use strict";
exports.__esModule = true;
var Selector = require('./homeKeySelector');
var BusinessWalletBalanceApI_1 = require("../../../apis/bpay/FundingAccount/BusinessWalletBalanceApI");
var MerchantProfileDetailApi_1 = require("../../../apis/bpay/FundingAccount/MerchantProfileDetailApi");
var businessWalletCheckPage = /** @class */ (function () {
    function businessWalletCheckPage(client) {
        this.browser = client;
        this.businessWalletApi = new BusinessWalletBalanceApI_1.BusinessWalletBalanceApI();
        this.merchantProfileDetailApi = new MerchantProfileDetailApi_1.MerchantProfileDetailApi();
    }
    return businessWalletCheckPage;
}());
exports.businessWalletCheckPage = businessWalletCheckPage;
