import {NightwatchBrowser} from 'nightwatch';
const Selector = require('./homeKeySelector');
import {ElementAction} from "../../bpay/base/ElementAction";
import {Assert} from "../../bpay/base/Assert";
import {BusinessWalletBalanceApI} from "../../../apis/bpay/FundingAccount/BusinessWalletBalanceApI";

export class CreateDisbursalWalletPage{

    public browser: NightwatchBrowser;

    constructor(client: NightwatchBrowser) {
        this.browser = client;
    }

     async clickOnCreateNewDisbursalWallet(){
        await ElementAction.clickByCss(this.browser, Selector.Home.createNewDisbursalWallet, "Click on Create new disbursal wallet button");
    }

     async enterAccountNameAndClickCreateAccount(){
        await ElementAction.enterValueByXpath(this.browser,Selector.Home.AccountNameTextBox, '"AutomationSubWallet"'+ new Date().getTime(), 'Entering sub wallet name');
        await ElementAction.clickByCss(this.browser, Selector.Home.CreateAccountButton, 'Click on create account button');
    }

    async clickOnSaveButton(){
        await  ElementAction.waitForPageToLoad(this.browser);
        await ElementAction.waitForElementPresentByXpath(this.browser, Selector.Home.SaveButton, 'Wait Untill save button is visible');
        await ElementAction.clickByXpath(this.browser, Selector.Home.SaveButton, 'Click on save button');
    }





}