"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var SubwalletDetailApi_1 = require("../../../apis/bpay/FundingAccount/SubwalletDetailApi");
var Selector = require('./homeKeySelector');
var ElementAction_1 = require("../../bpay/base/ElementAction");
var Assert_1 = require("../../bpay/base/Assert");
var BusinessWalletBalanceApI_1 = require("../../../apis/bpay/FundingAccount/BusinessWalletBalanceApI");
var MerchantProfileDetailApi_1 = require("../../../apis/bpay/FundingAccount/MerchantProfileDetailApi");
var FundingAccountPage = /** @class */ (function () {
    function FundingAccountPage(client) {
        this.browser = client;
        this.subwalletDetailApi = new SubwalletDetailApi_1.SubwalletDetailApi();
        this.businessWalletApi = new BusinessWalletBalanceApI_1.BusinessWalletBalanceApI();
        this.merchantProfileDetailApi = new MerchantProfileDetailApi_1.MerchantProfileDetailApi();
    }
    FundingAccountPage.prototype.assertBusinessWalletBalance = function (userDetails) {
        return __awaiter(this, void 0, void 0, function () {
            var actualBusinessWalletBalance, expectedBusinessWalletBalance;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.getElementTextByIndex(this.browser, Selector.Home.businessWalletBalance, 2, "Assert business wallet balance")];
                    case 1:
                        actualBusinessWalletBalance = _a.sent();
                        return [4 /*yield*/, console.log("actualBusinessWalletBalance is: " + actualBusinessWalletBalance)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.businessWalletApi.getBusinessWalletBalance(userDetails)];
                    case 3:
                        expectedBusinessWalletBalance = _a.sent();
                        return [4 /*yield*/, console.log("expectedBusinessWalletBalance is: " + expectedBusinessWalletBalance)];
                    case 4:
                        _a.sent();
                        Assert_1.Assert.isValueEqual(this.browser, actualBusinessWalletBalance, expectedBusinessWalletBalance, "Verified actual link page headers is '" + actualBusinessWalletBalance + "' as expected '" + expectedBusinessWalletBalance + "'.");
                        this.browser.pause(10);
                        return [2 /*return*/];
                }
            });
        });
    };
    //Assert merchant Account No (VAN)
    FundingAccountPage.prototype.assertMerchantAccountNo = function (userDetails) {
        return __awaiter(this, void 0, void 0, function () {
            var actualMerchantAccountNo, actualMerchantIFSC, trimAcc, expectedMerchantAccountNo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.getElementTextByIndex(this.browser, Selector.Home.MerchantAccountNo, 0, "Assert business wallet balance")];
                    case 1:
                        actualMerchantAccountNo = _a.sent();
                        return [4 /*yield*/, ElementAction_1.ElementAction.getElementTextByIndex(this.browser, Selector.Home.MerchantAccountNo, 1, "Assert business wallet balance")];
                    case 2:
                        actualMerchantIFSC = _a.sent();
                        return [4 /*yield*/, console.log("Expected: Merchant Account no and IFSC are: " + actualMerchantAccountNo + "     " + actualMerchantIFSC)];
                    case 3:
                        _a.sent();
                        trimAcc = new String(actualMerchantAccountNo).match(/\d+/g);
                        return [4 /*yield*/, this.merchantProfileDetailApi.getVAN(userDetails)];
                    case 4:
                        expectedMerchantAccountNo = _a.sent();
                        Assert_1.Assert.isValueEqual(this.browser, trimAcc[0], expectedMerchantAccountNo, "Verified actual bank account no is '" + trimAcc[0] + "' as expected '" + expectedMerchantAccountNo + "'.");
                        this.browser.pause(10);
                        return [2 /*return*/];
                }
            });
        });
    };
    FundingAccountPage.prototype.clickOnCreateNewDisbursalWallet = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.clickByCss(this.browser, Selector.Home.createNewDisbursalWallet, "Click on Create new disbursal wallet button")];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // Search the sub wallet by account name.
    FundingAccountPage.prototype.searchSubWalletByAccountname = function (accountName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByXpath(this.browser, Selector.Home.SeachBox, 'Wait for the search box')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, ElementAction_1.ElementAction.enterValueByXpath(this.browser, Selector.Home.SeachBox, accountName, "Enter subwallet name " + accountName + " in the search box")];
                    case 2:
                        _a.sent();
                        //beforeFundAdditionSubwalletBalance = await ElementAction.getTextByXpath(this.browser, Selector.Home.SubWalletBalance, 'Get subwallet balance before fund addition');
                        console.log(accountName + " is searched.");
                        return [2 /*return*/];
                }
            });
        });
    };
    //Assert subwallet record
    FundingAccountPage.prototype.assertSubwalletByAccountName = function (accountName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByXpath(this.browser, Selector.Home.AccountName.replace("value", accountName), accountName)];
                    case 1:
                        _a.sent();
                        // let flag = await  ElementAction.isElementPresentByXpath(this.browser,Selector.Home.AccountName.replace("value",accountName), accountName+" row is displayed");
                        return [4 /*yield*/, Assert_1.Assert.isXpathElePresent(this.browser, Selector.Home.AccountName.replace("value", accountName), true, accountName + " is present", "accountName")];
                    case 2:
                        // let flag = await  ElementAction.isElementPresentByXpath(this.browser,Selector.Home.AccountName.replace("value",accountName), accountName+" row is displayed");
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //Search sub wallet by account name
    FundingAccountPage.prototype.searchSubWallet = function (accountName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.waitForElementPresentByXpath(this.browser, Selector.Home.SeachBox, 'Wait for the search box')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, ElementAction_1.ElementAction.enterValueByXpath(this.browser, Selector.Home.SeachBox, accountName, 'Enter subwallet name ' + accountName + 'in the search box')];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.assertSubwalletByAccountName(accountName)];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //click add fund button
    FundingAccountPage.prototype.clickAddFundButton = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ElementAction_1.ElementAction.clickByXpath(this.browser, Selector.Home.AddFund, 'Click on the Add Fund button')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //Verify sub wallet amount
    FundingAccountPage.prototype.assertSubwalletBalance = function (userDetails, subWalletAccountName) {
        return __awaiter(this, void 0, void 0, function () {
            var ApiBalance, actualBalance;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.subwalletDetailApi.getSubwalletBalance(userDetails, subWalletAccountName)];
                    case 1:
                        ApiBalance = _a.sent();
                        return [4 /*yield*/, ElementAction_1.ElementAction.getTextByCSS(this.browser, Selector.Home.SubWalletBalance, 'Get subwallet balance before fund addition')];
                    case 2:
                        actualBalance = _a.sent();
                        return [4 /*yield*/, console.log("Actual Balance:" + actualBalance)];
                    case 3:
                        _a.sent(); //Number(beforeFundAdditionSubwalletBalanceApi) + Number(afterFundAdditionSubwalletBalanceApi);
                        return [4 /*yield*/, console.log("API balance" + ApiBalance)];
                    case 4:
                        _a.sent();
                        Assert_1.Assert.isValueEqual(this.browser, actualBalance, ApiBalance, "Verified actual link page headers is '" + actualBalance + "' as expected '" + ApiBalance + "'.");
                        return [2 /*return*/];
                }
            });
        });
    };
    return FundingAccountPage;
}());
exports.FundingAccountPage = FundingAccountPage;
