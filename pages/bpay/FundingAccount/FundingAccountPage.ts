import {SubwalletDetailApi} from "../../../apis/bpay/FundingAccount/SubwalletDetailApi";
import {NightwatchBrowser} from 'nightwatch';
const Selector = require('./homeKeySelector');
import {ElementAction} from "../../bpay/base/ElementAction";
import {Assert} from "../../bpay/base/Assert";
import {BusinessWalletBalanceApI} from "../../../apis/bpay/FundingAccount/BusinessWalletBalanceApI";
import {NumberUtil} from "../../util/NumberUtil";
import {MerchantProfileDetailApi} from "../../../apis/bpay/FundingAccount/MerchantProfileDetailApi";


export class FundingAccountPage {
    public browser: NightwatchBrowser;
    private subwalletDetailApi: SubwalletDetailApi;
    private businessWalletApi: BusinessWalletBalanceApI;
    private numberUtil: NumberUtil;
    private merchantProfileDetailApi: MerchantProfileDetailApi;

    constructor(client: NightwatchBrowser) {
        this.browser = client;
        this.subwalletDetailApi = new SubwalletDetailApi();
        this.businessWalletApi = new BusinessWalletBalanceApI();
        this.merchantProfileDetailApi = new MerchantProfileDetailApi();
    }

    async assertBusinessWalletBalance(userDetails) {
        let actualBusinessWalletBalance = await ElementAction.getElementTextByIndex(this.browser, Selector.Home.businessWalletBalance, 2,  "Assert business wallet balance" );
        await console.log("actualBusinessWalletBalance is: "+actualBusinessWalletBalance);
        let expectedBusinessWalletBalance = await this.businessWalletApi.getBusinessWalletBalance(userDetails);
        await console.log("expectedBusinessWalletBalance is: "+expectedBusinessWalletBalance);
        Assert.isValueEqual(this.browser,actualBusinessWalletBalance,expectedBusinessWalletBalance, "Verified actual link page headers is '"+actualBusinessWalletBalance+"' as expected '"+expectedBusinessWalletBalance+"'.");
        this.browser.pause(10);
    }
    //Assert merchant Account No (VAN)
    async assertMerchantAccountNo(userDetails) {
        let actualMerchantAccountNo = await ElementAction.getElementTextByIndex(this.browser, Selector.Home.MerchantAccountNo, 0, "Assert business wallet balance" );
        let actualMerchantIFSC = await ElementAction.getElementTextByIndex(this.browser, Selector.Home.MerchantAccountNo, 1, "Assert business wallet balance" );
        await console.log("Expected: Merchant Account no and IFSC are: "+actualMerchantAccountNo+"     "+actualMerchantIFSC);
        let trimAcc = new String(actualMerchantAccountNo).match(/\d+/g);
        let expectedMerchantAccountNo = await this.merchantProfileDetailApi.getVAN(userDetails);
        Assert.isValueEqual(this.browser,trimAcc[0],expectedMerchantAccountNo, "Verified actual bank account no is '"+trimAcc[0]+"' as expected '"+expectedMerchantAccountNo+"'.");
        this.browser.pause(10);
    }

    async clickOnCreateNewDisbursalWallet(){
        await ElementAction.clickByCss(this.browser, Selector.Home.createNewDisbursalWallet, "Click on Create new disbursal wallet button");
    }


    // Search the sub wallet by account name.
    async searchSubWalletByAccountname(accountName){
        await ElementAction.waitForElementPresentByXpath(this.browser, Selector.Home.SeachBox, 'Wait for the search box');
        await ElementAction.enterValueByXpath(this.browser, Selector.Home.SeachBox, accountName, "Enter subwallet name "+accountName+" in the search box");
        //beforeFundAdditionSubwalletBalance = await ElementAction.getTextByXpath(this.browser, Selector.Home.SubWalletBalance, 'Get subwallet balance before fund addition');
        console.log(accountName+" is searched.");

    }

    //Assert subwallet record
    async assertSubwalletByAccountName(accountName){
        await ElementAction.waitForElementPresentByXpath(this.browser,Selector.Home.AccountName.replace("value",accountName),accountName);
        // let flag = await  ElementAction.isElementPresentByXpath(this.browser,Selector.Home.AccountName.replace("value",accountName), accountName+" row is displayed");
        await Assert.isXpathElePresent(this.browser, Selector.Home.AccountName.replace("value",accountName), true,accountName + " is present","accountName");
    }
    //Search sub wallet by account name
    async searchSubWallet(accountName){
        await ElementAction.waitForElementPresentByXpath(this.browser, Selector.Home.SeachBox, 'Wait for the search box');
        await ElementAction.enterValueByXpath(this.browser, Selector.Home.SeachBox, accountName, 'Enter subwallet name '+accountName+'in the search box');
       await this.assertSubwalletByAccountName(accountName);
    }
    //click add fund button
    async clickAddFundButton(){
        await ElementAction.clickByXpath(this.browser, Selector.Home.AddFund, 'Click on the Add Fund button');
    }
    //Verify sub wallet amount
    async assertSubwalletBalance(userDetails, subWalletAccountName){
        let ApiBalance = await this.subwalletDetailApi.getSubwalletBalance(userDetails,subWalletAccountName);
        let actualBalance = await ElementAction.getTextByCSS(this.browser, Selector.Home.SubWalletBalance, 'Get subwallet balance before fund addition');
        await console.log("Actual Balance:"+actualBalance);//Number(beforeFundAdditionSubwalletBalanceApi) + Number(afterFundAdditionSubwalletBalanceApi);

        await console.log("API balance"+ ApiBalance);
        Assert.isValueEqual(this.browser,actualBalance ,ApiBalance, "Verified actual link page headers is '"+actualBalance+"' as expected '"+ApiBalance+"'.");

    }
}