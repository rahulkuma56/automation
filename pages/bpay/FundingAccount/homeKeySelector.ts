module.exports = {

    Home: {
        'businessWalletBalance': '.grid-inline',
        'createNewDisbursalWallet': '._1l-P',
        'AccountNameTextBox': '//input[@name=\'walletName\']',
        'CreateAccountButton' : '._3lxQ',
        'SaveButton': '//button[@class=\'btn btn-primary large _3lxQ\']',
        'SeachBox': '//input[@placeholder=\'Search by Account Name\']',
        'SearchIcon': '//div[@class=\'_21ua\']//span',
        'AddFund': '//button[@class=\'btn btn-default small \'][1]',
        'Amount': '//input[@name=\'amount\']',
        'AddFundButton': '//button[@type=\'submit\']',
        "SubWalletBalance": "[class='_1xEH ']",
        "AccountName":"//div[@title='value']",
        "MerchantAccountNo": ".grid.justify-start.align-center p",
        "MerchantIFSC": ".grid.justify-start.align-center p"
    }

}