import * as mysql from "mysql-ssh";
class DbConnection {


    executeQuery(option,query) {

        return mysql.connect(
            {
                host: option.server_host,
                user: option.server_user,
                password: option.server_password
            },
            {
                host: option.mysql_host,
                user: option.mysql_user,
                password: option.mysql_password,
                database: option.mysql_database,
                port: option.mysql_port
            }
        ).then((client) => {
            return new Promise((resolve,reject) => {
                client.query(query,function (err,result) {
                    client.close();
                    if(err){
                        reject(err);
                    }
                    resolve(result);
                });
            });

        });
    }
}






export  default  new DbConnection();