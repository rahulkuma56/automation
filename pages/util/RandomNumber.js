"use strict";
exports.__esModule = true;
var number = require("random-number");
var crypto = require("crypto");
var rand = {
    min: 30000,
    max: 300000000000,
    integer: true
};
var mobile = {
    min: 6000000000,
    max: 6999999999,
    integer: true
};
var RandomNumber = /** @class */ (function () {
    function RandomNumber() {
    }
    RandomNumber.prototype.random = function () {
        return number(rand);
    };
    RandomNumber.randomString = function () {
        return crypto.randomBytes(6).toString('hex');
    };
    RandomNumber.getRandomMobileNo = function () {
        return number(mobile);
    };
    RandomNumber.getCurrentTimeSpan = function () {
        return new Date().getTime();
    };
    return RandomNumber;
}());
exports.RandomNumber = RandomNumber;
exports["default"] = new RandomNumber();
