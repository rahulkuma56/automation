import {TokenGenerator} from "../../apis/global/oauthApis/TokenGenerator";
const Users = require("../../configs/ump/UserConfig");
const fs=require('fs');

const tokenGenrator=new TokenGenerator();

async function storeUserToken() {

    let obj = {};

    for(var key of Object.keys(Users) ) {
        for(let index=0; index < Users[key].length;index++ ) {
            let val =  await tokenGenrator.getWalletToken(Users[key][index].name,Users[key][index].password);
            obj[Users[key][index].name] = val;
        }
    }
    var json = "module.exports="+JSON.stringify(obj)+';';
    fs.writeFile('data/ump/automationToken.ts', json, 'utf8', function (error) {
        if(error!=null)
            console.log("Error in Token Generation is :: "+error.message);
        else
            console.log("*********** Successfully able to fetch and Store users token in 'automationToken.ts' file.");
    });
}

storeUserToken();

