"use strict";
exports.__esModule = true;
var csv = require("csv-writer");
var createCsvWriter = csv.createArrayCsvWriter;
var currentPath = process.cwd();
var headers = ['NAME', 'LANGUAGE'];
var records = [
    ['Bob', 'French, English'],
    ['Mary', 'English']
];
var path = currentPath + '/file.csv';
console.log(path);
var CsvFileCreater = /** @class */ (function () {
    function CsvFileCreater() {
    }
    CsvFileCreater.prototype.createCsv = function (record, header) {
        var csvWriter = createCsvWriter({
            path: path,
            header: header
        });
        csvWriter.writeRecords(record).then(function () {
            console.log("File has been created");
        });
        return path;
    };
    return CsvFileCreater;
}());
var obj = new CsvFileCreater();
obj.createCsv(records, headers);
exports["default"] = new CsvFileCreater();
