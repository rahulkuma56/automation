"use strict";
exports.__esModule = true;
var numberFormatter = require('indian-number-format');
var NumberUtil = /** @class */ (function () {
    function NumberUtil() {
    }
    NumberUtil.getCommaSepratedNumberUpTo2Digit = function (number) {
        if (number < 0)
            return '-' + numberFormatter.formatFixed(number * -1, 2);
        else
            return numberFormatter.formatFixed(number, 2);
    };
    NumberUtil.getCommaSepratedNumber = function (number) {
        if (number < 0)
            return '-' + numberFormatter.formatFixed(number * -1);
        else
            return numberFormatter.formatFixed(number);
    };
    NumberUtil.getDecimalWhenRequired = function (number) {
        return Math.round(number * 100) / 100;
    };
    NumberUtil.getValueWithUnit = function (number) {
        var newNumber = number.toString();
        var digits = newNumber.split(".")[0].length;
        if (digits === 8) {
            newNumber = newNumber[0] + '.' + newNumber.substring(1, 3) + ' Cr';
        }
        else {
            newNumber = this.getCommaSepratedNumberUpTo2Digit(number);
        }
        return newNumber;
    };
    return NumberUtil;
}());
exports.NumberUtil = NumberUtil;
