"use strict";
exports.__esModule = true;
var mysql = require("mysql-ssh");
var DbConnection = /** @class */ (function () {
    function DbConnection() {
    }
    DbConnection.prototype.executeQuery = function (option, query) {
        return mysql.connect({
            host: option.server_host,
            user: option.server_user,
            password: option.server_password
        }, {
            host: option.mysql_host,
            user: option.mysql_user,
            password: option.mysql_password,
            database: option.mysql_database,
            port: option.mysql_port
        }).then(function (client) {
            return new Promise(function (resolve, reject) {
                client.query(query, function (err, result) {
                    client.close();
                    if (err) {
                        reject(err);
                    }
                    resolve(result);
                });
            });
        });
    };
    return DbConnection;
}());
exports["default"] = new DbConnection();
