import {DateUtil} from "../DateUtil";
const failedTestInfo = require ('../../../allure-report/data/suites.json');
const failedTestCount = require ('../../../allure-report/widgets/summary.json');
const fs=require('fs');

export class FileReader {

    public getEmailSubject() {
        let failedTest= [''] ;
        let maxFailedCountShow = 2;
        let subject ='';
        let index=0;
        // let totalFailedCount = Number(failedTestCount['statistic']['failed'])-2;
        let totalFailedCount = Number(failedTestCount['statistic']['failed']);
        let totalScriptCount = Number(failedTestCount['statistic']['total']);
        let totalPassCount = Number(failedTestCount['statistic']['passed']);

        for (const parentkey of Object.keys(failedTestInfo['children'])) {
            let parentObj = failedTestInfo['children'][parentkey]['children'];
            for (let childKey of Object.keys(parentObj)) {
                if (parentObj[childKey]['status'] == 'failed') {
                    let rawData = fs.readFileSync('./allure-report/data/test-cases/' + parentObj[childKey]['uid'] + '.json', 'utf8');
                   if(index<maxFailedCountShow) {
                       failedTest[index] =  JSON.parse(rawData)['name']  ;
                       index++;
                   }else
                       break;
                }
            }
        }
        if(index>0)
            subject = process.env.Panel +" Automation Suite Result |"+" ENV : " +process.env.ENV +" | Date : "+DateUtil.getDateForEmailHeader()+" |"+ " Result : " + totalPassCount  + " Pass and "+ totalFailedCount + " Fail";
        else
            subject = process.env.Panel + " All Test Pass Successfully On |"+" ENV : " +process.env.ENV +" | Date : "+DateUtil.getDateForEmailHeader()+" |"+ " Total Test : " +totalScriptCount;
        return subject
    }
}

// function test() {
// console.log(new FileReader().getEmailSubject())
// }
// test();