"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var DateUtil_1 = require("../DateUtil");
var moment = require("moment");
var emailSender = require('emailjs/email');
var failedTestCount = require('../../../allure-report/widgets/summary.json');
var failedModuleData = require('../../../allure-report/widgets/suites.json');
var panelConfigData = require("../../../configs/panelConfiguration");
var FileReader_1 = require("./FileReader");
var NumberUtil_1 = require("../NumberUtil");
var EmailSender = /** @class */ (function () {
    function EmailSender() {
        this.failedTestData = new Map();
    }
    EmailSender.prototype.connectToServer = function () {
        this.emailServer = emailSender.server.connect({
            user: panelConfigData.EmailConfig.user,
            password: panelConfigData.EmailConfig.password,
            host: panelConfigData.EmailConfig.host,
            ssl: panelConfigData.EmailConfig.ssl
        });
    };
    EmailSender.prototype.getTestData = function () {
        this.totalTests = Number(failedTestCount['statistic']['total']);
        this.passedTests = Number(failedTestCount['statistic']['passed']);
        this.failedTests = Number(failedTestCount['statistic']['failed']);
        // console.log(failedModuleData[])
        this.skippedTests = Number(failedTestCount['statistic']['skipped']);
        this.passPercentage = NumberUtil_1.NumberUtil.getDecimalWhenRequired(this.passedTests * 100 / this.totalTests);
        this.failedPercentage = NumberUtil_1.NumberUtil.getDecimalWhenRequired(this.failedTests * 100 / this.totalTests);
        this.skippedPercentage = NumberUtil_1.NumberUtil.getDecimalWhenRequired(this.skippedTests * 100 / this.totalTests);
        for (var _i = 0, _a = Object.keys(failedModuleData['items']); _i < _a.length; _i++) {
            var parentkey = _a[_i];
            if (Number(failedModuleData['items'][parentkey]['statistic']['failed']) > 0)
                this.failedTestData.set(failedModuleData['items'][parentkey]['name'], Number(failedModuleData['items'][parentkey]['statistic']['failed']));
        }
    };
    // send the message and get a callback with an error or details of the message that was sent
    EmailSender.prototype.sendmailPrevious = function (link) {
        var hours = moment().format("HH") + ":00 HRS";
        var date = moment().format("DD-MM-YYYY");
        var testCaseLink = panelConfigData.EmailConfig.wikiLink;
        var bugList = panelConfigData.EmailConfig.BugList;
        var testHeader = "Hi All,\n" +
            "<p><p>Please find below the summary of " + process.env.Panel + " automation test suite:\n" +
            "<h1>" + process.env.Panel + " Automation Test Suite</h1>\n" +
            "<!-- Test Execution Details-->\n" +
            "<table style=\"width: 40%\"  border = \"1\">\n" +
            "\t<col width=\"140\">\n" +
            "  <tbody>\n" +
            "      <tr>\n" +
            "          <td bgcolor=\"Silver \"><b>Test Date</td>\n" +
            "          <td style=\"text-align:center;\"> " + date + "</td>\n" +
            "      </tr>\n" +
            "      <tr>\n" +
            "          <td bgcolor=\"Silver \"><b>Test Hour</td>\n" +
            "          <td style=\"text-align:center;\">" + hours + " </td>\n" +
            "      </tr>\n" +
            "      <tr>\n" +
            "          <td bgcolor=\"Silver \"><b>Test Env</td>\n" +
            "          <td style=\"text-align:center;\">" + (process.env.ENV).toUpperCase() + "</td>\n" +
            "      </tr>\n" +
            "  </tbody>\n" +
            "</table>\n";
        var summaryDetail = " <p><p>\n" +
            "<h4><u>Test  Execution Summary: </u></h4>\n" +
            "<table style=\"width: 40%\" border = \"1\">\n" +
            "\t<col width=\"140\">\n" +
            "\t<tbody>\n" +
            "      <tr>\n" +
            "          <td bgcolor=\"Silver \" ><b>Total TC#</b></td>\n" +
            "          <td style=\"text-align:center;\" >" + this.totalTests + "</td>\n" +
            "      </tr>\n" +
            "      <tr>\n" +
            "          <td bgcolor=\"Silver \"><b>Passed TC#</b></td>\n" +
            "          <td style=\"text-align:center;\">" + this.passedTests + "</td>\n" +
            "      </tr>\n" +
            "        <tr>\n" +
            "          <td bgcolor=\"Silver \"><b>Failed TC #</b></td>\n" +
            "          <td style=\"text-align:center;\">" + this.failedTests + "</td>\n" +
            "      </tr>\n" +
            "        <tr>\n" +
            "          <td bgcolor=\"Silver \"><b>Skipped TC#</b></td>\n" +
            "      \t  <td style=\"text-align:center;\">" + this.skippedTests + "</td>\n" +
            "      </tr>\n" +
            "  \t</tbody>\n" +
            "</table>\n";
        var linkDetails = "<p><p>\n" +
            "<table style=\"width: 40%\" border = \"1\">\n" +
            "\t<col width=\"140\">\n" +
            "\t<tbody>\n" +
            "\t\t<tr>\n" +
            "\t\t\t<td bgcolor=\"Silver \"><b>Detailed Test Report </b></td>\n" +
            "  \t\t\t<td style=\"text-align:center;\"><u></u><a href=" + link + ">Click Here</a></td>\n" +
            "\t\t</tr>\n" +
            "        <tr>\n" +
            "\t\t\t<td bgcolor=\"Silver \"><b>Test Cases Detail  </b></td>\n" +
            "  \t\t\t<td style=\"text-align:center;\"><u></u><a href=" + testCaseLink + ">Click Here</a></td>\n" +
            "\t\t</tr>\n" +
            "\t</tbody>\n" +
            "</table>\n";
        var bugDetails = "";
        if (bugList.length > 0) {
            bugDetails = "<p><p>\n" +
                "<h4><u>Open  Bugs:</u></h4>\n" +
                " <table style='width:80%' border = \"1\">\n" +
                " \t<col >\n" +
                "\t<tbody>\n" +
                "  \t<tr bgcolor=\"Silver \">\n" +
                "      \t<th width='14'> Sr.no</th>\n" +
                "       \t<th width='20'>Bug Id</th>\n" +
                "       \t<th width='20'>Created On</th>\n" +
                "       \t<th width='30'>Bug Summary</th>\n" +
                "  \t</tr>\n";
            for (var bugIndex = 0; bugIndex < bugList.length; bugIndex++) {
                bugDetails += "\t<tr>\n" + "<td style='text-align:center;'>" + (bugIndex + 1) + ".</td>\n" +
                    "        <td style=\"text-align:center;\"><a href='" + bugList[bugIndex].href + "'>" + bugList[bugIndex].ID + "</a></td>\n" +
                    "        <td style='text-align:center;'>" + bugList[bugIndex].createdOn + "</td>\n" +
                    "        <td style='text-align:left;'>" + bugList[bugIndex].summary + "</td>\n" +
                    "\t</tr>\n";
            }
            bugDetails += " </tbody>\n" +
                "</table>\n";
        }
        var testFooter = "<p><p><p><u>Notes:</u>\n" +
            "<p><ul><li>" + panelConfigData.EmailConfig.note + "</ul>\n" +
            "</p>\n" +
            "</p>\n" +
            "Please feel free to contact <i><u>pg.qa@paytm.com</i></u> in case of any concerns or queries.\n" +
            "<p><p>Thanks & Regards,\n" +
            "<p><b> PG QA</b>";
        this.emailServer.send({
            text: '',
            from: panelConfigData.EmailConfig.fromUser,
            to: panelConfigData.EmailConfig.toUser,
            subject: new FileReader_1.FileReader().getEmailSubject(),
            // subject:  "Test Email ",
            attachment: [{ data: testHeader + summaryDetail + linkDetails + bugDetails + testFooter, alternative: true }]
        }, function (err, message) { console.log(err || message); });
    };
    EmailSender.prototype.sendmail = function (link) {
        var hours = moment().format("HH") + ":00 HRS";
        var date = moment().format("DD-MM-YYYY");
        var testCaseLink = panelConfigData.EmailConfig.wikiLink;
        var bugList = panelConfigData.EmailConfig.BugList;
        var reportDetails = "<p>Hi All,</p>\n" +
            "<p>Please find below the summary of <span style=\"color: #0000ff;\">" + process.env.Panel + "</span> automation test suite:</p>\n" +
            "<h2 style=\"text-align: left;\"><span style=\"color: #0000ff;\">&nbsp;<span style=\"text-decoration: underline;\">" + process.env.Panel + "</span></span><span style=\"text-decoration: underline;\"><span style=\"color: #0000ff; text-decoration: underline;\"> Automation Test Suite</span><span style=\"color: #0000ff; text-decoration: underline;\"></span></span></h2>" +
            "<!-- EMAIL_PART_01 -->\n" +
            "<table border=\"2\" style=\"height: 131px; collapse; width: 200%;\" height=\"229\" width=\"700\">\n" +
            "<tbody>\n" +
            "<tr style=\"height: 22px;\">\n" +
            "<td style=\"width: 99.6048%; border-style: double; background-color: grey; border-color: black; height: 12px; text-align: center;\" colspan=\"6\"><span style=\"color: #ffffff; font-size: 14px;\">Test Execution Time</span></td>\n" +
            "</tr>\n" +
            "<tr style=\"height: 22px;\">\n" +
            "<th style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 14px; background-color: rgba(0, 0, 0, 0.1);\"><span>Test Date</span></th>\n" +
            "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 14px;\"><span style=\"color: #0000ff; font-size: 14px;\">" + date + "</span></td>\n" +
            "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 14px; background-color: rgba(0, 0, 0, 0.1);\"><strong>Test Hour</strong></td>\n" +
            "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 14px;\"><span style=\"color: #0000ff;\">" + hours + "</span></td>\n" +
            "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 14px; background-color: rgba(0, 0, 0, 0.1);\"><strong>Test Env.</strong></td>\n" +
            "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 14px;\"><span style=\"color: #0000ff;\">" + (process.env.ENV).toUpperCase() + "</span></td>\n" +
            "</tr>\n" +
            "<tr style=\"height: 23px;\">\n" +
            "<td style=\"width: 99.6048%; height: 23px; background-color: grey; text-align: center;\" colspan=\"6\"><span style=\"color: #ffffff; font-size: 14px;\">Test Execution Summary</span></td>\n" +
            "</tr>\n" +
            "<tr style=\"height: 23px;\">\n" +
            "<td style=\"width: 16.6008%; height: 69px; text-align: center; font-size: 14px; background-color: rgba(0, 0, 0, 0.1);\" rowspan=\"3\"><strong>Total TC#</strong></td>\n" +
            "<td style=\"width: 16.6008%; height: 69px; text-align: center; font-size: 14px;\" rowspan=\"3\"><span style=\"color: #0000ff;\">" + this.totalTests + "</span></td>\n" +
            "<td style=\"width: 16.6008%; height: 23px; font-size: 14px; background-color: rgba(0, 0, 0, 0.1);\"><strong>&nbsp;Pass TC#</strong></td>\n" +
            "<td style=\"width: 16.6008%; height: 23px; text-align: center; font-size: 14px;\"><span style=\"color: #339966;\">" + this.passedTests + "</span></td>\n" +
            "<td style=\"width: 16.6008%; height: 23px; font-size: 14px; background-color: rgba(0, 0, 0, 0.1);\"><strong>&nbsp;Pass %age</strong></td>\n" +
            "<td style=\"width: 16.6008%; height: 23px; text-align: center; font-size: 14px;\"><span style=\"color: #339966;\">" + this.passPercentage + "%</span></td>\n" +
            "</tr>\n" +
            "<tr style=\"height: 23px;\">\n" +
            "<td style=\"width: 16.6008%; height: 23px; font-size: 14px; background-color: rgba(0, 0, 0, 0.1);\"><strong>&nbsp;Fail TC#</strong></td>\n" +
            "<td style=\"width: 16.6008%; height: 23px; text-align: center; font-size: 14px;\"><span style=\"color: #ff0000;\">" + this.failedTests + "</span></td>\n" +
            "<td style=\"width: 16.6008%; height: 23px; font-size: 14px; background-color: rgba(0, 0, 0, 0.1);\"><strong>&nbsp;Fail %age</strong></td>\n" +
            "<td style=\"width: 16.6008%; height: 23px; text-align: center; font-size: 14px;\"><span style=\"color: #ff0000;\">" + this.failedPercentage + "%</span></td>\n" +
            "</tr>\n" +
            "<tr style=\"height: 23px;\">\n" +
            "<td style=\"width: 16.6008%; height: 23px; font-size: 14px; background-color: rgba(0, 0, 0, 0.1);\"><strong>&nbsp;Skip TC#</strong></td>\n" +
            "<td style=\"width: 16.6008%; height: 23px; text-align: center; font-size: 14px;\"><span style=\"color: #ff9900;\">" + this.skippedTests + "</span></td>\n" +
            "<td style=\"width: 16.6008%; height: 23px; font-size: 14px; background-color: rgba(0, 0, 0, 0.1);\"><strong>&nbsp;Skip %age</strong></td>\n" +
            "<td style=\"width: 16.6008%; height: 23px; text-align: center; font-size: 14px;\"><span style=\"color: #ff9900;\">" + this.skippedPercentage + "%</span></td>\n" +
            "</tr>\n" +
            "<tr style=\"height: 18px;\">\n" +
            "<td style=\"width: 99.6048%; height: 23px; background-color: grey; text-align: center;\" colspan=\"6\"><span style=\"color: #ff9900;\"><span style=\"color: #ffffff; font-size: 14px;\">Report &amp; Link</span><br /></span></td>\n" +
            "</tr>\n" +
            "<tr style=\"height: 19px;\">\n" +
            "<td style=\"width: 49.8024%; font-size: 14px; background-color: rgba(0, 0, 0, 0.1); height: 19px; text-align: left;\" colspan=\"3\"><strong>&nbsp;</strong><strong>&nbsp;Detailed Test Report</strong></td>\n" +
            "<td style=\"width: 49.8024%; text-align: center; font-size: 14px; height: 19px;\" colspan=\"3\"><a href='" + link + "'+><span style=\"color: #ff9900;\"><span style=\"color: #0000ff;\"><span style=\"text-decoration: underline;\">Click Here</span></span></span></a></td>\n" +
            "</tr>\n" +
            "<tr style=\"height: 19px;\">\n" +
            "<td style=\"font-size: 14px; background-color: rgba(0, 0, 0, 0.1); height: 19px; width: 49.8024%; text-align: left;\" colspan=\"3\"><strong>&nbsp; Test Cases detail&nbsp;</strong><span style=\"color: #0000ff;\">&nbsp;</span></td>\n" +
            "<td style=\"font-size: 14px; height: 19px; text-align: center; width: 49.8024%;\" colspan=\"3\"><span style=\"color: #ff9900;\"><span style=\"text-decoration: underline;\"><a href='" + testCaseLink + "'+><span style=\"color: #0000ff; text-decoration: underline;\">Click Here</span></a></span>&nbsp;</span></td>\n" +
            "</tr>";
        var failedModuleDataList = "";
        var failedModuleDataHeader = "";
        if (this.failedTestData.size > 0) {
            failedModuleDataHeader = "<tr style=\"height: 18px;\">\n" +
                "<td style=\"width: 99.6048%; height: 23px; background-color: grey; text-align: center;\" colspan=\"6\"><span style=\"color: #ff9900;\"><span style=\"color: #ffffff; font-size: 12px;\">Module with failures</span><br /></span></td>\n" +
                "</tr>\n" +
                "<tr style=\"height: 22px;\">\n" +
                "<th style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.1);\"><span>Sr.No</span></th>\n" +
                "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.1);\"><strong>Module</strong></td>\n" +
                "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.1);\"><strong>Count</strong></td>\n" +
                "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.1);\"><strong>Sr.No</strong></td>\n" +
                "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.1);\"><strong>Module</strong></td>\n" +
                "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.1);\"><strong>Count</strong></td>\n" +
                "</tr> ";
            var allModule = Array.from(this.failedTestData.keys());
            for (var index = 0; index < allModule.length; index += 2) {
                failedModuleDataList += "<tr style=\"height: 22px;\">\n" +
                    "<th style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.01);\"><span>" + (index + 1) + ".</span></th>\n" +
                    "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px;\"><span style=\"color: #0000ff; font-size: 12px;\">" + allModule[index] + "</span></td>\n" +
                    "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px;\"><span style=\"color: #0000ff; font-size: 12px;\">" + this.failedTestData.get(allModule[index]) + "</span></td>\n" +
                    "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.01);\"><strong>" + (index + 2) + ".</strong></td>\n" +
                    "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px;\"><span style=\"color: #0000ff; font-size: 12px;\">" + (allModule[index + 1] == undefined ? "N/A" : allModule[index + 1]) + "</span></td>\n" +
                    "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px;\"><span style=\"color: #0000ff;\">" + (this.failedTestData.get(allModule[index + 1]) == undefined ? "N/A" : this.failedTestData.get(allModule[index + 1])) + "</span></td>\n" +
                    "</tr>";
            }
        }
        var bugDetails = "";
        var bugHeader = "";
        if (moment().format("HH") == "10" || moment().format("HH") == "17") {
            if (bugList.length > 0) {
                bugHeader = "<tr style=\"height: 18px;\">\n" +
                    "<td style=\"width: 99.6048%; height: 23px; background-color: grey; text-align: center;\" colspan=\"6\"><span style=\"color: #ff9900;\"><span style=\"color: #ffffff; font-size: 12px;\">Open/Existing Bugs</span><br /></span></td>\n" +
                    "</tr>\n" +
                    "<tr style=\"height: 22px;\">\n" +
                    "<th style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.1);\"><span>Sr.No</span></th>\n" +
                    "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.1);\"><strong>Defect Id</strong></td>\n" +
                    "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.1);\"><strong>Created Dt</strong></td>\n" +
                    "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px; background-color: rgba(0, 0, 0, 0.1);\" colspan=\"3\"><strong>Defect Summary </strong></td>\n" +
                    "</tr>";
                for (var bugIndex = 0; bugIndex < bugList.length; bugIndex++) {
                    bugDetails += "<tr style=\"height: 22px;\">\n" +
                        "<th style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px;\"><span>" + (bugIndex + 1) + ".</span></th>\n" +
                        "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px;\"><span style=\"text-decoration: underline;\"><a href='" + bugList[bugIndex].href + "'><span style=\"color: #0000ff; text-decoration: underline;\">" + bugList[bugIndex].ID + "</span></a></span></td>\n" +
                        "<td style=\"width: 16.6008%; height: 22px; text-align: center; font-size: 12px;\"><span style=\"color: #0000ff;\">" + bugList[bugIndex].createdOn + "</span></td>\n" +
                        "<td style=\"width: 16.6008%; height: 22px; font-size: 12px; text-align: left;\" colspan=\"3\"><span style=\"color: #0000ff;\">" + bugList[bugIndex].summary + "</span></td>\n" +
                        "</tr>";
                }
            }
        }
        var endOFTable = "</table>\n";
        var testFooter = "";
        if (panelConfigData.EmailConfig.note != undefined) {
            testFooter = "<p><u></u></p>\n" +
                "<p><em><u>Notes:</u></em></p>\n" +
                "<ul>\n" +
                "<li>" + panelConfigData.EmailConfig.note + "</li>\n" +
                "</ul>\n";
        }
        testFooter += "<p><span>Please feel free to contact&nbsp;</span><i><u><a href=\"mailto:pg.qa@paytm.com\" target=\"_blank\" rel=\"noopener\">pg.qa@paytm.com</a></u></i><span>&nbsp;in case of any concerns or queries.<br /><br /></span></p>\n" +
            "<p>Thanks &amp; Regards,</p>\n" +
            "<p><b>PG QA</b></p>";
        this.emailServer.send({
            text: '',
            from: panelConfigData.EmailConfig.fromUser,
            to: panelConfigData.EmailConfig.toUser,
            // subject:  "Test Email",
            subject: new FileReader_1.FileReader().getEmailSubject(),
            attachment: [{ data: reportDetails + failedModuleDataHeader + failedModuleDataList + bugHeader + bugDetails + endOFTable + testFooter, alternative: true }]
        }, function (err, message) { console.log(err || message); });
    };
    return EmailSender;
}());
exports.EmailSender = EmailSender;
function test() {
    return __awaiter(this, void 0, void 0, function () {
        var mail, link;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    mail = new EmailSender();
                    link = '';
                    return [4 /*yield*/, mail.connectToServer()];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, mail.getTestData()];
                case 2:
                    _a.sent();
                    link = 'https://ump-automation.paytm.com/prod-report/' + process.env.ENV + "_" + process.env.Panel + "_" + DateUtil_1.DateUtil.getDateForEmailUtil() + '/allure-report/#';
                    return [4 /*yield*/, mail.sendmail(link)];
                case 3:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
test();
