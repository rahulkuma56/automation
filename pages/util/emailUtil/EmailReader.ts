var Imap = require('imap'),
    inspect = require('util').inspect;
const  Users =require("../../../configs/bpay/UserConfig");

function openInbox(cb) {
    imap.openBox('INBOX', true, cb);
}

var imap = new Imap({
    user: Users.EmailUser[0].email,
    password: Users.EmailUser[0].emailPassword,
    host: 'imap.gmail.com',
    port: 993,
    tls: true
});

export class EmailReader {

    async readBody() {
        return new Promise((resolve, reject) => {
            imap.connect();
            var buffer = '';
            imap.once('ready', function () {
                openInbox(function (err, box) {
                    if (err) throw err;
                    var f = imap.seq.fetch(box.messages.total + ':*', {bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)', 'TEXT']});
                    f.on('message', function (msg, seqno) {
                        console.log('Message #%d', seqno);
                        var prefix = '(#' + seqno + ') ';
                        msg.on('body', function (stream, info) {
                            var count = 0;
                            stream.on('data', function (chunk) {
                                count += chunk.length;
                                buffer += chunk.toString('utf8');
                                // if (info.which === 'TEXT')
                                    // console.log(prefix + 'Body [%s] (%d/%d)', inspect(info.which), count, info.size);
                            });
                            stream.once('end', function () {
                                if (info.which !== 'TEXT')
                                    console.log(prefix + 'Parsed header: %s', inspect(Imap.parseHeader(buffer)));
                                else{
                                    // console.log(prefix + 'Body [%s] Finished', inspect(info.which));
                                    resolve(buffer);
                                }

                            });
                        });
                        msg.once('attributes', function (attrs) {
                            console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
                        });
                        msg.once('end', function () {
                            console.log(prefix + 'Finished');
                        });
                    });
                    f.once('error', function (err) {
                        console.log('Fetch error: ' + err);
                    });
                    f.once('end', function () {
                        console.log('Done fetching all messages!');
                        imap.end();
                    });
                });


            });

        });
    }

    async getOTP() {
        let LOGIN_ACC_OTP ='Your One Time Password (OTP) for login to Paytm account is ';
        let body = await this.readBody();
        let index = body.toString().indexOf(LOGIN_ACC_OTP);
        if(index < 0){
            LOGIN_ACC_OTP = 'Your One Time Password (OTP) is ';
            index = body.toString().indexOf(LOGIN_ACC_OTP);
        }
        let stringLength = LOGIN_ACC_OTP.length;
        let otp = body.toString().substring(index + stringLength+89, index + stringLength + 95)
        console.log("The OTP is " + otp);
        return otp;
    }

}

// async function test() {
//     let emailReader = new EmailReader();
//     let otp = await emailReader.readBody();
//     console.log(otp);
// }
// test();


