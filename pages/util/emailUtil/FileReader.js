"use strict";
exports.__esModule = true;
var DateUtil_1 = require("../DateUtil");
var failedTestInfo = require('../../../allure-report/data/suites.json');
var failedTestCount = require('../../../allure-report/widgets/summary.json');
var fs = require('fs');
var FileReader = /** @class */ (function () {
    function FileReader() {
    }
    FileReader.prototype.getEmailSubject = function () {
        var failedTest = [''];
        var maxFailedCountShow = 2;
        var subject = '';
        var index = 0;
        // let totalFailedCount = Number(failedTestCount['statistic']['failed'])-2;
        var totalFailedCount = Number(failedTestCount['statistic']['failed']);
        var totalScriptCount = Number(failedTestCount['statistic']['total']);
        var totalPassCount = Number(failedTestCount['statistic']['passed']);
        for (var _i = 0, _a = Object.keys(failedTestInfo['children']); _i < _a.length; _i++) {
            var parentkey = _a[_i];
            var parentObj = failedTestInfo['children'][parentkey]['children'];
            for (var _b = 0, _c = Object.keys(parentObj); _b < _c.length; _b++) {
                var childKey = _c[_b];
                if (parentObj[childKey]['status'] == 'failed') {
                    var rawData = fs.readFileSync('./allure-report/data/test-cases/' + parentObj[childKey]['uid'] + '.json', 'utf8');
                    if (index < maxFailedCountShow) {
                        failedTest[index] = JSON.parse(rawData)['name'];
                        index++;
                    }
                    else
                        break;
                }
            }
        }
        if (index > 0)
            subject = process.env.Panel + " Automation Suite Result |" + " ENV : " + process.env.ENV + " | Date : " + DateUtil_1.DateUtil.getDateForEmailHeader() + " |" + " Result : " + totalPassCount + " Pass and " + totalFailedCount + " Fail";
        else
            subject = process.env.Panel + " All Test Pass Successfully On |" + " ENV : " + process.env.ENV + " | Date : " + DateUtil_1.DateUtil.getDateForEmailHeader() + " |" + " Total Test : " + totalScriptCount;
        return subject;
    };
    return FileReader;
}());
exports.FileReader = FileReader;
// function test() {
// console.log(new FileReader().getEmailSubject())
// }
// test();
