"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Imap = require('imap'), inspect = require('util').inspect;
var Users = require("../../../configs/bpay/UserConfig");
function openInbox(cb) {
    imap.openBox('INBOX', true, cb);
}
var imap = new Imap({
    user: Users.EmailUser[0].email,
    password: Users.EmailUser[0].emailPassword,
    host: 'imap.gmail.com',
    port: 993,
    tls: true
});
var EmailReader = /** @class */ (function () {
    function EmailReader() {
    }
    EmailReader.prototype.readBody = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        imap.connect();
                        var buffer = '';
                        imap.once('ready', function () {
                            openInbox(function (err, box) {
                                if (err)
                                    throw err;
                                var f = imap.seq.fetch(box.messages.total + ':*', { bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)', 'TEXT'] });
                                f.on('message', function (msg, seqno) {
                                    console.log('Message #%d', seqno);
                                    var prefix = '(#' + seqno + ') ';
                                    msg.on('body', function (stream, info) {
                                        var count = 0;
                                        stream.on('data', function (chunk) {
                                            count += chunk.length;
                                            buffer += chunk.toString('utf8');
                                            // if (info.which === 'TEXT')
                                            // console.log(prefix + 'Body [%s] (%d/%d)', inspect(info.which), count, info.size);
                                        });
                                        stream.once('end', function () {
                                            if (info.which !== 'TEXT')
                                                console.log(prefix + 'Parsed header: %s', inspect(Imap.parseHeader(buffer)));
                                            else {
                                                // console.log(prefix + 'Body [%s] Finished', inspect(info.which));
                                                resolve(buffer);
                                            }
                                        });
                                    });
                                    msg.once('attributes', function (attrs) {
                                        console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
                                    });
                                    msg.once('end', function () {
                                        console.log(prefix + 'Finished');
                                    });
                                });
                                f.once('error', function (err) {
                                    console.log('Fetch error: ' + err);
                                });
                                f.once('end', function () {
                                    console.log('Done fetching all messages!');
                                    imap.end();
                                });
                            });
                        });
                    })];
            });
        });
    };
    EmailReader.prototype.getOTP = function () {
        return __awaiter(this, void 0, void 0, function () {
            var LOGIN_ACC_OTP, body, index, stringLength, otp;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        LOGIN_ACC_OTP = 'Your One Time Password (OTP) for login to Paytm account is ';
                        return [4 /*yield*/, this.readBody()];
                    case 1:
                        body = _a.sent();
                        index = body.toString().indexOf(LOGIN_ACC_OTP);
                        if (index < 0) {
                            LOGIN_ACC_OTP = 'Your One Time Password (OTP) is ';
                            index = body.toString().indexOf(LOGIN_ACC_OTP);
                        }
                        stringLength = LOGIN_ACC_OTP.length;
                        otp = body.toString().substring(index + stringLength + 89, index + stringLength + 95);
                        console.log("The OTP is " + otp);
                        return [2 /*return*/, otp];
                }
            });
        });
    };
    return EmailReader;
}());
exports.EmailReader = EmailReader;
// async function test() {
//     let emailReader = new EmailReader();
//     let otp = await emailReader.readBody();
//     console.log(otp);
// }
// test();
