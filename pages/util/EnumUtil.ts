import {EnumValues} from "enum-values";


export class EnumUtil {

    static getKeyByValue(enumName,value){
        let key =  EnumValues.getNameFromValue(enumName,value);
        return key;
    }

    static getNames(value){
        let key =  EnumValues.getNames(value);
        return key;
    }
}

