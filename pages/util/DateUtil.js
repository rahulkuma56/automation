"use strict";
exports.__esModule = true;
var moment = require("moment");
var StringUtil_1 = require("./StringUtil");
var DateUtil = /** @class */ (function () {
    function DateUtil() {
    }
    DateUtil.convertMiliToDate = function (dateString) {
        var dateNum = Number.parseInt(dateString);
        var updatedDate = moment.unix(dateNum / 1000).format("DD MMM, hh:mm A");
        return "" + updatedDate;
    };
    DateUtil.getDate = function (dateString) {
        var updatedDate = moment(dateString).format("DD MMM, hh:mm A");
        if (updatedDate == 'Invalid date')
            updatedDate = "" + moment(dateString.substring(4).replace('IST', ''), "MMM DD HH:mm:ss yyyy").format("DD MMM, hh:mm A");
        if (updatedDate == 'Invalid date')
            updatedDate = "" + moment(dateString, "DD/MM/YYY HH:mm:ss").format("DD MMM, hh:mm A");
        return "" + updatedDate;
    };
    DateUtil.getDateWithFormat = function (dateString, dateFormat) {
        var updatedDate = "" + moment(dateString, dateFormat).format("DD MMM, hh:mm A");
        return "" + updatedDate;
    };
    DateUtil.getDateWithoutTime = function (dateString) {
        var updatedDate = "" + moment(dateString, moment.ISO_8601).format("DD MMM YYYY");
        return updatedDate;
    };
    DateUtil.getDateWithTime = function (dateString) {
        var updatedDate = "" + moment(dateString, moment.ISO_8601).format("DD MMM YYYY, hh:mm A");
        return updatedDate;
    };
    DateUtil.dateInMilliSeconds = function (date) {
        var ts = "" + moment(date).valueOf();
        return ts;
    };
    DateUtil.getDateAndMonth = function (dateString) {
        if (dateString === void 0) { dateString = ''; }
        return (dateString == '' ? "" + moment().format("D MMM") :
            "" + moment(dateString).format("D MMM"));
    };
    DateUtil.getDateAndMonth2Digit = function (dateString) {
        if (dateString === void 0) { dateString = ''; }
        return (dateString == '' ? "" + moment().format("DD MMM") :
            "" + moment(dateString).format("D MMM"));
    };
    DateUtil.getCurrentTimeMilliSeconds = function () {
        return "" + moment().valueOf();
    };
    DateUtil.getDateAndFullMonth = function (dateString) {
        if (dateString === void 0) { dateString = ''; }
        if (dateString.includes("/"))
            return this.getDateAndFullMonthForSlash(dateString);
        return (dateString == '' ? "" + moment().format("D MMMM") :
            "" + moment(dateString).format("D MMMM"));
    };
    DateUtil.getSingleDateAndFullMonth = function (dateString) {
        if (dateString === void 0) { dateString = ''; }
        if (dateString.includes("/"))
            return this.getSingleDateAndFullMonthForSlash(dateString);
        return (dateString == '' ? "" + moment().format("DD MMMM") :
            "" + moment(dateString).format("DD MMMM"));
    };
    DateUtil.getDateAndFullMonthForSlash = function (dateString) {
        if (dateString === void 0) { dateString = ''; }
        return (dateString == '' ? "" + moment().format("D MMMM") :
            "" + moment(dateString, "DD/MM/YYYY").format("D MMMM"));
    };
    DateUtil.getSingleDateAndFullMonthForSlash = function (dateString) {
        if (dateString === void 0) { dateString = ''; }
        return (dateString == '' ? "" + moment().format("DD MMMM") :
            "" + moment(dateString, "DD/MM/YYYY").format("DD MMMM"));
    };
    DateUtil.getDateForEmailUtil = function () {
        var updatedDate = moment().format("YYYY_MM_DD_HH");
        return updatedDate;
    };
    DateUtil.getDateForEmailHeader = function () {
        var updatedDate = moment().format("DD-MM-YYYY : HH");
        return updatedDate;
    };
    DateUtil.getDateInNumeric = function (dateString, actualDateFormat) {
        dateString = StringUtil_1.StringUtil.replaceAll(dateString, "/", "-");
        var updatedDate = moment(dateString, actualDateFormat).format("YYYYMMDD");
        if (updatedDate == 'Invalid date')
            updatedDate = moment(dateString, "DD-MM-YYYY").format("YYYYMMDD");
        var numericDate = Number.parseInt(updatedDate);
        return numericDate;
    };
    DateUtil.getTime = function (dateString) {
        var updatedDate = moment(dateString).format("hh:mm A");
        if (updatedDate == 'Invalid date')
            updatedDate = "" + moment(dateString.substring(4).replace('IST', ''), "MMM DD HH:mm:ss yyyy").format("hh:mm A");
        return "" + updatedDate;
    };
    return DateUtil;
}());
exports.DateUtil = DateUtil;
//
// function test() {
//     // console.log(DateUtil.getDateInNumeric(Duration_Slash.THIS_WEEK_START_DATE));
//     // console.log(DateUtil.getDateInNumeric("2019/10/04"));
//     // console.log(DateUtil.getDateInNumeric("04/10/2019",'DD/MM/YYYY'));
// }
//
// test();
