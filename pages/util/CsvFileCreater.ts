

import * as csv from 'csv-writer';

let createCsvWriter=csv.createArrayCsvWriter;
let  currentPath = process.cwd();

var headers=['NAME', 'LANGUAGE'];
const records = [
    ['Bob',  'French, English'],
    ['Mary', 'English']
];


const path=currentPath + '/file.csv';
console.log(path);

class CsvFileCreater {

     createCsv(record, header) {
        let csvWriter = createCsvWriter({
            path:path ,
            header: header
        });
      csvWriter.writeRecords(record).then(()=>{
            console.log("File has been created");
        });
      return path;
    }
}


var obj=new CsvFileCreater();
obj.createCsv(records,headers);

export default new CsvFileCreater();


