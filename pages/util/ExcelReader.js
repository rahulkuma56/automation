"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var exceljs_1 = require("exceljs");
var Users = require("../../configs/ump/UserConfig");
var DownloadList = require("../../apis/ump/reportAPI/DownloadReportList");
var fileSystem = require("fs");
var headers = [];
var data = [];
var workbook = new exceljs_1.Workbook();
function sortArray(firstParam, secondParam) {
    if (firstParam['Transaction_Date'] === secondParam['Transaction_Date']) {
        return 0;
    }
    else {
        return (firstParam['Transaction_Date'] < secondParam['Transaction_Date']) ? 1 : -1;
    }
}
var ExcelReader = /** @class */ (function () {
    function ExcelReader(fileType, fileName) {
        var currentLocation = process.cwd();
        if (currentLocation.includes('pages'))
            currentLocation = currentLocation.substring(0, currentLocation.indexOf("pages"));
        var directoyPath = currentLocation + "/data/downloadedFiles/";
        this.filePath = directoyPath + fileName;
        switch (fileType) {
            case FileType.CSV: {
                this.fileReader = workbook.csv.readFile(this.filePath);
                break;
            }
            case FileType.XLSX: {
                this.fileReader = workbook.xlsx.readFile(this.filePath);
                break;
            }
            default: {
                this.fileReader = workbook.csv.readFile(this.filePath);
                break;
            }
        }
    }
    ExcelReader.prototype.getHeaders = function (sheetIndex) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.fileReader.then(function () {
                            var sheet = workbook.getWorksheet(sheetIndex);
                            headers = sheet.getRow(1).values;
                            for (var index = 0; index < headers.length; index++) {
                                headers[index] = headers[index] == undefined ? headers[index] = '' : headers[index];
                            }
                            console.log("the headers are :- " + headers);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, headers];
                }
            });
        });
    };
    ExcelReader.prototype.getAllDataFromSheet = function (sheetIndex) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.fileReader.then(function () {
                            var sheet = workbook.getWorksheet(sheetIndex);
                            var headers = sheet.getRow(1).values;
                            for (var i = 1; i <= sheet.rowCount; i++) {
                                for (var j = 1; j <= sheet.columnCount; j++) {
                                    if (!data[i]) {
                                        data[i] = [];
                                    }
                                    data[i][headers[j]] = sheet.getRow(i).getCell(j).value;
                                }
                            }
                            data.shift();
                            data.shift();
                            //console.log(data);
                            data.sort(sortArray);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, data];
                }
            });
        });
    };
    ExcelReader.prototype.getValuesByColumnName = function (sheetIndex, columnName) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.fileReader.then(function () {
                            var sheet = workbook.getWorksheet(sheetIndex);
                            data = [];
                            var rowCount = sheet.rowCount;
                            for (var i = 1; i <= sheet.rowCount; i++) {
                                for (var j = 1; j <= sheet.columnCount; j++) {
                                    if (sheet.getRow(i).getCell(j).value === columnName) {
                                        while (i < rowCount) {
                                            data[i] = sheet.getRow(i + 1).getCell(j).value;
                                            i++;
                                        }
                                        break;
                                    }
                                }
                            }
                            data.shift();
                            console.log(data);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, data];
                }
            });
        });
    };
    ExcelReader.prototype.getRowValue = function (sheetIndex, columnName, uniqueValue) {
        this.fileReader.then(function () {
            var sheet = workbook.getWorksheet(sheetIndex);
            var data = [];
            var columnNumber;
            var rowNumber;
            for (var j = 1; j <= sheet.columnCount; j++) {
                if (sheet.getRow(1).getCell(j).value === columnName) {
                    columnNumber = j;
                    break;
                }
            }
            console.log("The column number is :-" + columnNumber);
            for (var i = 1; i <= sheet.rowCount; i++) {
                if (sheet.getRow(i).getCell(columnNumber).value === uniqueValue) {
                    rowNumber = i;
                    break;
                }
            }
            console.log("The row number is :-" + rowNumber);
            this.headers = sheet.getRow(1).values;
            for (var i = 1; i <= sheet.rowCount; i++) {
                for (var j = 1; j <= sheet.columnCount; j++) {
                    data[j] = sheet.getRow(rowNumber).getCell(j).value;
                }
                break;
            }
            data.shift();
            console.log("The value of the data is :- " + data);
        });
    };
    ExcelReader.prototype.deleteFile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var path, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, decodeURIComponent(this.filePath)];
                    case 1:
                        path = _a.sent();
                        return [4 /*yield*/, fileSystem.unlink(path, function (err) {
                                if (err)
                                    throw err;
                            })];
                    case 2:
                        _a.sent();
                        console.log("File Successfully Deleted");
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.error(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    return ExcelReader;
}());
exports.ExcelReader = ExcelReader;
var FileType;
(function (FileType) {
    FileType[FileType["CSV"] = 0] = "CSV";
    FileType[FileType["XLSX"] = 1] = "XLSX";
})(FileType = exports.FileType || (exports.FileType = {}));
// async function test(){
//     let downloadReportList = new URLVerification();
//     let downloadListData =  await downloadReportList.getDownloadList(new DataObject(Users.QrUsers[0]),null);
//     let fileName = downloadListData.get(ReportList.TRANSACTION)[0]['FILE NAME'];
//     let fileData = await  new ExcelReader(FileType.CSV,fileName).getHeaders(1);
//     console.log("The value is " + fileData);
// }
// test();
