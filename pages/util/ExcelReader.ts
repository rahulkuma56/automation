import {Csv, Workbook, Worksheet, Xlsx} from "exceljs";
import {NightwatchBrowser} from 'nightwatch';
const Users =require("../../configs/ump/UserConfig");
const DownloadList =require("../../apis/ump/reportAPI/DownloadReportList");
import * as fileSystem from 'fs';
import {resolve} from "dns";

var headers: any = [];
var data = [];
var workbook:Workbook = new Workbook();


function sortArray(firstParam, secondParam) {
    if (firstParam['Transaction_Date'] === secondParam['Transaction_Date']) {
        return 0;
    }
    else {
        return (firstParam['Transaction_Date'] < secondParam['Transaction_Date']) ? 1 : -1;
    }
}

export class ExcelReader{


    public fileReader;
    //public headers;
    public filePath;

    constructor(fileType : FileType, fileName:string){
        let currentLocation = process.cwd();
        if(currentLocation.includes('pages'))
            currentLocation = currentLocation.substring(0,currentLocation.indexOf("pages"));
        let directoyPath = currentLocation+"/data/downloadedFiles/";
         this.filePath = directoyPath +fileName;
        switch (fileType) {
            case FileType.CSV : {
                this.fileReader = workbook.csv.readFile(this.filePath);
                break;
            }

            case FileType.XLSX : {
                this.fileReader = workbook.xlsx.readFile(this.filePath);
                break;
            }

            default : {
                this.fileReader = workbook.csv.readFile(this.filePath);
                break;
            }
        }
    }

    async getHeaders(sheetIndex){
       await this.fileReader.then(function() {
            let sheet: Worksheet =   workbook.getWorksheet(sheetIndex);
             headers = sheet.getRow(1).values;
             for(let index=0;index<headers.length;index++) {
                 headers[index] = headers[index] == undefined ? headers[index] = '' : headers[index];
             }
            console.log("the headers are :- "+headers);

        });
        return headers;
    }

  async  getAllDataFromSheet(sheetIndex){
       await this.fileReader.then(function() {
            let sheet: Worksheet = workbook.getWorksheet(sheetIndex);
            let headers = sheet.getRow(1).values;

            for(var i=1;i<=sheet.rowCount;i++){
                for(var j=1;j<=sheet.columnCount;j++){

                    if (!data[i]) {
                        data[i] = [];
                    }
                    data[i][headers[j]] = sheet.getRow(i).getCell(j).value;
                }
            }
            data.shift();
            data.shift();
            //console.log(data);
            data.sort(sortArray);
        })
        return data;
    }

    async getValuesByColumnName(sheetIndex,columnName){
       await this.fileReader.then(function() {
            let sheet: Worksheet = workbook.getWorksheet(sheetIndex);
            data = [];
            let rowCount = sheet.rowCount;
            for(var i=1;i<=sheet.rowCount;i++){
                for(var j=1;j<=sheet.columnCount;j++){
                    if(sheet.getRow(i).getCell(j).value===columnName){
                        while(i<rowCount) {
                            data[i] = sheet.getRow(i + 1).getCell(j).value;
                            i++;
                        }
                        break;
                    }}
            }
            data.shift();
            console.log(data)
        });
        return data;
    }


    getRowValue(sheetIndex,columnName,uniqueValue){
        this.fileReader.then(function() {
            let sheet: Worksheet = workbook.getWorksheet(sheetIndex);
            let data=[];
            let columnNumber;
            let rowNumber;
            for(var j=1;j<=sheet.columnCount;j++){
                if(sheet.getRow(1).getCell(j).value===columnName){
                    columnNumber=j;
                    break;
                }
            }
            console.log("The column number is :-"+columnNumber);
            for(var i=1;i<=sheet.rowCount;i++){
                if(sheet.getRow(i).getCell(columnNumber).value===uniqueValue){
                    rowNumber=i;
                    break;
                }
            }

            console.log("The row number is :-"+rowNumber);
            this.headers = sheet.getRow(1).values;
            for(var i=1;i<=sheet.rowCount;i++) {
                for (var j = 1; j <= sheet.columnCount; j++) {
                    data[j] = sheet.getRow(rowNumber).getCell(j).value;
                }
                break;
            }
            data.shift();
            console.log("The value of the data is :- "+data)
        })
    }

    async deleteFile() {
        try {
            let path = await decodeURIComponent(this.filePath);
            await fileSystem.unlink(path, err => {
                if (err) throw err;
            });
            console.log("File Successfully Deleted");
        } catch (err) {
            console.error(err)
        }
    }
}

export enum FileType {
    CSV ,
    XLSX
}

// async function test(){
//     let downloadReportList = new URLVerification();
//     let downloadListData =  await downloadReportList.getDownloadList(new DataObject(Users.QrUsers[0]),null);
//     let fileName = downloadListData.get(ReportList.TRANSACTION)[0]['FILE NAME'];
//     let fileData = await  new ExcelReader(FileType.CSV,fileName).getHeaders(1);
//     console.log("The value is " + fileData);
// }
// test();






