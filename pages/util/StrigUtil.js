"use strict";
exports.__esModule = true;
var StrigUtil = /** @class */ (function () {
    function StrigUtil() {
    }
    StrigUtil.toCamelLetter = function (str) {
        return str.toLowerCase().split(' ').map(function (word) {
            return (word.charAt(0).toUpperCase() + word.slice(1));
        }).join(' ');
    };
    return StrigUtil;
}());
exports.StrigUtil = StrigUtil;
