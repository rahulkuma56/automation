import moment = require("moment");
import {StringUtil} from "./StringUtil";
import {Duration, Duration_Slash} from "../../apis/global/APIFilters";

export class DateUtil{

    static convertMiliToDate(dateString){
        let dateNum = Number.parseInt(dateString);
        let updatedDate = moment.unix(dateNum/1000).format("DD MMM, hh:mm A");
        return ""+updatedDate;
    }

    static getDate(dateString) {
        let updatedDate = moment(dateString).format("DD MMM, hh:mm A");
        if (updatedDate == 'Invalid date')
            updatedDate = ""+moment(dateString.substring(4).replace('IST', ''), "MMM DD HH:mm:ss yyyy").format("DD MMM, hh:mm A");
        if (updatedDate == 'Invalid date')
            updatedDate = ""+moment(dateString, "DD/MM/YYY HH:mm:ss").format("DD MMM, hh:mm A");
        return ""+updatedDate;
    }

    static getDateWithFormat(dateString,dateFormat:string) {
        let  updatedDate = ""+moment(dateString,dateFormat).format("DD MMM, hh:mm A");
        return ""+updatedDate;
    }


    static getDateWithoutTime(dateString) : string{
        let updatedDate = ""+moment(dateString, moment.ISO_8601).format("DD MMM YYYY");
        return updatedDate;
    }

    static getDateWithTime(dateString) : string{
        let updatedDate = ""+moment(dateString, moment.ISO_8601).format("DD MMM YYYY, hh:mm A");
        return updatedDate;
    }

    static dateInMilliSeconds(date) {
        let  ts = ""+moment(date).valueOf();
        return   ts;
    }

    static getDateAndMonth(dateString = ''){
        return (dateString=='' ?  ""+moment().format("D MMM"):
        ""+moment(dateString).format("D MMM"));
    }

    static getDateAndMonth2Digit(dateString = ''){
        return (dateString=='' ?  ""+moment().format("DD MMM"):
            ""+moment(dateString).format("D MMM"));
    }

    static getCurrentTimeMilliSeconds(){
        return ""+moment().valueOf()
    }


    static getDateAndFullMonth(dateString = ''){
        if(dateString.includes("/"))
            return this.getDateAndFullMonthForSlash(dateString);
       return (dateString=='' ?  ""+moment().format("D MMMM"):
         ""+moment(dateString).format("D MMMM"));
    }

    static getSingleDateAndFullMonth(dateString = ''){
        if(dateString.includes("/"))
            return this.getSingleDateAndFullMonthForSlash(dateString);
        return (dateString=='' ?  ""+moment().format("DD MMMM"):
            ""+moment(dateString).format("DD MMMM"));
    }


    static getDateAndFullMonthForSlash(dateString = ''){
        return (dateString=='' ?  ""+moment().format("D MMMM"):
            ""+moment(dateString,"DD/MM/YYYY").format("D MMMM"));
    }

    static getSingleDateAndFullMonthForSlash(dateString = ''){
        return (dateString=='' ?  ""+moment().format("DD MMMM"):
            ""+moment(dateString,"DD/MM/YYYY").format("DD MMMM"));
    }

    static getDateForEmailUtil() {
        let updatedDate = moment().format("YYYY_MM_DD_HH");
        return updatedDate;
    }

    static getDateForEmailHeader() {
        let updatedDate = moment().format("DD-MM-YYYY : HH");
        return updatedDate;
    }


    static getDateInNumeric(dateString:string,actualDateFormat:string) {
        dateString = StringUtil.replaceAll(dateString,"/","-");
        let updatedDate = moment(dateString,actualDateFormat ).format("YYYYMMDD");
        if (updatedDate == 'Invalid date')
            updatedDate = moment(dateString,"DD-MM-YYYY").format("YYYYMMDD");
        let numericDate = Number.parseInt(updatedDate);
        return numericDate
    }

    static getTime(dateString) {
        let updatedDate = moment(dateString).format("hh:mm A");
        if (updatedDate == 'Invalid date')
            updatedDate = ""+moment(dateString.substring(4).replace('IST', ''), "MMM DD HH:mm:ss yyyy").format("hh:mm A");
        return ""+updatedDate;
    }

}
//
// function test() {
//     // console.log(DateUtil.getDateInNumeric(Duration_Slash.THIS_WEEK_START_DATE));
//     // console.log(DateUtil.getDateInNumeric("2019/10/04"));
//     // console.log(DateUtil.getDateInNumeric("04/10/2019",'DD/MM/YYYY'));
// }
//
// test();


