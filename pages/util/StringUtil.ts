

export class StringUtil {

    public static toCamelLetter(str:string){
        return str.toLowerCase().split(' ').map(function(word) {
            return (word.charAt(0).toUpperCase() + word.slice(1));
        }).join(' ');
    }

    public static replaceAll(text:string,actual:string,expected:string){
        let updatedText = text.split(actual).join(expected);
        return updatedText;
    }
}

