"use strict";
exports.__esModule = true;
var enum_values_1 = require("enum-values");
var EnumUtil = /** @class */ (function () {
    function EnumUtil() {
    }
    EnumUtil.getKeyByValue = function (enumName, value) {
        var key = enum_values_1.EnumValues.getNameFromValue(enumName, value);
        return key;
    };
    EnumUtil.getNames = function (value) {
        var key = enum_values_1.EnumValues.getNames(value);
        return key;
    };
    return EnumUtil;
}());
exports.EnumUtil = EnumUtil;
