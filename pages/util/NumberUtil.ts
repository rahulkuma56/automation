
const numberFormatter = require('indian-number-format')

export class NumberUtil {

    public static getCommaSepratedNumberUpTo2Digit(number) {

        if(number<0)
            return '-'+ numberFormatter.formatFixed(number*-1, 2);
        else
            return numberFormatter.formatFixed(number, 2);
    }

    public static getCommaSepratedNumber(number) {
        if(number<0)
            return '-'+ numberFormatter.formatFixed(number*-1);
        else
            return numberFormatter.formatFixed(number);

    }

    public static getDecimalWhenRequired(number : any){
        return Math.round(number * 100) / 100
    }


    public static getValueWithUnit(number : any){
        let newNumber = number.toString();
        let digits = newNumber.split(".")[0].length;
        if(digits === 8){
            newNumber = newNumber[0] +'.' + newNumber.substring(1,3) + ' Cr';
        }
        else{
            newNumber = this.getCommaSepratedNumberUpTo2Digit(number);
        }
        return newNumber;
    }
}




