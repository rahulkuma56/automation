import * as number from "random-number";
let crypto = require("crypto");

var rand={
    min:30000,
    max:300000000000,
    integer:true
};

var mobile={
    min:6000000000,
    max:6999999999,
    integer:true
};


export class RandomNumber {

    random() {
       return number(rand);
    }

   static randomString() {

       return crypto.randomBytes(6).toString('hex');
   }

   static getRandomMobileNo() {
       return number(mobile);
   }

   static getCurrentTimeSpan(){
       return new Date().getTime();
   }



}

export default new RandomNumber();