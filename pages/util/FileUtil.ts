import * as fileSystem from "fs";
import {file} from "babel-types";

const replace = require('replace-in-file'); // library reference https://www.npmjs.com/package/replace-in-file#custom-regular-expressions

export class FileUtil{

    private filePath:string

    constructor(filePath:string){
        this.filePath = filePath;
    }

    async replaceContent(fromcontent:string,toContent:string){

        const options = {
            files: this.filePath,
            from: [fromcontent],
            to: toContent,
        };

        try {
            const results = await replace(options)
            console.log('Replacement results:', results);
        }
        catch (error) {
            console.error('Error occurred:', error);
        }

    }

    async deleteFile() {
        try {
            let path = await decodeURIComponent(this.filePath);
            await fileSystem.unlink(path, err => {
                if (err) throw err;
            });
            console.log("File Successfully Deleted");
        } catch (err) {
            console.error(err)
        }
    }

    async checkFileExistence() {
        let result : boolean;
        try {
            let path = await decodeURIComponent(this.filePath);
            result = await fileSystem.existsSync(path);
        } catch(err) {
            console.error(err);
        }
        return result;
    }

    async renameExistingFile(oldFileName:string,newFileName:string) {

        try {
            await fileSystem.renameSync(oldFileName,newFileName);
        }
        catch(err) {
            console.error(err);
        }
    }


}