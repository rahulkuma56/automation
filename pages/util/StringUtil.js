"use strict";
exports.__esModule = true;
var StringUtil = /** @class */ (function () {
    function StringUtil() {
    }
    StringUtil.toCamelLetter = function (str) {
        return str.toLowerCase().split(' ').map(function (word) {
            return (word.charAt(0).toUpperCase() + word.slice(1));
        }).join(' ');
    };
    StringUtil.replaceAll = function (text, actual, expected) {
        var updatedText = text.split(actual).join(expected);
        return updatedText;
    };
    return StringUtil;
}());
exports.StringUtil = StringUtil;
