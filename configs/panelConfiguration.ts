const boss = require ('./boss/bossConfigData');
const ump = require ('./ump/umpConfigData');

let panel;
if(process.env.Panel === 'UMP'){
    panel = ump;
}else if(process.env.Panel === 'BOSS'){
    panel = boss;
} else{
    panel = ump;
}

module.exports = panel;

