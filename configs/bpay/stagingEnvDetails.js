module.exports = {
    "DATABASE": {
        "PGPDB": {
            "server_host": '10.142.51.84',
            'server_port': 22,
            'server_password': 'read',
            'server_user': 'read',
            'mysql_host': 'localhost',
            "mysql_port": 3310,
            "mysql_user": 'pguser',
            "mysql_password": 'pgpass',
            "mysql_database": 'PAYTMPGDB'
        },
        "WALLET": {}
    },
    "URLS": {
        "pgp": "https://pgp-staging.paytm.in",
        "baseurl": "https://ump-staging.paytm.com",
        "oauth": "https://accounts-staging.paytm.in",
        "wallet": "http://wallet-staging.paytm.in"
    },
    "Constants": {
        "x_auth_ump": "zxcs-9098-kls-qw90-xcd",
        "authorization": 'Basic dGVzdGNsaWVudDo5MzhkZWJjYS02NWQyLTQ1NzEtYTEwZC0xMzQwNTNmNzhkNTE=',
        "Client_ID": "testclient",
        "Scope": 'paytm',
        "Client_Secret": "OPBqBHsByVsqYYzv3RsJ4Y5dcrmwzXYy"
    },
    "TestData": {
        "Mask_Verification_Order_ID_List": ['automation1571573937494', '201910211825160022', '201910211827180088'],
        "Mask_Verification_UPI_Order_ID": 'PARCEL323278',
        "Paid_Invoice_ID": "15718217965761",
        "Expired_Invoice_ID": "sachin123",
        "Pending_Invoice_ID": "15644531246208"
    },
    "NavigationUrl": {
        "PREVIEW": 'https://business-staging.paytm.com/link',
        "FACEBOOK": 'www.facebook.com',
        "WHATSAPP": 'https://web.whatsapp.com/',
        "PAYTMCARE": 'https://paytm.com/care'
    }
};
