var boss = require('./boss/bossConfigData');
var ump = require('./ump/umpConfigData');
var panel;
if (process.env.Panel === 'UMP') {
    panel = ump;
}
else if (process.env.Panel === 'BOSS') {
    panel = boss;
}
else {
    panel = ump;
}
module.exports = panel;
