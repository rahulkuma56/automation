"use strict";
var chromedriver = require("chromedriver");
var allure = require("nightwatch-allure-adapter");
module.exports = {
    // this controls whether to abort the test execution when an assertion failed and skip the rest
    // it's being used in waitFor commands and expect assertions
    abortOnAssertionFailure: false,
    // this will overwrite the default polling interval (currently 500ms) for waitFor commands
    // and expect assertions that use retry
    waitForConditionPollInterval: 500,
    // default timeout value in milliseconds for waitFor commands and implicit waitFor value for
    // expect assertions
    // waitForConditionTimeout: 15000,
    // this will cause waitFor commands on elements to throw an error if multiple
    // elements are found using the given locate strategy and selector
    throwOnMultipleElementsReturned: false,
    // controls the timeout time for async hooks. Expects the done() callback to be invoked within this time
    // or an error is thrown
    asyncHookTimeout: 5000,
    before: function (done) {
        console.log("Before Suite");
        chromedriver.start();
        done();
    },
    after: function (done) {
        chromedriver.stop();
        done();
        console.log("After Suite");
    },
    beforeEach: function (browser, done) {
        console.log("Before Each");
        done();
    },
    afterEach: function (browser, done) {
        browser.end();
        done();
        console.log("After Each");
    },
    /**
     * Allure Report for html report.
     */
    reporter: allure.write
};
