import {LoginPage} from '../../../pages/bpay/home/LoginPage';
import {NightwatchBrowser} from 'nightwatch';
import {DataObject} from "../../../apis/bpay/DataObject";
import {AddFundPage} from "../../../pages/bpay/FundingAccount/AddFundPage"
import {FundingAccountPage} from "../../../pages/bpay/FundingAccount/FundingAccountPage";

const  Users =require("../../../configs/bpay/UserConfig");


var login, homePage,fundingAccountPage, addFund;

export = {
    '@tags': ['regression'],
    'Add fund Test': function (client: NightwatchBrowser) {
        login = new LoginPage(client);
        fundingAccountPage = new FundingAccountPage(client);
        addFund = new AddFundPage(client);
        let userDetails = new DataObject(Users.BpayUser[0]);
        login.login(Users.BpayUser[0]);
        client.pause(3000);
        fundingAccountPage.searchSubWallet(1212);
       // fundingAccountPage.clickAddFundButton();
        //addFund.addFund(1);
        fundingAccountPage.assertSubwalletBalance(userDetails,"1212");
        client.pause(20000);
    }
}