import {LoginPage} from '../../../pages/bpay/home/LoginPage';
import {NightwatchBrowser} from 'nightwatch';
import {FundingAccountPage} from "../../../pages/bpay/FundingAccount/FundingAccountPage";

import {DataObject} from "../../../apis/bpay/DataObject";
const  Users =require("../../../configs/bpay/UserConfig");

var login, homePage, fundingAccountPage;


export = {

    '@tags': ['regression'],
    'Bussiness Wallet Balance': function (client: NightwatchBrowser) {
        login = new LoginPage(client);
        fundingAccountPage = new FundingAccountPage(client);
        let userDetails = new DataObject(Users.BpayUser[0]);
        login.login(Users.BpayUser[0]);
        client.pause(3000);
        fundingAccountPage.assertBusinessWalletBalance(userDetails);
        client.pause(20000);
    }
}