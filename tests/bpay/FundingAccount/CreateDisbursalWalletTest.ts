import {LoginPage} from '../../../pages/bpay/home/LoginPage';
import {NightwatchBrowser} from 'nightwatch';
import {NavigationPage} from "../../../pages/bpay/home/NavigationPage";
import {DataObject} from "../../../apis/bpay/DataObject";
import {CreateDisbursalWalletPage} from "../../../pages/bpay/FundingAccount/CreateDisbursalWalletPage";
import {FundingAccountPage} from "../../../pages/bpay/FundingAccount/FundingAccountPage";

const  Users =require("../../../configs/bpay/UserConfig");


var login, homePage, createDisbursalWallet, fundingAccountPage;


export = {

    '@tags': ['regression'],
    'Create Disbursal Account': function (client: NightwatchBrowser) {
        login = new LoginPage(client);
        homePage = new NavigationPage(client);
        fundingAccountPage = new FundingAccountPage(client);
        createDisbursalWallet = new CreateDisbursalWalletPage(client);
        let userDetails = new DataObject(Users.BpayUser[0]);
        login.login(Users.BpayUser[0]);
        client.pause(3000);
        fundingAccountPage.clickOnCreateNewDisbursalWallet();
        createDisbursalWallet.enterAccountNameAndClickCreateAccount();
        createDisbursalWallet.clickOnSaveButton();
        client.pause(20000);
    }
}