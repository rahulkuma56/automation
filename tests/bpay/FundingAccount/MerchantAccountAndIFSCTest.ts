import {LoginPage} from '../../../pages/bpay/home/LoginPage';
import {NightwatchBrowser} from 'nightwatch';
import {DataObject} from "../../../apis/bpay/DataObject";
import {FundingAccountPage} from "../../../pages/bpay/FundingAccount/FundingAccountPage";

const  Users =require("../../../configs/bpay/UserConfig");


var login, homePage;

export = {
    '@tags': ['regression'],
    'Merchant Account (VAN)': function (client: NightwatchBrowser) {
        login = new LoginPage(client);

       let fundingAccountPage = new FundingAccountPage(client);
        let userDetails = new DataObject(Users.BpayUser[0]);
        login.login(Users.BpayUser[0]);
        client.pause(3000);
        fundingAccountPage.assertMerchantAccountNo(Users.BpayUser[0]);
        client.pause(20000);
    }
}