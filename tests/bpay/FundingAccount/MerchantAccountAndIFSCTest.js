"use strict";
var LoginPage_1 = require("../../../pages/bpay/home/LoginPage");
var DataObject_1 = require("../../../apis/bpay/DataObject");
var FundingAccountPage_1 = require("../../../pages/bpay/FundingAccount/FundingAccountPage");
var Users = require("../../../configs/bpay/UserConfig");
var login, homePage;
module.exports = {
    '@tags': ['regression'],
    'Merchant Account (VAN)': function (client) {
        login = new LoginPage_1.LoginPage(client);
        var fundingAccountPage = new FundingAccountPage_1.FundingAccountPage(client);
        var userDetails = new DataObject_1.DataObject(Users.BpayUser[0]);
        login.login(Users.BpayUser[0]);
        client.pause(3000);
        fundingAccountPage.assertMerchantAccountNo(Users.BpayUser[0]);
        client.pause(20000);
    }
};
