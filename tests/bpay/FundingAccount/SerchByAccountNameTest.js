"use strict";
var LoginPage_1 = require("../../../pages/bpay/home/LoginPage");
var DataObject_1 = require("../../../apis/bpay/DataObject");
var FundingAccountPage_1 = require("../../../pages/bpay/FundingAccount/FundingAccountPage");
var Users = require("../../../configs/bpay/UserConfig");
var login, homePage, fundingAccountPage;
module.exports = {
    '@tags': ['regression'],
    'Search Sub Wallet By Account NO': function (client) {
        login = new LoginPage_1.LoginPage(client);
        fundingAccountPage = new FundingAccountPage_1.FundingAccountPage(client);
        var userDetails = new DataObject_1.DataObject(Users.BpayUser[0]);
        login.login(Users.BpayUser[0]);
        client.pause(3000);
        fundingAccountPage.searchSubWalletByAccountname("Demo Subwallet");
        fundingAccountPage.assertSubwalletByAccountName("Demo Subwallet");
        client.pause(20000);
    }
};
