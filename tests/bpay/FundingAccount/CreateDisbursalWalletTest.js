"use strict";
var LoginPage_1 = require("../../../pages/bpay/home/LoginPage");
var NavigationPage_1 = require("../../../pages/bpay/home/NavigationPage");
var DataObject_1 = require("../../../apis/bpay/DataObject");
var CreateDisbursalWalletPage_1 = require("../../../pages/bpay/FundingAccount/CreateDisbursalWalletPage");
var FundingAccountPage_1 = require("../../../pages/bpay/FundingAccount/FundingAccountPage");
var Users = require("../../../configs/bpay/UserConfig");
var login, homePage, createDisbursalWallet, fundingAccountPage;
module.exports = {
    '@tags': ['regression'],
    'Create Disbursal Account': function (client) {
        login = new LoginPage_1.LoginPage(client);
        homePage = new NavigationPage_1.NavigationPage(client);
        fundingAccountPage = new FundingAccountPage_1.FundingAccountPage(client);
        createDisbursalWallet = new CreateDisbursalWalletPage_1.CreateDisbursalWalletPage(client);
        var userDetails = new DataObject_1.DataObject(Users.BpayUser[0]);
        login.login(Users.BpayUser[0]);
        client.pause(3000);
        fundingAccountPage.clickOnCreateNewDisbursalWallet();
        createDisbursalWallet.enterAccountNameAndClickCreateAccount();
        createDisbursalWallet.clickOnSaveButton();
        client.pause(20000);
    }
};
