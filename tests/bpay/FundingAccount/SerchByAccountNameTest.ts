import {LoginPage} from '../../../pages/bpay/home/LoginPage';
import {NightwatchBrowser} from 'nightwatch';
import {DataObject} from "../../../apis/bpay/DataObject";
import {FundingAccountPage} from "../../../pages/bpay/FundingAccount/FundingAccountPage";
const  Users =require("../../../configs/bpay/UserConfig");

var login, homePage, fundingAccountPage;

export = {
    '@tags': ['regression'],
    'Search Sub Wallet By Account NO': function (client: NightwatchBrowser) {
        login = new LoginPage(client);

        fundingAccountPage = new FundingAccountPage(client);
        let userDetails = new DataObject(Users.BpayUser[0]);
        login.login(Users.BpayUser[0]);
        client.pause(3000);
        fundingAccountPage.searchSubWalletByAccountname("Demo Subwallet");
        fundingAccountPage.assertSubwalletByAccountName("Demo Subwallet");
        client.pause(20000);
    }
}