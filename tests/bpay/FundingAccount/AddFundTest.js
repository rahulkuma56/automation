"use strict";
var LoginPage_1 = require("../../../pages/bpay/home/LoginPage");
var DataObject_1 = require("../../../apis/bpay/DataObject");
var AddFundPage_1 = require("../../../pages/bpay/FundingAccount/AddFundPage");
var FundingAccountPage_1 = require("../../../pages/bpay/FundingAccount/FundingAccountPage");
var Users = require("../../../configs/bpay/UserConfig");
var login, homePage, fundingAccountPage, addFund;
module.exports = {
    '@tags': ['regression'],
    'Add fund Test': function (client) {
        login = new LoginPage_1.LoginPage(client);
        fundingAccountPage = new FundingAccountPage_1.FundingAccountPage(client);
        addFund = new AddFundPage_1.AddFundPage(client);
        var userDetails = new DataObject_1.DataObject(Users.BpayUser[0]);
        login.login(Users.BpayUser[0]);
        client.pause(3000);
        fundingAccountPage.searchSubWallet(1212);
        // fundingAccountPage.clickAddFundButton();
        //addFund.addFund(1);
        fundingAccountPage.assertSubwalletBalance(userDetails, "1212");
        client.pause(20000);
    }
};
