"use strict";
exports.__esModule = true;
var moment = require("moment");
exports.Duration = {
    // TODAY_START_DATE const can also be used as the end date for YESTERDAY_START_DATE.
    TODAY_START_DATE: moment().format('YYYY-MM-DD'),
    CURRENT_TIME: moment().format(),
    YESTERDAY_START_DATE: moment().subtract(1, 'day').format('YYYY-MM-DD'),
    LAST_30_DAYS: moment().subtract(30, 'day').format('YYYY-MM-DD'),
    // THIS_WEEK_START_DATE : moment().startOf('week').add(1, 'day').format('YYYY-MM-DD'),
    // THIS_WEEK_START_DATE : moment().startOf('week').add(-6, 'day').format('YYYY-MM-DD'),
    THIS_WEEK_START_DATE: moment().startOf('isoWeek').format('YYYY-MM-DD'),
    THIS_MONTH_START_DATE: moment().startOf('month').format('YYYY-MM-DD'),
    END_DATE: moment().add(1, 'day').format('YYYY-MM-DD'),
    EXPIRY_DATE: moment().add(365, 'day').format('YYYY-MM-DD')
};
exports.Duration_Slash = {
    // TODAY_START_DATE const can also be used as the end date for YESTERDAY_START_DATE.
    TODAY_START_DATE: moment().format('DD/MM/YYYY'),
    YESTERDAY_START_DATE: moment().subtract(1, 'day').format('DD/MM/YYYY'),
    LAST_30_DAYS: moment().subtract(30, 'day').format('DD/MM/YYYY'),
    THIS_WEEK_START_DATE: moment().startOf('isoWeek').format('DD/MM/YYYY'),
    THIS_MONTH_START_DATE: moment().startOf('month').format('DD/MM/YYYY'),
    END_DATE: moment().add(1, 'day').format('DD/MM/YYYY'),
    EXPIRY_DATE: moment().add(365, 'day').format('DD/MM/YYYY')
};
exports.DateFormat = {
    YYYY_HYPHEN: 'YYYY-MM-DD',
    DD_SLASH: "DD/MM/YYYY",
    E_MMM_DD: 'E MMM dd HH:mm:ss z YYYY',
    DD_MM_YY: 'DD/MM/yyyy HH:mm:ss'
};
var OrderType;
(function (OrderType) {
    OrderType["TRANSACTION"] = "ACQUIRING";
    OrderType["REFUND"] = "REFUND";
    OrderType["M2B"] = "M2B";
    OrderType["M2B_REFUND_ACCUIRING"] = "M2B,REFUND,ACQUIRING";
})(OrderType = exports.OrderType || (exports.OrderType = {}));
var TransactionType;
(function (TransactionType) {
    TransactionType["ACQUIRING"] = "Payment Received";
    TransactionType["REFUND"] = "Refund";
    TransactionType["M2B"] = "Transferred to Bank";
})(TransactionType = exports.TransactionType || (exports.TransactionType = {}));
var Status;
(function (Status) {
    Status["ALL"] = "";
    Status["SUCCESS"] = "SUCCESS";
    Status["PENDING"] = "PENDING";
    Status["FAILURE"] = "FAILURE";
})(Status = exports.Status || (exports.Status = {}));
var SubUserStatus;
(function (SubUserStatus) {
    SubUserStatus["ACCEPTED"] = "Joined";
    SubUserStatus["PENDING"] = "Pending";
    SubUserStatus["REJECTED"] = "Declined";
})(SubUserStatus = exports.SubUserStatus || (exports.SubUserStatus = {}));
var Method;
(function (Method) {
    Method["POST"] = "POST";
    Method["GET"] = "GET";
    Method["DELETE"] = "DELETE";
    Method["PUT"] = "PUT";
})(Method = exports.Method || (exports.Method = {}));
var ContentType;
(function (ContentType) {
    ContentType["OCTETSTREAM"] = "application/octet-stream";
    ContentType["TEXTPLAIN"] = "text/plain";
    ContentType["JSON"] = "application/json";
    ContentType["URLENCODED"] = "application/x-www-form-urlencoded";
})(ContentType = exports.ContentType || (exports.ContentType = {}));
var FilterType;
(function (FilterType) {
    FilterType["REFUND_ID"] = "bizOrderId";
    FilterType["ORDER_ID"] = "merchantTransId";
    FilterType["UTR_NO"] = "utrNo";
    FilterType["UTR_ID"] = "settlementBillId";
})(FilterType = exports.FilterType || (exports.FilterType = {}));
var M2BStatus;
(function (M2BStatus) {
    M2BStatus["SUCCESS"] = "Bank Transfer Successful";
    M2BStatus["PENDING"] = "Bank Transfer Pending";
})(M2BStatus = exports.M2BStatus || (exports.M2BStatus = {}));
var PayMode;
(function (PayMode) {
    PayMode["UPI"] = "UPI";
    PayMode["PPBL"] = "Paytm Payments Bank";
    PayMode["PPI"] = "Paytm Wallet";
    PayMode["BALANCE"] = "Paytm Wallet";
    PayMode["CC"] = "Credit Card";
    PayMode["CREDIT_CARD"] = "Credit Card";
    PayMode["DEBIT_CARD"] = "Debit Card";
    PayMode["DC"] = "Debit Card";
    PayMode["NB"] = "Net Banking";
    PayMode["NET_BANKING"] = "Net Banking";
    PayMode["COD"] = "COD";
    PayMode["EMI"] = "EMI";
    PayMode["BANK_EXPRESS"] = "Bank Express";
    PayMode["IMPS"] = "IMPS";
    PayMode["ATM"] = "ATM Card";
    PayMode["MP_COD"] = "Cash on Delivery";
    PayMode["CASH_COUPON"] = "Cash Coupon";
    PayMode["PREPAID_CARD"] = "Paytm Wallet";
    PayMode["HYBRID_PAYMENT"] = "Hybrid Payment";
    PayMode["PAYTM_DIGITAL_CREDIT"] = "Paytm Postpaid";
    PayMode["LOYALTY_POINTS"] = "Paytm Loyalty Points";
    PayMode["MULTIPLE_PAYMODES"] = "Credit Card, Debit Card, Net Banking";
})(PayMode = exports.PayMode || (exports.PayMode = {}));
var MonthlyAcceptanceLimit;
(function (MonthlyAcceptanceLimit) {
    MonthlyAcceptanceLimit["UPI"] = "UPILimit";
    MonthlyAcceptanceLimit["PPBL"] = "PPBLLimit";
    MonthlyAcceptanceLimit["PPI"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["BALANCE"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["CC"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["CREDIT_CARD"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["DEBIT_CARD"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["DC"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["NB"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["NET_BANKING"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["COD"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["EMI"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["BANK_EXPRESS"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["IMPS"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["ATM"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["MP_COD"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["CASH_COUPON"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["PREPAID_CARD"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["HYBRID_PAYMENT"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["PAYTM_DIGITAL_CREDIT"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["LOYALTY_POINTS"] = "monthlyCollectionLimit";
    MonthlyAcceptanceLimit["MULTIPLE_PAYMODES"] = "monthlyCollectionLimit";
})(MonthlyAcceptanceLimit = exports.MonthlyAcceptanceLimit || (exports.MonthlyAcceptanceLimit = {}));
