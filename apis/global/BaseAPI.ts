import * as request from "request-promise";
const expect = require('chai').expect;
import { NightwatchBrowser } from 'nightwatch';
import {Assert} from "../../pages/bpay/base/Assert";

class BaseAPI{

    execute<T={}>(option) {
        console.log('\nRequest is :: ');
        console.log(option)
        return new Promise<T>(( resolve, reject) => {
            return request(option).then((response)=>{
                console.log('\nResponse is :: ');
                console.log(response.body)
                expect(response.statusCode).to.be.equal(200);
                resolve(response.body);
            }).catch(function(error) {
                console.log(error);
                expect(undefined).to.be.equal(200);
            });
        })
    }

    executeAndVerify<T={}>(browser:NightwatchBrowser,option,apiName:String) {
        console.log('\nRequest is :: ');
        console.log(option)
        return new Promise<T>(( resolve) => {
            return request(option).then((response)=>{
                console.log('\nResponse is :: ');
                console.log(response.body);
                Assert.isValueEqual(browser,response.statusCode,200,"Verify the response code of API '"+apiName+"' is 200.");
                resolve(response.body);
            }).catch(function(error) {
                Assert.isValueEqual(browser,false,true,"Unable to fetch the details for API :: '"+apiName+"'.");
                console.log(error);
            });
        })
    }
}

export default new BaseAPI();