"use strict";
exports.__esModule = true;
var request = require("request-promise");
var expect = require('chai').expect;
var Assert_1 = require("../../pages/bpay/base/Assert");
var BaseAPI = /** @class */ (function () {
    function BaseAPI() {
    }
    BaseAPI.prototype.execute = function (option) {
        console.log('\nRequest is :: ');
        console.log(option);
        return new Promise(function (resolve, reject) {
            return request(option).then(function (response) {
                console.log('\nResponse is :: ');
                console.log(response.body);
                expect(response.statusCode).to.be.equal(200);
                resolve(response.body);
            })["catch"](function (error) {
                console.log(error);
                expect(undefined).to.be.equal(200);
            });
        });
    };
    BaseAPI.prototype.executeAndVerify = function (browser, option, apiName) {
        console.log('\nRequest is :: ');
        console.log(option);
        return new Promise(function (resolve) {
            return request(option).then(function (response) {
                console.log('\nResponse is :: ');
                console.log(response.body);
                Assert_1.Assert.isValueEqual(browser, response.statusCode, 200, "Verify the response code of API '" + apiName + "' is 200.");
                resolve(response.body);
            })["catch"](function (error) {
                Assert_1.Assert.isValueEqual(browser, false, true, "Unable to fetch the details for API :: '" + apiName + "'.");
                console.log(error);
            });
        });
    };
    return BaseAPI;
}());
exports["default"] = new BaseAPI();
