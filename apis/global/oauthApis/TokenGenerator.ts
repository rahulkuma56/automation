import * as token from "./PaytmToken"
import * as request from "../BaseAPI";
import {Method} from "../APIFilters";
import {Constants} from "../../../data/ump/Constants";
const config =require("../../../configs/ump/EnvConfig");

export class TokenGenerator {

    async getPaytmToken(userName, password) {
        return await token.default.getPaytmToken(userName, password);
    }


    async option(userName, password) {
        return {
            uri: config.URLS.oauth + Constants.User_Token + await this.getPaytmToken(userName, password),
            method:
            Method.GET,
            resolveWithFullResponse: true,
            headers:
                {
                    'authorization': config.Constants.authorization,
                    'cache-control': Constants.NO_CACHE
                },

        }
    }

    async getWalletToken(userName, password) {

        var data = await request.default.execute<string>(await this.option(userName, password));
        var data2 = JSON.parse(data).tokens;
        var token;

        for (var i = 0; i < data2.length; i++) {

            if (data2[i].scope == "wallet") {
                token = data2[i].access_token;
                break;
            }
        }
        return token;
    }

}
//
// function generateTokenf() {
//
//     console.log("Toekn ::::"+new TokenGenerator().getWalletToken('divya1.arora@paytm.com','paytm@123'));
//
// }
// generateTokenf();



