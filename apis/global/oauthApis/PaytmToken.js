"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var config = require("../../../configs/ump/EnvConfig");
var request = require("../BaseAPI");
var tokenHelper = require("./TokenHelper");
var Constants_1 = require("../../../data/ump/Constants");
var APIFilters_1 = require("../APIFilters");
var PaytmToken = /** @class */ (function () {
    function PaytmToken() {
    }
    PaytmToken.prototype.options = function (code) {
        return {
            uri: config.URLS.oauth + Constants_1.Constants.Oauth_Token,
            method: APIFilters_1.Method.POST,
            resolveWithFullResponse: true,
            headers: {
                'authorization': config.Constants.authorization,
                'cache-control': Constants_1.Constants.NO_CACHE,
                'content-type': Constants_1.Constants.FORM_URL_ENCODED
            },
            form: {
                'grant_type': Constants_1.Constants.AUTHORIZATION_CODE,
                'code': code,
                'client_id': config.Constants.Client_ID,
                'scope': config.Constants.Scope,
                'client_secret': config.Constants.Client_Secret
            }
        };
    };
    PaytmToken.prototype.getPaytmToken = function (username, password) {
        return __awaiter(this, void 0, void 0, function () {
            var code, data, e_1, token;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, tokenHelper["default"].getHelperToken(username, password)];
                    case 1:
                        code = _a.sent();
                        console.log("Code :: " + code);
                        return [4 /*yield*/, request["default"].execute(this.options(code))];
                    case 2:
                        data = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 4];
                    case 4:
                        token = JSON.parse(data)['access_token'];
                        console.log("Token :: " + token);
                        return [2 /*return*/, token];
                }
            });
        });
    };
    return PaytmToken;
}());
// function test() {
//     new PaytmToken().getPaytmToken('8840888219','paytm@123');
// }
//
// test();
//
exports["default"] = new PaytmToken();
