const config =require("../../../configs/ump/EnvConfig");
import * as request from '../BaseAPI';
import * as tokenHelper from "./TokenHelper";
import {Constants} from "../../../data/ump/Constants";
import {Method} from "../APIFilters";
class PaytmToken {

  private  options(code) {
        return {
            uri:config.URLS.oauth+Constants.Oauth_Token,
            method:Method.POST,
            resolveWithFullResponse: true,
            headers:{
                'authorization':config.Constants.authorization,
                'cache-control': Constants.NO_CACHE,
                'content-type' : Constants.FORM_URL_ENCODED
            },
            form:{
                'grant_type':Constants.AUTHORIZATION_CODE,
                'code':code,
                'client_id':config.Constants.Client_ID,
                'scope':config.Constants.Scope,
                'client_secret': config.Constants.Client_Secret
            }
        }
    }


   async getPaytmToken(username, password) {
       let code,data;
      try {
          code = await tokenHelper.default.getHelperToken(username,password);
          console.log("Code :: "+code);
          data = await request.default.execute(this.options(code));
      }catch (e) {
          console.log(e);
      }
       let token = JSON.parse(data)['access_token'];
       console.log("Token :: "+token)
       return token;
    }
}

// function test() {
//     new PaytmToken().getPaytmToken('8840888219','paytm@123');
// }
//
// test();
//
export default new PaytmToken();