const config =require("../../../configs/ump/EnvConfig");
import * as requestBase from '../BaseAPI';
import {ContentType} from "../APIFilters";
import {Constants} from "../../../data/ump/Constants";
import {Method} from "../APIFilters";
class TokenHelper {

   private options(username,password)
    {
        return {
            uri:config.URLS.oauth+Constants.authorize,
            method:Method.POST,
            resolveWithFullResponse: true,
            headers:{
                'authorization':config.Constants.authorization,
                'content-type': ContentType.URLENCODED,
                'cache-control': Constants.NO_CACHE
            },
            form:{
                'response_type':'code',
                'client_id':config.Constants.Client_ID,
                'do_not_redirect':'true',
                'scope':config.Constants.Scope,
                'username':username,
                'password':password

            }

        }

    }


   async getHelperToken(username,password) {
        let data = await requestBase.default.execute<string>(this.options(username,password));
        console.log(JSON.parse(data).code);
        return JSON.parse(data).code;
    }

}
//
// function test() {
//     new TokenHelper().getHelperToken('8840888219','paytm@123');
// }
// test();
var object = new TokenHelper();
export default  new TokenHelper();