"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var token = require("./PaytmToken");
var request = require("../BaseAPI");
var APIFilters_1 = require("../APIFilters");
var Constants_1 = require("../../../data/ump/Constants");
var config = require("../../../configs/ump/EnvConfig");
var TokenGenerator = /** @class */ (function () {
    function TokenGenerator() {
    }
    TokenGenerator.prototype.getPaytmToken = function (userName, password) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, token["default"].getPaytmToken(userName, password)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TokenGenerator.prototype.option = function (userName, password) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = {};
                        _b = config.URLS.oauth + Constants_1.Constants.User_Token;
                        return [4 /*yield*/, this.getPaytmToken(userName, password)];
                    case 1: return [2 /*return*/, (_a.uri = _b + (_c.sent()),
                            _a.method = APIFilters_1.Method.GET,
                            _a.resolveWithFullResponse = true,
                            _a.headers = {
                                'authorization': config.Constants.authorization,
                                'cache-control': Constants_1.Constants.NO_CACHE
                            },
                            _a)];
                }
            });
        });
    };
    TokenGenerator.prototype.getWalletToken = function (userName, password) {
        return __awaiter(this, void 0, void 0, function () {
            var data, _a, _b, data2, token, i;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = request["default"]).execute;
                        return [4 /*yield*/, this.option(userName, password)];
                    case 1: return [4 /*yield*/, _b.apply(_a, [_c.sent()])];
                    case 2:
                        data = _c.sent();
                        data2 = JSON.parse(data).tokens;
                        for (i = 0; i < data2.length; i++) {
                            if (data2[i].scope == "wallet") {
                                token = data2[i].access_token;
                                break;
                            }
                        }
                        return [2 /*return*/, token];
                }
            });
        });
    };
    return TokenGenerator;
}());
exports.TokenGenerator = TokenGenerator;
//
// function generateTokenf() {
//
//     console.log("Toekn ::::"+new TokenGenerator().getWalletToken('divya1.arora@paytm.com','paytm@123'));
//
// }
// generateTokenf();
