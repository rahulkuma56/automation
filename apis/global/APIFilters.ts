import moment = require("moment");


export const Duration  = {
    // TODAY_START_DATE const can also be used as the end date for YESTERDAY_START_DATE.
    TODAY_START_DATE : moment().format('YYYY-MM-DD'),
    CURRENT_TIME : moment().format(),
    YESTERDAY_START_DATE : moment().subtract(1, 'day').format('YYYY-MM-DD'),
    LAST_30_DAYS : moment().subtract(30, 'day').format('YYYY-MM-DD'),
    // THIS_WEEK_START_DATE : moment().startOf('week').add(1, 'day').format('YYYY-MM-DD'),
    // THIS_WEEK_START_DATE : moment().startOf('week').add(-6, 'day').format('YYYY-MM-DD'),
    THIS_WEEK_START_DATE : moment().startOf('isoWeek').format('YYYY-MM-DD'),
    THIS_MONTH_START_DATE : moment().startOf('month').format('YYYY-MM-DD'),
    END_DATE : moment().add(1, 'day').format('YYYY-MM-DD'),
    EXPIRY_DATE : moment().add(365, 'day').format('YYYY-MM-DD')
}


export const Duration_Slash  = {
    // TODAY_START_DATE const can also be used as the end date for YESTERDAY_START_DATE.
    TODAY_START_DATE : moment().format('DD/MM/YYYY'),
    YESTERDAY_START_DATE : moment().subtract(1, 'day').format('DD/MM/YYYY'),
    LAST_30_DAYS : moment().subtract(30, 'day').format('DD/MM/YYYY'),
    THIS_WEEK_START_DATE : moment().startOf('isoWeek').format('DD/MM/YYYY'),
    THIS_MONTH_START_DATE : moment().startOf('month').format('DD/MM/YYYY'),
    END_DATE : moment().add(1, 'day').format('DD/MM/YYYY'),
    EXPIRY_DATE : moment().add(365, 'day').format('DD/MM/YYYY')
}

export const DateFormat ={
    YYYY_HYPHEN : 'YYYY-MM-DD',
    DD_SLASH : "DD/MM/YYYY",
    E_MMM_DD :'E MMM dd HH:mm:ss z YYYY',// Tue May 07 11:55:33 IST 2019
   DD_MM_YY : 'DD/MM/yyyy HH:mm:ss'

}

export enum OrderType {
    TRANSACTION = 'ACQUIRING',
    REFUND = 'REFUND',
    M2B = 'M2B',
    M2B_REFUND_ACCUIRING = "M2B,REFUND,ACQUIRING"
}

export enum TransactionType {
    ACQUIRING = 'Payment Received',
    REFUND = 'Refund',
    M2B = "Transferred to Bank"
}

export enum Status {
    ALL = '',
    SUCCESS = 'SUCCESS',
    PENDING = 'PENDING',
    FAILURE = 'FAILURE'
}

export enum SubUserStatus {
         ACCEPTED = 'Joined',
        PENDING = 'Pending',
        REJECTED= 'Declined'
}

export enum Method {
    POST = 'POST',
    GET = 'GET',
    DELETE = 'DELETE',
    PUT = 'PUT'
}

export enum ContentType {
    OCTETSTREAM='application/octet-stream',
    TEXTPLAIN='text/plain',
    JSON = 'application/json',
    URLENCODED = 'application/x-www-form-urlencoded',
}

export enum FilterType {
    REFUND_ID = 'bizOrderId',
    ORDER_ID = 'merchantTransId',
    UTR_NO = 'utrNo',
    UTR_ID = 'settlementBillId'
}

export enum M2BStatus{
    SUCCESS  = "Bank Transfer Successful",
    PENDING = "Bank Transfer Pending"
}

export enum PayMode {
    UPI= 'UPI',
    PPBL= 'Paytm Payments Bank',
    PPI= 'Paytm Wallet',
    BALANCE = 'Paytm Wallet',
    CC= 'Credit Card',
    CREDIT_CARD = 'Credit Card',
    DEBIT_CARD = 'Debit Card',
    DC= 'Debit Card',
    NB= 'Net Banking',
    NET_BANKING = 'Net Banking',
    COD= 'COD',
    EMI= 'EMI',
    BANK_EXPRESS= 'Bank Express',
    IMPS= 'IMPS',
    ATM= 'ATM Card',
    MP_COD= 'Cash on Delivery',
    CASH_COUPON= 'Cash Coupon',
    PREPAID_CARD= 'Paytm Wallet',
    HYBRID_PAYMENT= 'Hybrid Payment',
    PAYTM_DIGITAL_CREDIT= 'Paytm Postpaid',
    LOYALTY_POINTS= 'Paytm Loyalty Points',
    MULTIPLE_PAYMODES= 'Credit Card, Debit Card, Net Banking'

}


export enum MonthlyAcceptanceLimit {
    UPI= 'UPILimit',
    PPBL= 'PPBLLimit',
    PPI= 'monthlyCollectionLimit',
    BALANCE = 'monthlyCollectionLimit',
    CC= 'monthlyCollectionLimit',
    CREDIT_CARD = 'monthlyCollectionLimit',
    DEBIT_CARD = 'monthlyCollectionLimit',
    DC= 'monthlyCollectionLimit',
    NB= 'monthlyCollectionLimit',
    NET_BANKING = 'monthlyCollectionLimit',
    COD= 'monthlyCollectionLimit',
    EMI= 'monthlyCollectionLimit',
    BANK_EXPRESS= 'monthlyCollectionLimit',
    IMPS= 'monthlyCollectionLimit',
    ATM= 'monthlyCollectionLimit',
    MP_COD= 'monthlyCollectionLimit',
    CASH_COUPON= 'monthlyCollectionLimit',
    PREPAID_CARD= 'monthlyCollectionLimit',
    HYBRID_PAYMENT= 'monthlyCollectionLimit',
    PAYTM_DIGITAL_CREDIT= 'monthlyCollectionLimit',
    LOYALTY_POINTS= 'monthlyCollectionLimit',
    MULTIPLE_PAYMODES= 'monthlyCollectionLimit'

}

