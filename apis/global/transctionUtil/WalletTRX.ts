import BaseAPI from "../../global/BaseAPI";
import {Constants} from "../../../data/ump/Constants";
import {ContentType} from "../APIFilters";
import {RandomNumber} from "../../../pages/util/RandomNumber";
const Users = require("../../../configs/ump/UserConfig");

class WalletTRX {

    options(merchantMID:string,trxAmount:string) {
        return {
            method: 'POST',
            url: 'https://pgp-staging.paytm.in/theia/HANDLER_IVR/CLW_APP_PAY',
            resolveWithFullResponse: true,
            qs: {
                JsonData:
                    '{"CheckSum":"",' +
                    '"Channel":"WEB",' +
                    '"DeviceId":"9650788700",' +
                    '"OrderId":"automation'+RandomNumber.getCurrentTimeSpan()+'"'+',' +
                    '"TokenType":"OAUTH",' +
                    '"SSOToken":"138fbd28-3bc0-45ad-9d97-20937f715400",' +
                    '"AuthMode":"USRPWD",' +
                    '"ReqType":"CLW_APP_PAY",' +
                    '"EMAIL":"anju.kumari@paytm.com",' +
                    '"IndustryType":"Retail",' +
                    '"AppIP":"",' +
                    '"MSISDN":"1234",' +
                    '"TxnAmount":'+trxAmount+',' +
                    '"MId":"'+merchantMID+'",' +
                    '"PaymentMode":"PPI",' +
                    '"CustomerId":"1234567",' +
                    '"Currency":"INR"}'
            },
            headers: {
                "cache-control":  Constants.NO_CACHE,
                "content-type":  ContentType.JSON,
            }
        }
    }
    async  walletTRX(trxAmount:string) {
        let data = [];
        for(var key of Object.keys(Users) ) {
            for(let index=0; index < Users[key].length;index++ ) {
                data[index] =   await BaseAPI.execute(this.options(Users[key][index].mid,trxAmount));
            }
        }
        return data;
    }
}

async function doTransaction() {
    let object = new WalletTRX();

    object.walletTRX('9.1')
}

doTransaction();
