import * as request from "../BaseAPI";
const config = require ('../../configs/EnvConfig');
import * as number from "../../../pages/util/RandomNumber";
import * as DomParser from "dom-parser";
import {Method} from "../APIFilters";
import {AddMoney} from "./AddMoney";
import BaseAPI from "../BaseAPI";
import {Constants} from "../../../data/ump/Constants";
const Token=require('../../../data/ump/Token');
const User=require('../../../configs/ump/UserConfig');
var chai=require('chai');
var expect=chai.expect;



export class ProcessTransaction {

    private option(user, amount) {
        return {
            uri: config.URLS.pgp + Constants.PROCESS_TRANSACTION,
            method: Method.POST,
            resolveWithFullResponse:true,
            form: {
                ORDER_ID: 'UMP_AUTOMATION_' + number.default.random(),
                MID: user.mid,
                INDUSTRY_TYPE_ID: 'Retail',
                CHANNEL_ID: 'WEB',
                TXN_AMOUNT: amount,
                AUTH_MODE: '3D',
                WEBSITE: 'retail',
                SSO_TOKEN: Token[user.name],
                TOKEN_TYPE: 'SSO',
                PAYMENT_TYPE_ID: 'PPI',
                REQUEST_TYPE: 'OFFLINE'
            },
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            }
        };

    };

    async getTransactionDetail(user, amount) {
        let txnData = {};
        let data = await request.default.execute(this.option(user, amount));
        var parser = new DomParser();
        var el = parser.parseFromString(data);
        txnData["mid"] = el.getElementsByName('MID')[0].getAttribute("value");
        txnData["txnId"] = el.getElementsByName('TXNID')[0].getAttribute("value");
        txnData["amount"] = el.getElementsByName('TXNAMOUNT')[0].getAttribute("value");
        txnData["orderId"] = el.getElementsByName("ORDERID")[0].getAttribute("value");
        console.log(txnData);
        return txnData;
    }


    async makeTransaction(user, amount) {
        let data = await BaseAPI.execute(this.option(user, amount));
        let parser = new DomParser();
        let parse = parser.parseFromString(data);
        let status = parse.getElementsByName("STATUS")[0].getAttribute("value");
        expect(status).to.be.equals("TXN_SUCCESS");
    };
}

// function test() {
//     let trx = new ProcessTransaction().makeTransaction(User.QrUsers[0],1.9);
// }
//
// test();




