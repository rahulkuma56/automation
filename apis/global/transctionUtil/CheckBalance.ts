const config =require("../../../configs/ump/EnvConfig");
import * as request from "../BaseAPI"
const Token=require('../../../data/ump/Token');
class CheckBalance {
    options(user) {
        return{
            uri: config.URLS.wallet + '/wallet-web/checkBalance',
            headers:{
                ssotoken:Token[user.name]
            }
        }

    }


  async  checkBalance(user) {
        let data=await request.default.execute(this.options(user));
        console.log(data);
        return data;
    }
}
