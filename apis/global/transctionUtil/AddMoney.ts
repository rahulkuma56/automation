import * as number from "../../../pages/util/RandomNumber";
import * as request from "../BaseAPI";
const config =require("../../../configs/ump/EnvConfig");
const Token=require('../../../data/ump/Token');
const User=require('../../../configs/ump/UserConfig');


export class AddMoney {

    option(user, amount) {


        let num = number.default.random();

        return {

            uri: config.URLS.wallet + '/wallet-web/AddMoney',
            method: "POST",

            resolveWithFullResponse:true,


            body: {
                request: {
                    'pgTxnId': num,
                    'txnAmount': amount,
                    'txnCurrency': "INR",
                    'txnStatus': "SUCCESS",
                    'merchantOrderId': num,
                    'merchantGuid': "125fd26c-4d98-11e2-b20c-e89a8ff309ea",
                    'pgResponseCode': "01",
                    'pgResponseMessage': "Txn Successful.",
                    'bankTxnId': num,
                    'bankName': "ICICI",
                    'gatewayName': "ICICI",
                    'paymentMode': "CC",
                    'binNumber': null,
                    'cardType': null,
                    'mid': "klbGlV59135347348753",
                    'requestType': "WEB"
                },
                metadata: "INR",
                ipAddress: "127.0.0.1",
                platformName: "PayTM",
                operationType: "ADD_MONEY_VIA_MERCHANT",
            },

            headers: {
                'ssotoken': Token[user.name],
            },

            json: true


        }


    };

    async addMoney(user, amount) {
        let data = await request.default.execute(this.option(user, amount));
        console.log(data);
        return data;
    };
}



