import * as request from "../BaseAPI";
const config =require("../../configs/EnvConfig");
import {ProcessTransaction} from "./ProcessTransaction";

const transaction=new ProcessTransaction();

export class Refund {

    options(mid,txnId,orderId,amount) {

        return {
            uri: config.URLS.pgp + "/refund/HANDLER_INTERNAL/REFUND",
            method:"POST",
            body:{
                "MID": mid,
                "TXNID": txnId,
                "ORDERID": orderId,
                "REFID": orderId,
                "REFUNDAMOUNT": amount,
                "TXNTYPE": "REFUND"
            },
            headers: {
                'content-type': 'application/json'
            },

            json:true
        }
    }


   async refund(user,amount)
   {
        let txnDetail=await transaction.getTransactionDetail(user,amount);
        console.log(txnDetail);
        let resp=await request.default.execute(this.options(user,txnDetail['txnId'],txnDetail['orderId'],amount));
         console.log(resp);

    }

}



