import { NightwatchBrowser } from 'nightwatch';
const Token = require ('../../../data/bpay/token');
const config =require("../../../configs/bpay/EnvConfig");
import {Constants} from "../../../data/bpay/constants";
import {ContentType, Status} from "../../global/APIFilters";
const Users =require("../../../configs/bpay/UserConfig");
import {DataObject} from "../../bpay/DataObject";
import * as BaseAPI from "../../global/BaseAPI";
import {NumberUtil} from "../../../pages/util/NumberUtil";

export class SubwalletDetailApi {

    public END_POINT = Constants.subwalletDetail;

    options(dataObject: DataObject) {
        return {
            uri: config.URLS.baseurl + this.END_POINT,
            method: 'GET',
            resolveWithFullResponse: true,

            headers: {
                'cache-control': 'no-cache',
                'x-user-mid': dataObject.mid,
                'x-user-token': "beb63e60-7df5-4233-9d89-27f86f0e6300",
                'content-type': ContentType.JSON,
                'x-auth-ump': config.Constants.x_auth_ump
            }
        }
    }

    async getList(dataobject: DataObject) {
        let data = await BaseAPI.default.execute<string>(this.options(dataobject));
        return JSON.parse(data);
    }

    async getSubwalletBalance(transactionListData: DataObject,subWalletAccountName){
        let response = await BaseAPI.default.execute<string>(this.options(transactionListData));
        let balance;
        let subwalletDetailList = JSON.parse(response);
        for(let list of subwalletDetailList){
            if(list['walletName']===subWalletAccountName){
                balance=list['walletBalance'];
                break;
            }
        }
        await console.log(NumberUtil.getCommaSepratedNumberUpTo2Digit(balance));
        return NumberUtil.getCommaSepratedNumberUpTo2Digit(balance);

    }

}