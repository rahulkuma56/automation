import { NightwatchBrowser } from 'nightwatch';
const Token = require ('../../../data/bpay/token');
const config =require("../../../configs/bpay/EnvConfig");
import {Constants} from "../../../data/bpay/constants";
import {ContentType, Status} from "../../global/APIFilters";
const Users =require("../../../configs/bpay/UserConfig");
import {DataObject} from "../../bpay/DataObject";
import * as BaseAPI from "../../global/BaseAPI";
import {NumberUtil} from "../../../pages/util/NumberUtil";

export class MerchantProfileDetailApi {

    public END_POINT = Constants.merchantProfileDetail;

    options(dataObject: DataObject) {
        return {
            uri: config.URLS.baseurl + this.END_POINT,
            method: 'GET',
            resolveWithFullResponse: true,

            headers: {
                'cache-control': 'no-cache',
                'x-user-mid': dataObject.mid,
                'x-user-token': "beb63e60-7df5-4233-9d89-27f86f0e6300",
                'content-type': ContentType.JSON,
                'x-auth-ump': config.Constants.x_auth_ump
            }
        }
    }

    async getList(dataobject: DataObject) {
        let data = await BaseAPI.default.execute<string>(this.options(dataobject));
        await console.log(JSON.parse(data));
        return JSON.parse(data);
    }

    async getVAN(dataobject: DataObject) {
        let balanceDetails = await BaseAPI.default.execute<string>(this.options(dataobject));
        let responseInJson = JSON.parse(balanceDetails);
        await console.log("**************** \n Json parsed value: " + responseInJson['van']);
        return responseInJson['van'];
    }

}

    /*async function test() {
    let val = await new MerchantProfileDetailApi().getList(Users.BpayUser[0]);
    let val1 = await new MerchantProfileDetailApi().getVAN(Users.BpayUser[0]);
}
test();*/

