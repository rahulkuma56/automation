import { NightwatchBrowser } from 'nightwatch';
const Token = require ('../../../data/bpay/token');
const config =require("../../../configs/bpay/EnvConfig");
import {Constants} from "../../../data/bpay/constants";
import {ContentType, Status} from "../../global/APIFilters";
const Users =require("../../../configs/bpay/UserConfig");
import {DataObject} from "../../bpay/DataObject";
import * as BaseAPI from "../../global/BaseAPI";

export class BusinessWalletBalanceApI {

    public END_POINT = Constants.fundingAccount;

    options(dataObject: DataObject) {
        return {
            uri: config.URLS.baseurl + this.END_POINT,
            method: 'GET',
            resolveWithFullResponse: true,

            headers:{
                'cache-control': 'no-cache',
                'x-user-mid': dataObject.mid,
                'x-user-token': "beb63e60-7df5-4233-9d89-27f86f0e6300",
                'content-type': ContentType.JSON,
                'x-auth-ump': config.Constants.x_auth_ump
            }
        }
    }

    async getList(dataobject:DataObject) {
        let data = await BaseAPI.default.execute<string>(this.options(dataobject));
        return JSON.parse(data);
    }

    async getRefactoredData(transactionListData: DataObject) {
        let balanceDetails = await BaseAPI.default.execute<string>(this.options(transactionListData));
        let walletDetailList = [];
    }

}

// async function test() {
//     let val = await new BusinessWalletBalanceApI().getList(Users.BpayUser[0]);
//     let val1 = await new BusinessWalletBalanceApI().getRefactoredData(Users.BpayUser[0]);
// }
// test();



