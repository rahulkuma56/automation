import { NightwatchBrowser } from 'nightwatch';
const Token = require ('../../../data/bpay/token');
const config =require("../../../configs/bpay/EnvConfig");
import {Constants} from "../../../data/bpay/constants";
import {ContentType, Status} from "../../global/APIFilters";
const Users =require("../../../configs/bpay/UserConfig");
import {DataObject} from "../../bpay/DataObject";
import * as BaseAPI from "../../global/BaseAPI";
import {NumberUtil} from "../../../pages/util/NumberUtil";

export class BusinessWalletBalanceApI {

    public END_POINT = Constants.fundingAccount;

    options(dataObject: DataObject) {
        return {
            uri: config.URLS.baseurl + this.END_POINT,
            method: 'GET',
            resolveWithFullResponse: true,

            headers:{
                'cache-control': 'no-cache',
                'x-user-mid': dataObject.mid,
                'x-user-token': "beb63e60-7df5-4233-9d89-27f86f0e6300",
                'content-type': ContentType.JSON,
                'x-auth-ump': config.Constants.x_auth_ump
            }
        }
    }

    async getList(dataobject:DataObject) {
        let data = await BaseAPI.default.execute<string>(this.options(dataobject));
        return JSON.parse(data);
    }

    async getBusinessWalletBalance(transactionListData: DataObject) {
        let balanceDetails = await BaseAPI.default.execute<string>(this.options(transactionListData));
        let walletDetailList = [];
        let balance;
        //businessWalletBalance = JSON.parse(balanceDetails)['response']['result'][0]['walletType'];
        walletDetailList = JSON.parse(balanceDetails)['response']['result'];
        for(let list of walletDetailList){
                if(list['walletType']==='BUSINESS_WALLET'){
                    balance=list['balance'];
                    break;
                }
        }
        await console.log(NumberUtil.getCommaSepratedNumberUpTo2Digit(balance));
        return NumberUtil.getCommaSepratedNumberUpTo2Digit(balance);
    }

}

 async function test() {
     let val = await new BusinessWalletBalanceApI().getList(Users.BpayUser[0]);
     let val1 = await new BusinessWalletBalanceApI().getBusinessWalletBalance(Users.BpayUser[0]);
     console.log(val1);
 }
 test();



