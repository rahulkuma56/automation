"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Token = require('../../../data/bpay/token');
var config = require("../../../configs/bpay/EnvConfig");
var constants_1 = require("../../../data/bpay/constants");
var APIFilters_1 = require("../../global/APIFilters");
var Users = require("../../../configs/bpay/UserConfig");
var BaseAPI = require("../../global/BaseAPI");
var NumberUtil_1 = require("../../../pages/util/NumberUtil");
var BusinessWalletBalanceApI = /** @class */ (function () {
    function BusinessWalletBalanceApI() {
        this.END_POINT = constants_1.Constants.fundingAccount;
    }
    BusinessWalletBalanceApI.prototype.options = function (dataObject) {
        return {
            uri: config.URLS.baseurl + this.END_POINT,
            method: 'GET',
            resolveWithFullResponse: true,
            headers: {
                'cache-control': 'no-cache',
                'x-user-mid': dataObject.mid,
                'x-user-token': "beb63e60-7df5-4233-9d89-27f86f0e6300",
                'content-type': APIFilters_1.ContentType.JSON,
                'x-auth-ump': config.Constants.x_auth_ump
            }
        };
    };
    BusinessWalletBalanceApI.prototype.getList = function (dataobject) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, BaseAPI["default"].execute(this.options(dataobject))];
                    case 1:
                        data = _a.sent();
                        return [2 /*return*/, JSON.parse(data)];
                }
            });
        });
    };
    BusinessWalletBalanceApI.prototype.getBusinessWalletBalance = function (transactionListData) {
        return __awaiter(this, void 0, void 0, function () {
            var balanceDetails, walletDetailList, balance, _i, walletDetailList_1, list;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, BaseAPI["default"].execute(this.options(transactionListData))];
                    case 1:
                        balanceDetails = _a.sent();
                        walletDetailList = [];
                        //businessWalletBalance = JSON.parse(balanceDetails)['response']['result'][0]['walletType'];
                        walletDetailList = JSON.parse(balanceDetails)['response']['result'];
                        for (_i = 0, walletDetailList_1 = walletDetailList; _i < walletDetailList_1.length; _i++) {
                            list = walletDetailList_1[_i];
                            if (list['walletType'] === 'BUSINESS_WALLET') {
                                balance = list['balance'];
                                break;
                            }
                        }
                        return [4 /*yield*/, console.log(NumberUtil_1.NumberUtil.getCommaSepratedNumberUpTo2Digit(balance))];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, NumberUtil_1.NumberUtil.getCommaSepratedNumberUpTo2Digit(balance)];
                }
            });
        });
    };
    return BusinessWalletBalanceApI;
}());
exports.BusinessWalletBalanceApI = BusinessWalletBalanceApI;
function test() {
    return __awaiter(this, void 0, void 0, function () {
        var val, val1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, new BusinessWalletBalanceApI().getList(Users.BpayUser[0])];
                case 1:
                    val = _a.sent();
                    return [4 /*yield*/, new BusinessWalletBalanceApI().getBusinessWalletBalance(Users.BpayUser[0])];
                case 2:
                    val1 = _a.sent();
                    console.log(val1);
                    return [2 /*return*/];
            }
        });
    });
}
test();
