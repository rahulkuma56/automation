import {Status} from "../global/APIFilters";


export class DataObject {

    public userName: string;
    public password: string;
    public mid: string;
    public userType:string;
    public filterType: string;
    public filterValue: string;
    public orderCreatedStartTime;
    public orderCreatedEndTime;
    public status: Status;


    constructor(userDetail, startDate = '', endDate = '') {
        this.userName = userDetail.name;
        this.password = userDetail.password;
        this.mid = userDetail.mid;
        this.userType = userDetail.merchantType;
        this.orderCreatedStartTime = startDate;
        this.orderCreatedEndTime = endDate;
    }

}