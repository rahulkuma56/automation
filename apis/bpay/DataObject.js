"use strict";
exports.__esModule = true;
var DataObject = /** @class */ (function () {
    function DataObject(userDetail, startDate, endDate) {
        if (startDate === void 0) { startDate = ''; }
        if (endDate === void 0) { endDate = ''; }
        this.userName = userDetail.name;
        this.password = userDetail.password;
        this.mid = userDetail.mid;
        this.userType = userDetail.merchantType;
        this.orderCreatedStartTime = startDate;
        this.orderCreatedEndTime = endDate;
    }
    return DataObject;
}());
exports.DataObject = DataObject;
