export class Constants {

    public static NO_CACHE = 'no-cache';

    public static fundingAccount = '/api/v1/subwallet/balance';
    public static addFund = '/api/v1/subwallet/balance';
    public static  subwalletDetail = '/api/v2/subwallet';
    public static merchantProfileDetail = '/api/v1/merchantprofile/details'
}




