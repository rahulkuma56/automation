"use strict";
exports.__esModule = true;
var Constants = /** @class */ (function () {
    function Constants() {
    }
    Constants.NO_CACHE = 'no-cache';
    Constants.fundingAccount = '/api/v1/subwallet/balance';
    Constants.addFund = '/api/v1/subwallet/balance';
    Constants.subwalletDetail = '/api/v2/subwallet';
    Constants.merchantProfileDetail = '/api/v1/merchantprofile/details';
    return Constants;
}());
exports.Constants = Constants;
