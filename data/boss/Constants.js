"use strict";
exports.__esModule = true;
var Constants = /** @class */ (function () {
    function Constants() {
    }
    //public static COMMISSION_DETAILS = '/api/v3/merchant/umpmer16529288099081/commission?searchType=ALL';
    //public static COMMISSION_DETAILS = '/api/v3/merchant/UATIER58713478430221/commission?searchType=ALL';
    Constants.COMMISSION_DETAILS = '/api/v3/merchant/UATIER95741758356371/commission?searchType=ALL';
    Constants.INSTRUMENT_DETAILS = '/api/v2/merchant/MID_OPTION/channels?status=ALL';
    Constants.NO_CACHE = 'no-cache';
    return Constants;
}());
exports.Constants = Constants;
