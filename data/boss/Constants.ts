import {Method} from "../../apis/global/APIFilters";

export class Constants {

    //public static COMMISSION_DETAILS = '/api/v3/merchant/umpmer16529288099081/commission?searchType=ALL';
    //public static COMMISSION_DETAILS = '/api/v3/merchant/UATIER58713478430221/commission?searchType=ALL';
    public static COMMISSION_DETAILS = '/api/v3/merchant/UATIER95741758356371/commission?searchType=ALL';

    public static INSTRUMENT_DETAILS = '/api/v2/merchant/MID_OPTION/channels?status=ALL';

    public static NO_CACHE = 'no-cache';


}