export class Constants {

    public static authorize = '/oauth2/authorize';

    public static User_Token = '/oauth2/usertokens/';

    public static Oauth_Token = '/oauth2/token';

    public static NO_CACHE = 'no-cache';

    public static paymentBulkLink = '/api/v1/payment/link/bulk';

    public static TRANSACTION_LIST='/api/v2/order/list';

    public static TRANSACTION_SUMMARY='/api/v2/order/summary';

    public static TRANSACTION_ORDER_DETAIL='/api/v3/order/detail';

    public static MERCHANT_PROFILE_KYC='/api/v1/merchantprofile/kyc';

    public static MERCHANT_PROFILE='/api/v1/profile';

    public static MERCHANT_COMMISSION='/api/v1/merchantprofile/commission?searchType=all';

    public static MERCHANT_ADDRESS='/api/v1/merchantprofile/address';

    public static MERCHANT_BASIC='/api/v1/merchantprofile/basic';

    public static Get_WALLET_QRCODE='/api/v1/qrcode/wallet/merchant/?type=';

    public static ANALYTICS_TXNS_PAYMODE='/api/v1/analytics/txns/paymode';

    public static FUND_MOVE='/api/v1/bw/config/fund/move';

    public static MERCHANT_PROFILE_PAYMODES ='/api/v1/merchantprofile/paymodes';

    public static ANALYTICS_TXNS = '/api/v1/analytics/txns';

    public static PAYMENT_LINK = '/api/v2/payment/link';

    public static BULK_PAYMENT_LINK = '/api/v1/payment/link/files';

    public static LINK_FETCH = '/api/v2/invoice/fetch';

    public static INVOICE_SUMMARY = '/api/v2/invoice/summary';

    public static INVOICE_PREVIEW = '/api/v2/invoice/preview';

    public static MERCHANT_DETAIL =  "/api/v2/invoice/detail/merchant";

    public static INVOICE_DETAILS = "/api/v2/invoice/detail/txn";

    public static API_CONFIG = '/api/v1/merchantprofile/website/config';

    public static STAGING_KEY = '/api/v2/key';

    public static PROD_KEY = '/api/v2/key';

    public static V1_SUBUSER='/api/v1/subusers?start=1&end=5';
    
    public static SUB_USERS_ROLES = '/api/v2/subusers/roles';

    public static SUB_USERS = '/api/v2/subusers';

    public static SETTLEMENT_BILL_LIST='/api/v2/settlement/bill/list';

    public static SETTLEMENT_BILL_SUMMARY='/api/v2/settlement/bill/summary';

    public static SETTLEMENT_WITHDRAW_BALANCE = '/api/v1/bw/withdraw/balance';

    public static SETTLEMENT_UTR_LIST='/api/v2/settlement/txn/list';

    public static BW_ORDER_SEARCH = '/api/v1/bw/order/search';

    public static MERCHANT_MAPPING='/static/merchant/mappings';

    public static V1_CONTEXT='/api/v1/context';

    public static CACHE_CONTROL='cache-control';

    public static AUTHORIZATION_CODE = "authorization_code"

    public static MERCHANT_PROFILE_COMMISSION='/api/v1/merchantprofile/commission?searchType=all';

    public static FORM_URL_ENCODED='application/x-www-form-urlencoded';

    public static MERCHANT_PROFILE_COMISSION_V2='/api/v2/merchantprofile/commission?searchType=all';

    public static PROCESS_TRANSACTION = '/theia/processTransaction';

    public static DOWLNLOAD_LIST = '/api/v2/download';

    public static GST_INVOICE = '/api/v1/invoice?year=2019';

}




