let automationToken = require('./automationToken');
let prodToken = require('./prodToken');
let stagingToken = require('./stagingToken')
let Token;
if (process.env.ENV === 'prod') {
    Token = prodToken;
}else if(process.env.ENV === 'preProd'){
    Token = prodToken;
}else if(process.env.ENV === 'staging'){
    Token = stagingToken;
}
else {
    Token = automationToken;
}

module.exports=Token;