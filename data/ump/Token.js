var automationToken = require('./automationToken');
var prodToken = require('./prodToken');
var stagingToken = require('./stagingToken');
var Token;
if (process.env.ENV === 'prod') {
    Token = prodToken;
}
else if (process.env.ENV === 'preProd') {
    Token = prodToken;
}
else if (process.env.ENV === 'staging') {
    Token = stagingToken;
}
else {
    Token = automationToken;
}
module.exports = Token;
